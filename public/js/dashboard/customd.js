$(document).ready(function() {
    $("#password").keyup(function() {
        var a = /([0-9])/;
        var b = /([a-zA-Z])/;
        var c = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
        if ($("#password").val().length < 6) {
            $("#password-strength-status").removeClass();
            $("#password-strength-status").addClass("text-danger font-weight-700");
            $("#password-strength-status").html("Weak (should be atleast 6 characters.)");
        } else if ($("#password").val().match(a) && $("#password").val().match(b) && $("#password").val().match(c)) {
            $("#password-strength-status").removeClass();
            $("#password-strength-status").addClass("text-success font-weight-700");
            $("#password-strength-status").html("Strong");
        } else {
            $("#password-strength-status").removeClass();
            $("#password-strength-status").addClass("text-warning font-weight-700");
            $("#password-strength-status").html("Medium ");
        }
    });

    $("#cpassword").keyup(function() {
        if ($("#password").val() != $("#cpassword").val()) $("#password-strength-status").html("password does not match"); else $("#password-strength-status").html("password match");
    });

$(document).on('click', '.user_bus a', function(event){
    event.preventDefault(); 
    var page = $(this).attr('href').split('page=')[1];
    $.ajax({
            url:"users/fetch_data?page="+page,
            success:function(data)
            {
                $('#user_data').html(data);
            }
          })
       })

$(document).on('click', '.vendor_bus a', function(event){
    event.preventDefault(); 
    var page = $(this).attr('href').split('page=')[1];
    $.ajax({
            url:"vendors/fetch_data?page="+page,
            success:function(data)
            {
                $('#vendor_data').html(data);
            }
          })
       })

$("#admintodo").click(function() {
     localStorage.setItem("adminnote", $(".adminnote").text());
    });


$("#adminpro").click(function() {

    $('.adminprofl').each(function() 
    {
       $('.adminprofl').attr('disabled',false);
    })
    });

$(document).on('click', '.deleteuser #deletecp', function(event)
{
    $('#deletemodal').modal('show');
    $('#deletes').attr('href',$(this).attr('data-url'));
});

$(document).on('click', '.deletevendor #deletecp', function(event){
        $('#deletemodal').modal('show');
        $('#deletes').attr('href',$(this).attr('data-url'));
    }); 

$(document).on('click', '.deleteorder #deletecp', function(event)
{
    $('#deletemodal').modal('show');
    $('#deletes').attr('href',$(this).attr('data-url'));
});
/* User child admin code */
$(document).on('click', '.deleteuser #viewuser', function(event){

        $('#viewusermodal').modal('show');
        $('#viewusermodal #img').attr('src',$(this).attr('data-img'));
        $('#viewusermodal .name').text($(this).attr('data-name'));
        $('#viewusermodal .email').text($(this).attr('data-email'));
        $('#viewusermodal .phone').text($(this).attr('data-phone'));
        $('#viewusermodal .dob').attr('href','public/'+$(this).attr('data-dob'));
        $('#viewusermodal .college').text($(this).attr('data-college'));
        $('#viewusermodal .course').text($(this).attr('data-course'));
        $('#viewusermodal .address').text($(this).attr('data-address'));
        $('#viewusermodal .interest').text($(this).attr('data-interest'));
        $('#viewusermodal .urself').text($(this).attr('data-urself'));
});

/* deal area section */

$(document).on('click', '.dealadmin #viewdeal', function(event){

        $('#viewdealmodal').modal('show');
        $('#viewdealmodal .deal_id').text($(this).attr('data-id'));
        $('#viewdealmodal #img').attr('src',$(this).attr('data-img'));
        $('#viewdealmodal .dealname').text($(this).attr('data-name'));
        $('#viewdealmodal .price').text($(this).attr('data-price'));
       /* $('#viewdealmodal .quantity').text($(this).attr('data-discount'));*/
        $('#viewdealmodal .discount').text($(this).attr('data-discount'));
        $('#viewdealmodal .validity').text($(this).attr('data-validity'));
        $('#viewdealmodal .description').text($(this).attr('data-desc')); 
    }); 

$(document).on('click', '.dealadmin #editdeal', function(event){

        $('#editmodaldeal').modal('show');
        $('#editmodaldeal #id').val($(this).attr('data-id'));
        $('#editmodaldeal #name').val($(this).attr('data-name'));
        $('#editmodaldeal #price').val($(this).attr('data-price'));
       /* $('#editmodaldeal #quantity').val($(this).attr('data-discount'));*/
        $('#editmodaldeal #discount').val($(this).attr('data-discount'));
        $('#editmodaldeal #validity').val($(this).attr('data-validity'));
        $('#editmodaldeal #description').val($(this).attr('data-desc'));
    }); 

/* edit cat */
$(document).on('click', '.deleteuser #editcat', function(event){

        $('#editmodalcat').modal('show');
        $('#editmodalcat #id').val($(this).attr('data-id'));
        $('#editmodalcat #cat').val($(this).attr('data-cat'));
        $('#editmodalcat #icon').val($(this).attr('data-icon'));
    }); 

$(document).on('click', '.addcat', function(event){

        $('#addmodalcat').modal('show');
        $('#addmodalcat #cat').val($(this).attr('data-cat'));
        $('#addmodalcat #icon').val($(this).attr('data-icon'));
    }); 
	
/*coupon area section */

$(document).on('click', '.couadmin #viewcou', function(event){

        $('#viewcoumodal').modal('show');
        $('#viewcoumodal #img').attr('src',$(this).attr('data-img'));
        $('#viewcoumodal .cou_id').text($(this).attr('data-id'));
        $('#viewcoumodal .couname').text($(this).attr('data-name'));
        $('#viewcoumodal .code').text($(this).attr('data-code'));
        $('#viewcoumodal .discount').text($(this).attr('data-discount'));
        $('#viewcoumodal .validity').text($(this).attr('data-validity'));
        $('#viewcoumodal .description').text($(this).attr('data-desc')); 
    }); 

$(document).on('click', '.couadmin #editcou', function(event){

        $('#editmodalcou').modal('show');
        $('#editmodalcou #id').val($(this).attr('data-id'));
        $('#editmodalcou #name').val($(this).attr('data-name'));
        $('#editmodalcou #code').val($(this).attr('data-code'));
       /* $('#editmodalcou #quantity').val($(this).attr('data-discount'));*/
        $('#editmodalcou #discount').val($(this).attr('data-discount'));
        $('#editmodalcou #validity').val($(this).attr('data-validity'));
        $('#editmodalcou #description').val($(this).attr('data-desc'));
    }); 

/*payment section*/

$(document).on('click', '.adminpay #viewpay', function(event){

        $('#viewpay').modal('show');
        $('#viewpay .id').text($(this).attr('data-id'));
        $('#viewpay .email').text($(this).attr('data-email'));
        $('#viewpay .phone').text($(this).attr('data-phone'));
        $('#viewpay .plan').text($(this).attr('data-plan'));
        $('#viewpay .amount').text($(this).attr('data-amount'));
        $('#viewpay .status').text($(this).attr('data-status'));
        $('#viewpay .date').text($(this).attr('data-date')); 
    }); 

$("#memedit").click(function() {
    $('.memeditable').attr('contenteditable',true)
});

$("#memtable td").keyup(function() {

   var id=$(this).attr('id');
   var field=$(this).attr('field');
   var value = $.trim($(this).text()).toLowerCase();
   var datas = { id : id, field : field, value : value}
    $.ajax({
            Type: 'POST',
            data: datas,
            url: "featuresedit",
                success:function(data)
                {
                  
                }
            })
});

 var SITEURL = 'http://159.65.158.198/chilra/';
 var token = $('meta[name="csrf-token"]').attr('content');

 $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('.inv_status').on('change', function() {
      var id=$(this).val();
      var mer=$('.Merchant').attr('data-id');
      $.ajax({
                type:'post',
                url: SITEURL + 'admin/inv_status',
                data:{id:id,mer:mer},
                success:function(data)
                {
                   $('.responce').removeClass('d-none');
                }
              })
    })
    var $body = document.getElementsByTagName('body')[0];
    var copyToClipboard = function(secretInfo) 
    {
        var $tempInput = document.createElement('INPUT');
        $body.appendChild($tempInput);
        $tempInput.setAttribute('value', secretInfo)
        $tempInput.select();
        document.execCommand('copy');
        $body.removeChild($tempInput);
    }

    $(".thumb .copy-link").on('click', function(){
    var secretInfo = $(this).attr('src');
    copyToClipboard(secretInfo);
      $('.container-fluid').find('.alert').removeClass('d-none');
      setTimeout(function() {
        $('.container-fluid').find('.alert').addClass('d-none');
    }, 2500);
    });


jQuery(document).ready(function() {
    jQuery('.loader').fadeOut(100);
});

});