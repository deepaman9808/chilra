$(document).ready(function(){

/*custom tickbox policy*/

var token = $('meta[name="csrf-token"]').attr('content');

$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

    $("#customCheckRegister").click(function()
    {
       if ($(this).prop('checked')==true)
       { 
        $('#create_account').attr('disabled',false);
       }
       else
       {
        $('#create_account').attr('disabled',true);
       }
    });

     $("#mrphone").keyup(function()
    {
        if ($('#mrphone').val().length == 10) 
        {
           $('#verifyphone').attr('disabled',false); 
        }
        else
        {
            $('#verifyphone').attr('disabled',true); 
        }
       
    });

    $("#verifyphone").click(function()
    {
        var number=$('#mrphone').val();
        var datas = { nm:number ,token:token}
        
        $.ajax({
                data:datas,
                url:"send_otp",
                success:function(data)
                {
                  $('#sendotpsuccess').removeClass("none");
                  $('#otp').attr('disabled',false);
                  $('#verifyphone').attr('disabled',true);
                }
              })
        setTimeout(function() 
        {
          $('#verifyphone').attr('disabled',false);
        }, 30000);
    });

/* password strength */

   $("#password").keyup(function(){
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password').val().length < 6) 
            {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('text-danger font-weight-700');
                $('#password-strength-status').html("Weak (should be atleast 6 characters.)");
            }
            else 
            {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) 
                {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('text-success font-weight-700');
                    $('#password-strength-status').html("Strong");
                } 
                else {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('text-warning font-weight-700');
                    $('#password-strength-status').html("Medium ");
                }
            }

        })

   $(document).on( "click", ".deletedl", function() {
      $('#deletemodal').modal('show');
      var id=$(this).attr('data-id');
     $(".yes").attr("href", "deletedeal/"+id);
   });

    $(document).on("click", ".editdl", function() 
    {

      $('#editmodaldl').modal('show');
      $('#deal_id').attr('value',$(this).attr('data-id'));
      $('#dealname').attr('value',$(this).attr('data-name'));
      $('#discount').attr('value',$(this).attr('data-discount'));
      $('#validity').attr('value',$(this).attr('data-validity'));
      $('#price').attr('value',$(this).attr('data-price'));
      $('#desc').attr('value',$(this).attr('data-desc'));
      $('#inlineFormCustomSelect').val($(this).attr('data-cat'));

      
     var check= $.trim($(this).attr('data-checked'))
     if(check=='true')
     {
      $('#customCheck1').attr('checked',true);
      $('#customCheck1').attr('value',1);
     }
     else
     {
      $('#customCheck1').attr('checked',false);
      
     }
      
    });

    $(document).on("click", ".editcp", function() 
    {

      $('#editmodalcp').modal('show');
      $('#coupon_id').attr('value',$(this).attr('data-id'));
      $('#name').attr('value',$(this).attr('data-name'));
      $('#discount').attr('value',$(this).attr('data-discount'));
      $('#validity').attr('value',$(this).attr('data-validity'));
      $('#code').attr('value',$(this).attr('data-code'));
      $('#desc').attr('value',$(this).attr('data-desc'));
      
      $('#inlineFormCustomSelect').val($(this).attr('data-cat'));
      
    });


  $(document).on("click", ".deletecp", function() {

     $('#deletemodal').modal('show');
      var id=$(this).attr('data-id');
     $(".dele").attr("href", "deletecoupon/"+id);
    });


    $("#image").on('change', function() {
     var file = this.files[0];
      if(file.type != "image/png" && file.type != "image/jpeg" && file.type != "image/gif")
      {
        alert("Please choose png, jpeg or gif files");
        return false;
      }
      var reader = new FileReader();
      reader.onload = function(e) 
      {
        $('#wizardPicturePreview').attr('src', e.target.result);
      }
      reader.readAsDataURL(file);
      //$('#getmer').submit();
});


      var SITEURL = 'http://159.65.158.198/chilra/';
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
       
         $('body').on('click', '.buy_now', function(e){
          var totalAmount = $(this).attr("data-amount");
          var product_id =  $(this).attr("data-id");
          var phone =  $(this).attr("data-phone");
          var email =  $(this).attr("data-email"); 
          var image =  $(this).attr("data-image");
          var plan  =  $(this).attr("data-plan");

           var options = {
           "key": "rzp_test_bjCg62rbDvcrf4",
           "amount": (totalAmount*100), // 2000 paise = INR 20
           "name": "chilra",
           "description": "Payment",
           "image": image,

           "handler": function (response){
                $.ajax({  
                   url: SITEURL + 'paysuccess',
                   type: 'post',
                   dataType: 'json',
                   data: {
                           payment_id: response.razorpay_payment_id , 
                           amount : totalAmount ,
                           product_id : product_id,
                           email:email,
                           phone:phone,
                           plan:plan,
                           token:token
                          }, 
                   success: function (msg) 
                   {
                       window.location.href = SITEURL + '/merchant/membership/success';
                   }
               });

           },

          "prefill": {
               "contact": phone,
               "email":   email,
           },
           "theme": {
               "color": "#f00000"
           }
         };
         var rzp1 = new Razorpay(options);
         rzp1.open();
         e.preventDefault();
         });

  /*confirm password */

       $("#cpassword").keyup(function(){
        {
            if($('#password').val()!= $('#cpassword').val())
            {
              $('#password-strength-status').html("password does not match");
            }
            else
            {
              $('#password-strength-status').html("password match");
            }
           
        }
    })
        $("#account .categoryjs").click(function()
        {
         $(this).find('.choice').removeClass("has-error");
       });

        $(".delete .deletecv").click(function()
        {
          $('#deletemodal').modal('show');
          $('#deletes').attr('href',$(this).attr('data-url'));
       });




     $('.order_status').on('change', function() {
      var id=$('.order_value').attr('data-value');
      $.ajax({
                type:'post',
                url: SITEURL + 'merchant/order_status',
                data:{id:id},
                success:function(data)
                {
                  
                }
              })
    })
});


