$(document).ready(function(){

$.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
});
       
/* token csrf and site url */

var token=$('meta[name="csrf-token"]').attr('content');
var SITEURL = 'http://159.65.158.198/chilra/';

$.ajax({
  url: "https://geolocation-db.com/jsonp",
  jsonpCallback: "callback",
  dataType: "jsonp",
  success: function(location) {
    $('#state').html(location.state);
    $('#city').html(location.city);
  }
});

/*custom tickbox policy*/

    $("#customCheckRegister").click(function()
    {
       if ($(this).prop('checked')==true)
       { 
        $('#create_account').attr('disabled',false);
       }
       else
       {
        $('#create_account').attr('disabled',true);
       }
    });

     $("#mobile").keyup(function()
    {
        if ($('#mobile').val().length == 10) 
        {
           $('.verifyphone').attr('disabled',false); 
        }
        else
        {
            $('.verifyphone').attr('disabled',true); 
        }
       
    });

    $(".verifyphone").click(function()
    {
      if ($('#mobile').val().length == 10) 
      {
        var number=$('#mobile').val();
        var datas = { nm:number,token:token }
        $.ajax({
            data:datas,
            url:"home/send_otp",
                success:function(data)
                {
                     $('.verifyphone').addClass('d-none');
                     $('.sendsuccess').removeClass('d-none');
                     $('.verifyotp').removeClass('d-none');
                }
            })
        $('#otpverify').removeClass("d-none"); 
      }
      else
      {
        console.log('Something went wrong.someones tries to hack');
      }
    });

    $("#otp").on('keyup change', function ()
    {
        if ($('#otp').val().length == 6) 
        {
          $('.verifyotp').attr('disabled',false); 
          $('.verified').addClass('d-none');
          $('.otpexpire').addClass('d-none');
          $('.wrongotp').addClass('d-none');
        }
        else
        {
            $('.verifyotp').attr('disabled',true); 
        }
       
    });

    $(".verifyotp").click(function()
    {
       $('.sendsuccess').addClass('d-none');
        var otp=$('#otp').val();
        var datas = { otp:otp,token:token }
        $.ajax({
            data:datas,
            url:"home/check_otp",
                success:function(data)
                {
                     $('.verifyphone').addClass('d-none');
                     $('.verifyotp').removeClass('d-none');

                     if(data==1)
                     {
                        $('.verified').removeClass('d-none');
                        $('.verifyotp').addClass('d-none');
                        $('.otpexpire').addClass('d-none');
                        $('.wrongotp').addClass('d-none');
                        setTimeout(function() 
                        {
                          $('#otpverify').addClass("d-none"); 
                        }, 3000);
                        
                     }
                     else if(data==2)
                     {
                       $('.otpexpire').removeClass('d-none');
                       $('#otp').val('');
                     }
                     else
                     {
                      $('.wrongotp').removeClass('d-none');
                      $('#otp').val('');
                     }
                }
            })
        $('#otpverify').removeClass("d-none"); 
    });

/* password strength */

   $("#password").keyup(function(){
            var number = /([0-9])/;
            var alphabets = /([a-zA-Z])/;
            var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
            if ($('#password').val().length < 6) 
            {
                $('#password-strength-status').removeClass();
                $('#password-strength-status').addClass('text-danger font-weight-700');
                $('#password-strength-status').html("Weak (should be atleast 6 characters.)");
            }
            else 
            {
                if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) 
                {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('text-success font-weight-700');
                    $('#password-strength-status').html("Strong");
                } 
                else {
                    $('#password-strength-status').removeClass();
                    $('#password-strength-status').addClass('text-warning font-weight-700');
                    $('#password-strength-status').html("Medium ");
                }
            }

        })

  /*confirm password */
       $("#cpassword").keyup(function(){
        {
            if($('#password').val()!= $('#cpassword').val())
            {
              $('#password-strength-status').html("password does not match");
            }
            else
            {
              $('#password-strength-status').html("password match");
            }
        }
    })

    $('.top').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
      return false;
    });

/* deal payment save */
      
         $('body').on('click', '.buy_now', function(e){
          var pid= document.URL.split('=').pop();
          var phone =  $(this).attr("data-phone");
          var email =  $(this).attr("data-email"); 
          var image =  $(this).attr("data-image");
          var datas = { pid:pid,token:token }

          $.ajax({
            data:datas,
            url:"check_pid",
                success:function(data)
                {
                    var totalAmount = data.price-data.price*data.discount/100;
                    var merchant   =  data.merchant_id;

                     var options = {
                     "key": "rzp_test_bjCg62rbDvcrf4",
                     "amount": (totalAmount*100), // 2000 paise = INR 20
                     "name": "chilra",
                     "description": "Payment",
                     "image": image,

                     "handler": function (response){
                          $.ajax({  
                             url: SITEURL + 'deal/pay',
                             type: 'post',
                             dataType: 'json',
                             data: {
                                     payment_id: response.razorpay_payment_id , 
                                     amount : totalAmount ,
                                     product_id : pid,
                                     merchant : merchant,
                                     email:email,
                                     phone:phone,
                                     saving:data.price*data.discount/100,
                                     token:token
                                    }, 
                             success: function (msg) 
                             {
                                 $('#paysModal').modal('show');
                                 $('.paym_id').text(response.razorpay_payment_id);
                                 
                             }
                         });

                     },

                    "prefill": {
                         "contact": phone,
                         "email":   email,
                     },
                     "theme": {
                         "color": "#f00000"
                     }
                   };
                      var rzp1 = new Razorpay(options);
                      rzp1.open();
                      e.preventDefault();
                   }
             });
       });


  /*deals star ratings*/
     
     $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); 
   
 
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  $('#stars li').one('click', function(){
    var onStar = parseInt($(this).data('value'), 10); 
    
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    

    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) 
    {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else 
    {
        msg = "We will improve ourselves";
    }
    responseMessage(msg);

    var deal_id=document.URL.split('=').pop();
    var user_id=$('#user_id').attr('value');
    
      $.ajax({  
                  url: SITEURL + 'home/deal/review',
                  type: 'post',
                  dataType: 'json',
                  data: {
                          user_id: user_id,
                          deal_id:deal_id,
                          review:ratingValue,
                          token:token
                        }, 
                  success: function (data) 
                  {
                     $('.before-rate .rs').html( '('+data+ ' ratings)' );
                  }
              });
      });

  function responseMessage(msg) 
  {
    $('.success-box div.text-message').html("<span>" + msg + "</span>");

    setTimeout(function() 
    {
      $('.success-box').addClass('d-none');
    }, 1900);
  }

      /*deals active */
     $("#postcm").click(function(){
        {
           var message = $('textarea#commentarea').val();

           if(message)
           {
             var user_id=$('#user_id').attr('value');
             var name = $('#logname').attr('value');
             var deal_id=document.URL.split('=').pop();
             var image = $('#avatar').attr('value');
             var page =  $('#page').attr('value');

            $.ajax({  
                  url: SITEURL + 'home/deal/comment',
                  type: 'post',
                  dataType: 'json',
                  data: {
                          user_id: user_id,
                          deal_id:deal_id,
                          comment:message,
                          page:page,
                          token:token
                        }, 
                  success: function (data) 
                  {
                     $('textarea#commentarea').val('');
                     location.reload();
                     /*$('.appending').html('<li class="media p-4"><a href="#" class="img"><img src='+image+' alt="" class="img-rounded"></a><div class="media-body ml-3 py-2"><strong class="text-success">'+name+'</strong> <span> made a comment</span><br><span class="text-muted"><small class="text-muted">Now</small> </span><p>'+data.comment+'</p></div><div class="text-right" style="cursor: pointer;"> <a href="&enc=1234"><i class="fa fa-trash orange" area-hidden="true"></i></a></div></li>').hide().fadeIn(1500);*/
                  }
              });
            }
            else
            {
              alert('Message area empty')
            }
        }
    })


     $(".log #code").click(function(){
        {
           $('#myModal').modal('show');
           $('#myModal .modal-title').text($(this).attr('data-name'));
           $('#myModal .coupon').attr('id',$(this).attr('data-id'));
           $('#myModal .discount').text($(this).attr('data-discount')+' %');
           $('#myModal .imageavatar').attr('src',$(this).attr('data-image'));
           $('#myModal .desc').text($(this).attr('data-desc'));
           $('#myModal .promo').text($(this).attr('data-code'));
           $('#myModal .expire').text($(this).attr('data-ex'));
        }
      });
     $(".log #codes").click(function(){
           {
           $('#myModals').modal('show');
            var counter = 7;
            var interval = setInterval(function() {
            counter--;
            if (counter <= 0) {
              clearInterval(interval);
              window.location.replace("../../chilra/profile");  
              return;
            } else {
            $('#time').text(counter);
            }
          }, 1000);
        }
      });

     $("#print").click(function(){
            var printContents = document.getElementById('modalbody').innerHTML;
            w=window.open();
            w.document.write(printContents);
            w.print();
            w.close();
          });

     $('#clock').countdown($('#validity').attr('value'), function(event) {
       $(this).html(event.strftime('%D days %H:%M:%S'));
     });

        var $body = document.getElementsByTagName('body')[0];
        var copyToClipboard = function(secretInfo) 
        {
          var $tempInput = document.createElement('INPUT');
          $body.appendChild($tempInput);
          $tempInput.setAttribute('value', secretInfo)
          $tempInput.select();
          document.execCommand('copy');
          $body.removeChild($tempInput);
        }

      $("#cclip").on('click', function(){
        var secretInfo = $('#ccopy').text();
        copyToClipboard(secretInfo);

        var id=$('.coupon').attr('id');
        var user_id=$('#user_id').attr('value');
        $.ajax({  
                  url: SITEURL + 'home/coupon/report',
                  type: 'post',
                  data: {
                          user_id: user_id,
                          id:id,
                          used:1
                        }, 
                  success: function (data) 
                  {
                      $('.thank-you-pop .hide').css('display',"block");
                      $('.modal-footer #print').attr('disabled',false);
                  }
              });

      });

      $("#dclip").one('click', function(){

        var secretInfo = $('#dcopy').text();
        copyToClipboard(secretInfo);
        $('.thank-you-pop .hide').css('display',"block");
        $('.modal-footer #print').attr('disabled',false);
      });

      $(".clipb").one('click', function(){

        var secretInfo = $(this).attr('id');
        copyToClipboard(secretInfo);
        $(this).text('copied');
        $(this).addClass('text-success');
      });

      $(".modal").on("hidden.bs.modal", function () {

           $('.thank-you-pop .hide').css('display',"none");
        });


  $.validator.addMethod('filesize', function (value, element,param) {
  var size=element.files[0].size;
  size=size/1024;
  size=Math.round(size);
  return this.optional(element) || size <=param ;
  
}, 'File size must be less than 4 mb');


$('#Follow').on('click', function() {
/*follower increase */
var id = document.URL.split('=').pop();
var login=$('#login_id').attr('value');
        $.ajax({  
                  url: SITEURL + 'follower',
                  type: 'post',
                  data: {
                          user_id: id,
                          login_id:login,
                          token:token
                        }, 
                  success: function (data) 
                  {
                     $('.followdone').html('Successfully Followed');
                  }
              });

        setTimeout(function() {
          $('.followdone').html('');
        }, 3000);
});

$('.tabbable ul>li>a').on('click', function() 
{
  $.cookie('activecc', $(this).attr('data-id'));
});

if($.cookie('activecc')== null)
{

  $("#third-tab").addClass("active");
  $(".pills #third").addClass("active show");
}

if($.cookie('activecc')==1)
{
  $("#first-tab").addClass("active");
  $(".pills #first").addClass("active show");
}

if($.cookie('activecc')==2)
{
 
  $("#second-tab").addClass("active");
  $(".pills #second").addClass("active show");
}

if($.cookie('activecc')==3)
{

  $("#third-tab").addClass("active");
  $(".pills #third").addClass("active show");
}

if($.cookie('activecc')==4)
{

  $("#fourth-tab").addClass("active");
  $(".pills #fourth").addClass("active show");
}

if($.cookie('activecc')==5)
{
  $("#fifth-tab").addClass("active");
  $(".pills #fifth").addClass("active show");
}




$('#second .deletedl').on('click', function() {
	$('#deletemodal').modal('show');
	$('#deletes').attr('href',$(this).attr('data-url'));
});


/*open modal when local checkout*/
$('.localcheck').on('click', function() {

var id=$('#user_id').attr('value');
var deal=document.URL.split('=').pop();
var loc =$(this).attr('data-loc');
        $.ajax({  
                  url: SITEURL + 'localcheck',
                  type: 'post',
                  data: {
                          user_id: id,
                          deal_id:deal,
                          loc:loc,
                          token:token
                        }, 
                  success: function (data) 
                  {
                     $('.paym_id').text(data);
                     $('#paysModal').modal('show');
                  }
              });

  
});


$(document).on('click', '#second .editlocal', function(event){

        $('#editlocal').modal('show');
        $('#editlocal #deal_id').val($(this).attr('data-id'));
        $('#editlocal #name').val($(this).attr('data-name'));
        $('#editlocal #price').val($(this).attr('data-price'));
        $('#editlocal #quantity').val($(this).attr('data-discount'));
        $('#editlocal #discount').val($(this).attr('data-discount'));
        $('#editlocal #validity').val($(this).attr('data-validity'));
        $('#editlocal #desc').val($(this).attr('data-desc'));
        $('#editlocal #loc').val($(this).attr('data-loc'));
        $('#inlineFormCustomSelect').val($(this).attr('data-cat'));
    }); 




$('#form').validate({ 

    rules: {

        name: {

            required: true,
            maxlength: 80,
            minlength: 4

        },

        email: {

            required: true,
            email: true,
            maxlength:100

        },

        image: {

            required: true,
            extension: "jpeg|png|jpg",
            filesize: 4000

        },

        dob: {

            required: true,
            extension: "pdf|jpeg|png|jpg",
            filesize: 4000

        },

        gender: {

            required: true,
        },

        mobile: {

            required: true,
            number: true,
            minlength: 10,
            maxlength:10

        },

        college: {

            required: true,
            maxlength: 120,
            minlength: 4
        },

        course: {

            required: true,
            maxlength: 120,
            minlength: 4
        },

        address: {

            required: true,
            maxlength: 120,
            minlength: 4
        },

        urself: {

            required: true,
            minlength:3,
            maxlength: 300
        },

        interest: {

            required: true,
            minlength:3,
            maxlength: 80
        },
        message: {

            required: true,
        },

        password: {

            required: true,
        },

        password_confirmation: {
          required:true,
          equalTo: "#password"
        },
        
        cpassword: {
          required:true,
          equalTo: "#password"
        },
    },

});


});


