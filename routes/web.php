<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* home */
Route::get('/', 'UserController@home')->name('/');                                                   					//homepage

/* merchant */
Route::get('/merchant/login', 'MerchantController@merchantlogin')->name('merchant/login');            				    //Merchant login
Route::get('/merchant/register', 'MerchantController@merchantregister')->name('merchant/register');   					//Merchant register 
Route::get('/merchant/send_otp', 'MerchantController@send_otp')->name('merchant/send_otp');           					//Merchant registration
Route::get('/merchant', 'MerchantController@merchant')->name('merchant');                             					//Merchant dashboard
Route::get('/merchant/deals', 'MerchantController@deals')->name('merchant/deals');                    					//Merchant deals
Route::get('/merchant/coupons', 'MerchantController@merchantcoupon')->name('merchant/coupons');       					//Merchant coupens
Route::get('/merchant/payments', 'MerchantController@merchantpayment')->name('merchant/payments');    					//Merchant payments
Route::get('/merchant/profile', 'MerchantController@merchantprofile')->name('merchant/profile');      					//Merchant profile
Route::get('/merchant/reports', 'MerchantController@merchantreport')->name('merchant/reports');       					//Merchant report
Route::get('/merchant/membership', 'MerchantController@merchantmembership')->name('merchant/membership');   			//Merchant membership
Route::get('/merchant/support', 'MerchantController@merchantsupport')->name('merchant/support');      					//Merchant support
Route::get('/merchant/business', 'MerchantController@merchantbusiness')->name('merchant/business');    			        //Merchant business
Route::get('/mlogout', 'MerchantController@mlogout')->name('mlogout');  								     			//logout vendor 
Route::get('/merchant/membership/success', 'MerchantController@merchantsuccess')->name('merchant/membership/success');  //Merchant registration 
Route::post('/merchant/order_status', 'MerchantController@order_status')->name('merchant/order_status');                //Merchant order status 
Route::get('/merchant/invoice', 'MerchantController@invoice')->name('merchant/invoice');    			                //Merchant invoice
Route::post('/merchant/profile/update', 'MerchantController@merchant_profile')->name('merchant/profile/update');        //Merchant profile 
Route::post('/merchant/profile/password', 'MerchantController@merchant_password')->name('merchant/profile/password');   //Merchant profile 
Route::post('/merchant_reg', 'MerchantController@merchant_reg')->name('merchant_reg');                       			//Merchant registration 
Route::post('/vendorlog', 'MerchantController@vendorlog')->name('vendorlog');                                			//Merchant login post
Route::post('/merchant/dealsave', 'MerchantController@dealsave')->name('merchant/dealsave');                 			//deal save
Route::post('/merchant/dealupdate', 'MerchantController@dealupdate')->name('merchant/dealupdate');                 		//deal update
Route::post('/merchant/couponsave', 'MerchantController@couponsave')->name('merchant/couponsave');           			//coupon save
Route::post('/merchant/couponupdate', 'MerchantController@couponupdate')->name('merchant/couponupdate');                //coupon update
Route::post('/merchant/resave', 'MerchantController@resave')->name('merchant/resave');                       			//Merchant resturent 
Route::get('/merchant/deletedeal/{id}', 'MerchantController@deletedeal')->name('merchant/deletedeal/{id}');   			//coupon delete
Route::get('/merchant/deletecoupon/{id}', 'MerchantController@deletecoupon')->name('merchant/deletecoupon/{id}');   	//coupon delete
Route::post('paysuccess', 'MerchantController@paysuccess');                                                     //merchant pay payment

/* vendor forget */
Route::get('/merchant/forget', 'MerchantController@forget')->name('merchant/forget'); 						           //forget vendor
Route::post('/merchant/sendemail', 'MerchantController@send')->name('merchant/sendemail'); 					           //send email to vendor 
Route::get('/merchant/newpassword/{code}', 'MerchantController@newpassword')->name('merchant/newpassword');            //password reset link 
Route::post('/merchant/setpassword', 'MerchantController@setpassword')->name('merchant/setpassword');; 			       //set new password vendor



/* user area */
Route::post('/registered', 'UserController@registered')->name('registered');        						//Registration page post
Route::post('/log', 'UserController@log')->name('log');                             						//Login page post
Route::post('/update', 'UserController@update')->name('update');                    						//user details update
Route::post('/facebook', 'UserController@facebook')->name('facebook');              						//user details update
Route::get('/logout', 'UserController@logout')->name('logout');       										//Logout user  
Route::get('/home', 'UserController@home')->name('home');             										//home user 
Route::get('/profile', 'UserController@profile')->name('profile');    										//profile user
Route::get('/login', 'UserController@login')->name('login');          										//login user
Route::get('/register', 'UserController@register')->name('register'); 										//register user
Route::get('/home/deals', 'UserController@deals')->name('home/deals'); 										//deals show user
Route::get('/home/coupons', 'UserController@coupons')->name('home/coupons'); 								//coupons show user
Route::get('local', 'UserController@local')->name('local');    												//locals deals user
Route::get('localbuy', 'UserController@localbuy')->name('localbuy');    									//localbuy deals user
Route::get('/home/business', 'UserController@business')->name('home/business'); 							//business show user
Route::get('/home/store', 'UserController@store')->name('home/store'); 										//store show user
Route::get('/home/alldeals', 'UserController@alldeals')->name('home/deals/alldeals'); 			            //all deals show user
Route::get('/contactus', 'UserController@contact')->name('contactus');                 			            //comtactus  page show
Route::post('/home/deal/review', 'UserController@review')->name('home/deal/review'); 						//save review
Route::post('/home/deal/comment', 'UserController@comment')->name('home/deal/comment'); 					//save comment
Route::post('/home/coupon/report', 'UserController@couponreport')->name('home/coupon/report'); 				//save coupon report
Route::get('/home/send_otp', 'UserController@send_otp')->name('home/send_otp'); 							//send otp to user
Route::get('/home/check_otp', 'UserController@check_otp')->name('home/check_otp'); 							//check otp of user
Route::get('/home/blog', 'UserController@blog')->name('home/blog'); 										//show blog pages to user
Route::get('/home/blogpage', 'UserController@blogpage')->name('home/blogpage'); 							//show single blog pages to user
Route::post('/user/localdeal', 'UserController@localdeals')->name('user/localdeal');                 		//deal save
Route::post('/local/dealupdate', 'UserController@dealupdate')->name('local/dealupdate');                 	//deal update
Route::get('/home/allstore', 'UserController@allstore')->name('home/allstore');                 			//all  stores show
Route::get('/home/dealhunters', 'UserController@dealhunters')->name('home/dealhunters');                 	//all  dealhunters show
Route::get('/userprofile', 'UserController@userprofile')->name('userprofile'); 								//show user profile info 
Route::post('/follower', 'UserController@follower')->name('follower'); 										//add followers to db
Route::post('/localcheck', 'UserController@localcheck')->name('localcheck'); 								//add local deals purchase to db
Route::post('/contactdb', 'UserController@contactdb')->name('contactdb'); 									//add  contactus message to db
Route::post('/settingup', 'UserController@settingup')->name('settingup'); 									//Update userprofile password
Route::post('/deal/pay', 'UserController@dealpay')->name('deal/pay'); 										//deals payment pay
Route::get('/deal/success', 'UserController@dealsuccess')->name('deal/success'); 							//deals payment success
Route::get('/privacypolicy', 'UserController@privacypolicy')->name('privacypolicy'); 						//user privacypolicy page
Route::get('/home/check_pid', 'UserController@check_pid')->name('home/check_pid'); 						     //check deal details before payment


/* user forget */
Route::get('/forgotuser', 'UserController@forgotuser')->name('forgotuser'); 						//forget user
Route::post('/sendemailuser', 'UserController@send')->name('sendemailuser'); 						//send email to user for password reset
Route::get('/newpassworduser/{code}', 'UserController@newpassword')->name('newpassworduser');       //password reset link open by user
Route::post('/setpassworduser', 'UserController@setpassword')->name('setpassworduser');; 			//set new password user



/* facebook */
Route::get('/auth/redirect/{provider}', 'UserController@redirect');  								//Facebook get api
Route::get('/callback/{provider}', 'UserController@callback');       								//Facebook callback


/* admin login */
Route::get('/adminlogin', 'AdminController@login')->name('adminlogin'); 							//login page show
Route::post('/adminlog', 'AdminController@adminlog')->name('adminlog'); 							//login page authentication
Route::post('/adminpassword', 'AdminController@adminpassword')->name('adminpassword'); 				//admin password change
Route::post('/adminproimage', 'AdminController@adminproimage')->name('adminproimage'); 				//admin image change



/* admin forget */
Route::get('/forgotadmin', 'AdminController@forgotadmin')->name('forgotadmin'); 					//forget password admin
Route::post('/sendemail', 'AdminController@send')->name('sendemail'); 								//send email to admin for password reset
Route::get('/newpassword/{id}/{code}', 'AdminController@newpassword')->name('newpassword'); 	    //password reset link open by admin
Route::post('/setpassword', 'AdminController@setpassword')->name('setpassword');				    //set new password admin



/* admin Register */
/*Route::get('/adminreg', 'AdminController@register')->name('adminreg');  							//register page show*/
Route::post('/adminregister', 'AdminController@adminregister')->name('adminregister');  			//register page insert
Route::post('/adminpro', 'AdminController@adminpro')->name('adminpro');  							//update admin profile
Route::post('/category/edit', 'AdminController@catedit')->name('category/edit');  					//category edit
Route::post('/category/add', 'AdminController@catadd')->name('category/add');  					    //category add
Route::get('/category/edit', 'AdminController@catedit')->name('category/edit');  					//category edit


/* Dashboard routes */
Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');  							//admin dashboard 
Route::get('/users', 'AdminController@users')->name('users');  										//admin users 
Route::get('/vendors', 'AdminController@vendors')->name('vendors');  								//admin vendors 
Route::get('/adminprofile', 'AdminController@dprofile')->name('adminprofile');  					//admin dprofile 
Route::get('/dlogout', 'AdminController@dlogout')->name('dlogout');  								//admin logout 
Route::get('/mainsite', 'AdminController@mainsite')->name('mainsite');  							//admin mainsite 
Route::get('/membership', 'AdminController@membership')->name('membership');  						//admin membership 
Route::get('/orders', 'AdminController@orders')->name('orders');  								    //admin orders 
Route::get('/coupons', 'AdminController@coupons')->name('coupons');  								//admin coupons 
Route::get('/deals', 'AdminController@deals')->name('deals');  										//admin deals  
Route::get('/plans', 'AdminController@plans')->name('plans');  										//admin plans 
Route::get('/featuresedit', 'AdminController@featuresedit')->name('featuresedit');  				//admin featuresedit 
Route::get('/settings', 'AdminController@settings')->name('settings');  							//admin settings 
Route::post('featureadd', 'AdminController@featureadd')->name('featureadd'); 						//admin add membership feature
Route::get('/student', 'AdminController@student')->name('student');          						//approve user
Route::get('/locals', 'AdminController@locals')->name('locals');          						    //Local deals
Route::get('/dealhunters', 'AdminController@dealhunters')->name('dealhunters');          			//Local dealhunters
Route::get('/invoices', 'AdminController@invoices')->name('invoices');          			        //merchant invoices
Route::get('/contact', 'AdminController@contact')->name('contact');          			            //Contactus show in dashboard
Route::get('/localcomments', 'AdminController@localcomment')->name('localcomments');          	    //Localcomments show in dashboard
Route::get('/comments', 'AdminController@comment')->name('comments');          			            //merchantcomments show in dashboard
Route::get('/showblog', 'AdminController@blog')->name('showblog');          			            //show blogs in dashbaord
Route::get('/addblog', 'AdminController@addblog')->name('addblog');          			            //addblog in dashbaord
Route::get('/media', 'AdminController@media')->name('media');          			                    //Media show in dashboard
Route::post('/addmedia', 'AdminController@addmedia')->name('addmedia');          	                //save media to database
Route::get('/editblog', 'AdminController@editblog')->name('editblog');          			        //editblog in dashbaord
Route::post('/saveblog', 'AdminController@saveblog')->name('save/blog');          	                //save blog to database
Route::post('/upblog', 'AdminController@upblog')->name('upblog');          	                        //update blog to database
Route::post('/admin/inv_status', 'AdminController@inv_status')->name('admin/inv_status');          	//merchant invoices status change

/* ajax fetch and delete dashboard */
Route::get('users/fetch_data', 'AdminController@fetch_data'); 													//ajax fetch users
Route::get('vendors/fetch_data', 'AdminController@fetch_data_vendor');							    			//ajax fetch vendors
Route::get('users/removedata', 'AdminController@removedata')->name('destroy'); 									//ajax remove user
Route::get('vendors/removedata', 'AdminController@removevendor')->name('removevendor'); 						//ajax remove user


Route::post('/admindeal/edit', 'AdminController@vendordealedit')->name('admin/edit');                      		//vendordeal edit admin
Route::post('/admincoupon/edit', 'AdminController@vendorcouedit')->name('coupon/edit');                    		//vendorcoupon edit admin

Route::get('/admin/deletedeal/{id}', 'AdminController@deletedeal')->name('admin/deletedeal/{id}');         		//deal deletes admin
Route::get('/admin/deletecoupon/{id}', 'AdminController@deletecoupon')->name('admin/deletecoupon/{id}');   		//coupon deletes admin
Route::get('/deletepayment/{id}', 'AdminController@deletepayment')->name('deletepayment/{id}');            		//payment deletes admin


/* site settings */
Route::post('sitename', 'AdminController@sitename')->name('sitename'); 								       		//change site name
Route::post('icons', 'AdminController@icons')->name('icons'); 										       		//change site icons
Route::post('cont', 'AdminController@cont')->name('cont'); 										       		    //change contactus page info
Route::post('social', 'AdminController@social')->name('social'); 										        //change homepage social links
Route::post('calltoaction', 'AdminController@calltoaction')->name('calltoaction'); 								//change homepage button text 


