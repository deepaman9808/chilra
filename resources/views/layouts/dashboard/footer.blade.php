  <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; {{ date("Y") }} <a href="https://www.chilra.com" class="font-weight-bold ml-1" target="_blank">Chilra</a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="https://www.chilra.com" class="nav-link" target="_blank">Chilra</a>
              </li>
              <li class="nav-item">
                <a href="https://www.chilra.com/aboutus" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="https://www.chilra.com/blog" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://www.chilra.com/license" class="nav-link" target="_blank">License</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
