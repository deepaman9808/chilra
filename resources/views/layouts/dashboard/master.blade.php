<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Chilra</title>
  <!-- Favicon -->
  <link href="{{ url(asset('images/brand/favicon.png')) }}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ url(asset('js/dashboard/plugins/nucleo/css/nucleo.css')) }}" rel="stylesheet">
  <link href="{{ url(asset('js/dashboard/plugins/@fortawesome/fontawesome-free/css/all.min.css')) }}" rel="stylesheet" />
  <script src="{{ url(asset('js/dashboard/plugins/ckeditor/ckeditor.js')) }}"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ url(asset('css/dashboard/argon-dashboard.css?v=1.1.2')) }}" rel="stylesheet" />
  <link href="{{ url(asset('css/dashboard/argon-dashboard.css')) }}" rel="stylesheet">

</head>
<body>
    @yield('content')
      <!-- js files -->
    <script src="{{url(asset('js/jquery-3.4.1.min.js')) }}" type="text/javascript"></script>
    <script src="{{ url(asset('bootstrap/js/bootstrap.bundle.min.js')) }}"></script>
    <script src="{{ url(asset('js/dashboard/argon-dashboard.min.js?v=1.1.2')) }}"></script>
    <script src="{{ url(asset('js/dashboard/customd.js')) }}"></script>
</body>
</html>