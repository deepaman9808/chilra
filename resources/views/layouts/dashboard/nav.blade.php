<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
  <!-- collapse -->
  <div class="container-fluid">
    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <!-- Brand -->
    <ul class="nav align-items-center">
      <li class="nav-item dropdown">
        <a class="navbar-brand pt-0" href="{{('dashboard')}}">
          <img src="{{ url(asset('images/brand/logo2.png')) }}" class="navbar-brand-img" width="150px">
        </a>
      </li>
    </ul>
    <!-- User -->
    <ul class="nav align-items-center d-md-none">
      {{-- <li class="nav-item dropdown">
        <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="ni ni-bell-55"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> --}}
      <li class="nav-item dropdown">
        <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="media align-items-center">
            <span class="avatar avatar-sm rounded-circle">
              @if(Auth::guard('admin')->user()->image)
              <img alt="Image placeholder" style="height: 100%;" src="public/{{ Auth::guard('admin')->user()->image }}" >
              @else
              <img alt="Image placeholder" src="{{ asset('img') }}/theme/team-1-800x800.jpg">
              @endif
            </span>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
          <div class=" dropdown-header noti-title">
            <h6 class="text-overflow m-0">Welcome! {{auth::guard('admin')->user()->name}}</h6>
          </div>
          <a href="{{ ('adminprofile') }}" class="dropdown-item">
            <i class="ni ni-single-02"></i>
            <span>My profile</span>
          </a>
          <a href="{{ ('settings') }}" class="dropdown-item">
            <i class="ni ni-settings-gear-65"></i>
            <span>Settings</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="{{ ('dlogout') }}" class="dropdown-item">
            <i class="ni ni-user-run"></i>
            <span>Logout</span>
          </a>
        </div>
      </li>
    </ul>
    <!-- Collapse -->
    <div class="collapse navbar-collapse" id="sidenav-collapse-main">
      <!-- Collapse header -->
      <div class="navbar-collapse-header d-md-none">
        <div class="row">
          <div class="col-6 collapse-brand">
            <a href="{{ url('dashboard') }}">
              <img src="{{ url(asset('images/brand/logo2.png')) }}">
            </a>
          </div>
          <div class="col-6 collapse-close">
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
            <span></span>
            <span></span>
            </button>
          </div>
        </div>
      </div>
      <!-- Form -->
      <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="https://argon-dashboard-laravel.creative-tim.com/home">Dashboard</a>
      {{-- <form class="mt-4 mb-3 d-md-none">
        <div class="input-group input-group-rounded input-group-merge">
          <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <span class="fa fa-search"></span>
            </div>
          </div>
        </div>
      </form>  --}}
      <!-- Navigation -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link  @if(Request::path()=='dashboard') active @endif" href="{{ url('/dashboard') }}">
            <i class="fa fa-home text-primary" aria-hidden="true"></i> Dashboard
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::path()=='users') active @elseif(Request::path()=='student') active @elseif(Request::path()=='vendors') active @endif " href="#navbar-forms" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-forms">
            <i class="fa fa-user text-pink" aria-hidden="true"></i>
            <span class="nav-link-text">Users</span>
          </a>
          <div class="collapse @if(Request::path()=='users') show @elseif(Request::path()=='student') show @elseif(Request::path()=='vendors') show @endif" id="navbar-forms">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a href="{{ ('users') }}" class="nav-link @if(Request::path()=='users') active @endif">
                  <span class="sidenav-normal"> Users  </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('student') }}" class="nav-link @if(Request::path()=='student') active @endif">
                  <span class="sidenav-normal"> Students  </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('vendors') }}" class="nav-link @if(Request::path()=='vendors') active @endif">
                  <span class="sidenav-normal"> Vendors </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::path()=='locals') active @elseif(Request::path()=='dealhunters') active @elseif(Request::path()=='localcomments') active @endif " href="#navbar-formsf" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-formsf">
            <i class="fa fa-location-arrow text-orange" aria-hidden="true"></i>
            <span class="nav-link-text"> Locals</span>
          </a>
          <div class="collapse @if(Request::path()=='locals') show @elseif(Request::path()=='dealhunters') show @elseif(Request::path()=='localcomments') show @endif" id="navbar-formsf">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a href="{{ ('locals') }}" class="nav-link @if(Request::path()=='locals') active @endif">
                  <span class="sidenav-normal"> Deals  </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('dealhunters') }}" class="nav-link @if(Request::path()=='dealhunters') active @endif">
                  <span class="sidenav-normal"> Deal Hunters  </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('localcomments') }}" class="nav-link @if(Request::path()=='localcomments') active @endif">
                  <span class="sidenav-normal"> Comments </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::path()=='coupons') active @elseif(Request::path()=='deals') active @elseif(Request::path()=='comments') active @endif " href="#navbar-mer" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-mer">
            <i class="fa fa-bars text-yellow" aria-hidden="true"></i>
            <span class="nav-link-text">Merchant</span>
          </a>
          <div class="collapse @if(Request::path()=='deals') show @elseif(Request::path()=='coupons') show @elseif(Request::path()=='comments') show  @endif" id="navbar-mer">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a href="{{ ('deals') }}" class="nav-link @if(Request::path()=='deals') active @endif">
                  <span class="sidenav-normal"> Deals </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('coupons') }}" class="nav-link @if(Request::path()=='coupons') active @endif">
                  <span class="sidenav-normal"> Coupons </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('comments') }}" class="nav-link @if(Request::path()=='comments') active @endif">
                  <span class="sidenav-normal"> Comments </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::path()=='plans') active @endif" href="{{ url('/plans') }}">
            <i class="fa fa-shopping-cart text-green" aria-hidden="true"></i> Plans
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::path()=='payments') active @elseif(Request::path()=='payments') active @elseif(Request::path()=='invoices') active @endif " href="#navbar-me" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-me">
            <i class="fa fa-usd text-info" aria-hidden="true"></i>
            <span class="nav-link-text">Payments</span>
          </a>
          <div class="collapse @if(Request::path()=='membership') show @elseif(Request::path()=='orders') show @elseif(Request::path()=='invoices') show @endif" id="navbar-me">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a href="{{ ('membership') }}" class="nav-link @if(Request::path()=='membership') active @endif">
                  <span class="sidenav-normal"> Membership  </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('orders') }}" class="nav-link @if(Request::path()=='orders') active @endif">
                  <span class="sidenav-normal"> Orders </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('invoices') }}" class="nav-link @if(Request::path()=='invoices') active @endif">
                  <span class="sidenav-normal"> Monthly Invoices </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link  @if(Request::path()=='contact') active @endif" href="{{ url('/contact') }}">
            <i class="fa fa-envelope-open-o text-blue" aria-hidden="true"></i> Contact Us
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::path()=='addblog') active @elseif(Request::path()=='showblog') active @elseif(Request::path()=='media') active @endif " href="#navbar-mem" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-mem">
            <i class="fa fa-rss text-orange" aria-hidden="true"></i>
            <span class="nav-link-text">Blog</span>
          </a>
          <div class="collapse @if(Request::path()=='addblog') show @elseif(Request::path()=='showblog') show @elseif(Request::path()=='media') show @endif" id="navbar-mem">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a href="{{ ('showblog') }}" class="nav-link @if(Request::path()=='showblog') active @endif">
                  <span class="sidenav-normal"> Blogs  </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('addblog') }}" class="nav-link @if(Request::path()=='addblog') active @endif">
                  <span class="sidenav-normal"> Add Blog </span>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ ('media') }}" class="nav-link @if(Request::path()=='media') active @endif">
                  <span class="sidenav-normal"> Add Media </span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link @if(Request::path()=='settings') active @endif" href="{{ url('/settings') }}">
            <i class="fa fa-cogs text-pink" aria-hidden="true"></i> Settings
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="main-content">
  <!-- Navbar -->
  <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
      <!-- Form -->
      <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="https://argon-dashboard-laravel.creative-tim.com/home">Dashboard</a>
     {{--  <form class="navbar-search navbar-search-light form-inline mr-sm-3">
        <div class="form-group mb-0">
          <div class="input-group input-group-alternative input-group-merge">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Search" type="text">
          </div>
        </div> 
      </form> --}}
      <!-- User -->
      <ul class="navbar-nav align-items-center d-none d-md-flex">
        <li class="nav-item dropdown">
        <li class="nav-item dropdown">
          <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                @if(Auth::guard('admin')->user()->image)
                <img alt="Image placeholder" src="public/{{ Auth::guard('admin')->user()->image}}" style="height:100%">
                @else
                <img alt="Image placeholder" src="{{ asset('img') }}/theme/team-1-800x800.jpg" >
                @endif
              </span>
              <div class="media-body ml-2 d-none d-lg-block">
                <span class="mb-0 text-sm  font-weight-bold">Admin</span>
              </div>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome! {{auth::guard('admin')->user()->name}}</h6>
            </div>
            <a href="{{ ('adminprofile') }}" class="dropdown-item">
              <i class="fa fa-user" aria-hidden="true"></i>
              <span>My profile</span>
            </a>
            <a href="{{ ('settings') }}" class="dropdown-item">
              <i class="fa fa-cogs" aria-hidden="true"></i>
              <span>Settings</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="{{ ('dlogout') }}" class="dropdown-item">
              <i class="fa fa-sign-out" aria-hidden="true"></i>
              <span>Logout</span>
            </a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <div class="loader"></div>