<header class="topbar" data-navbarbg="skin6">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <div class="navbar-header" data-logobg="skin5">
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </a>
            <div class="navbar-brand">
                <a href="{{ url('') }}" class="logo">
                    <b class="logo-icon">
                    <img src="{{ url('public/images/brand/logo2.png')}}" height="40px" width="170px">
                    </b>
                </a>
            </div>
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-search" aria-hidden="true"></i>
            </a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin6">
            <ul class="navbar-nav float-left mr-auto">
                {{-- <li class="nav-item search-box">
                    <a class="nav-link waves-effect waves-dark" href="javascript:void(0)">
                        <div class="d-flex align-items-center">
                            <i class="fa fa-search font-20 mr-1 mt-3" aria-hidden="true"></i>
                            <div class="ml-1 d-none d-sm-block">
                                <span>Search</span>
                            </div>
                        </div>
                    </a>
                    <form class="app-search position-absolute">
                        <input type="text" class="form-control" placeholder="Search &amp; enter">
                        <a class="srh-btn">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                    </form>
                </li> --}}
            </ul>
            <ul class="navbar-nav float-right">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ url(asset(session()->get('image'))) }}" alt="user" class="rounded-circle" height="37px" width="37px">
                        <span class="m-l-5 font-medium d-none d-sm-inline-block">{{ Auth::guard('vendor')->user()->vendor_name }}<i class="fa fa-angle-down ml-1"></i></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                        <span class="with-arrow">
                            <span class="bg-primary"></span>
                        </span>
                        <div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
                            <div class="">
                                <img src="{{ url(asset(session()->get('image'))) }}" alt="user" class="rounded-circle" height="50px" width="50px">
                            </div>
                            <div class="m-l-10">
                                <h4 class="m-b-0">{{ Auth::guard('vendor')->user()->vendor_name }}</h4>
                                <p class=" m-b-0">{{ Auth::guard('vendor')->user()->email }}</p>
                            </div>
                        </div>
                        <div class="profile-dis scrollable ps-container ps-theme-default">
                            <a class="dropdown-item" href="{{ url('merchant/profile') }}">
                            <i class="fa fa-user m-r-5 m-l-5"></i> My Profile</a>
                            <a class="dropdown-item" href="{{url('merchant')}}">
                                <i class="fa fa-inr m-r-5 m-l-5"></i> My Balance
                            </a>
                            <a class="dropdown-item" href="{{ url('merchant/business') }}">
                                <i class="fa fa-cogs m-r-5 m-l-5"></i> Store Settings</a>
                           {{--  <div class="dropdown-divider"></div> --}}
                            {{-- <a class="dropdown-item" href="{{ url('mlogout')}}">
                                <i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                            <div class="dropdown-divider"></div> --}}
                            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                                <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                            </div>
                            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;">
                                <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div>
                            </div>
                            <div class="p-l-30 p-10">
                                <a href="{{ url('mlogout') }}" class="btn btn-sm btn-danger btn-rounded">Logout</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
</header>
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant') }}" aria-expanded="false">
                        <i class="fa fa-tachometer" aria-hidden="true"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/deals') }}" aria-expanded="false">
                        <i class="fa fa-clone" aria-hidden="true"></i>
                        <span class="hide-menu">Deals</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/business') }}" aria-expanded="false">
                        <i class="fa fa-coffee"></i>
                        <span class="hide-menu">Business</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/coupons') }}" aria-expanded="false">
                        <i class="fa fa-ticket" aria-hidden="true"></i>
                        <span class="hide-menu">Coupons</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/reports') }}" aria-expanded="false">
                        <i class="fa fa-bar-chart" aria-hidden="true"></i>
                        <span class="hide-menu">Reports</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/membership') }}" aria-expanded="false">
                        <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
                        <span class="hide-menu">Membership</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/payments') }}" aria-expanded="false">
                        <i class="fa fa-money" aria-hidden="true"></i>
                        <span class="hide-menu">Payments</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/invoice') }}" aria-expanded="false">
                        <i class="fa fa-inr"></i>
                        <span class="hide-menu">Invoices</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ url('merchant/support') }}" aria-expanded="false">
                        <i class="fa fa-life-ring" aria-hidden="true"></i>
                        <span class="hide-menu">Support</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>