<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon icon -->
        <title>Chilra</title>

        <link href="{{ url(asset('images/brand/favicon.png')) }}" rel="icon" type="image/png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
         {{-- <link href="{{url('public/dist/libs/chartist/dist/chartist.min.css') }}" rel="stylesheet"> --}} 

        @if(last(request()->segments())=='register' || last(request()->segments())=='login')
            <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
            <link href="{{url(asset('css/merchant/bootstrap.min.css')) }}" rel="stylesheet">
            <link href="{{url(asset('css/merchant/wizard.css')) }}" rel="stylesheet">
            <link href="{{url(asset('css/merchant/demo.css')) }}"   rel="stylesheet">
        @else
            <link href="{{url(asset('css/merchant/style.min.css')) }}" rel="stylesheet">
            <link href="{{url(asset('css/merchant/custom.css')) }}" rel="stylesheet"> 
        @endif

    </head>
    <body>
        @yield('content')

        <script src="{{url(asset('js/jquery-3.4.1.min.js'))}}" type="text/javascript"></script>
        <script src="{{url(asset('bootstrap/js/bootstrap.min.js')) }}" type="text/javascript"></script>
        <script src="{{url(asset('js/merchant/jquery.bootstrap.js')) }}" type="text/javascript"></script>

        <script src="{{url(asset('js/merchant/material-bootstrap-wizard.js')) }}"></script>
        <script src="{{url(asset('js/merchant/jquery.validate.min.js')) }}"></script>

        <script src="{{url(asset('js/merchant/sparkline.js')) }}"></script>
        <script src="{{url(asset('js/merchant/waves.js')) }}"></script>
        <script src="{{url(asset('js/merchant/sidebarmenu.js')) }}"></script>
        <script src="{{url(asset('js/merchant/custom.min.js')) }}"></script>
        <script src="{{url(asset('js/merchant/customm.js')) }}"></script>
        <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    </body>
</html>