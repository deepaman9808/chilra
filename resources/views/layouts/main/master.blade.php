<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Chilra</title>
		<link href="{{ url(asset('images/brand/favicon.png')) }}" rel="icon" type="image/png">
		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
		<!-- Icons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link href="{{ url(asset('bootstrap/css/bootstrap.min.css')) }}" rel="stylesheet">
		<link href="{{ url(asset('css/main/style.css')) }}" rel="stylesheet">
		<script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/5eb7f938967ae56c52186f63/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script>
	</head>
	<body>
		@yield('content')
		<!-- js files -->
		<script src="{{url(asset('js/jquery-3.4.1.min.js')) }}" type="text/javascript"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
		<script src="{{ url(asset('bootstrap/js/bootstrap.min.js')) }}" type="text/javascript"></script>
		<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
		<script src="{{ url(asset('js/main/jquery.countdown.min.js')) }}"></script>
		<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
		<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
		<script src="{{ url(asset('js/main/custom.js')) }}"></script>
		
	</body>
</html>