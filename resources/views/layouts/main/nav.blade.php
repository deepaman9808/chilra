<section class="home-nav">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-lg-6 col-sm-12 p-0">
				<nav class="navbar navbar-expand-md navbar-dark">
					<button class="navbar-toggler bg-white text-dark" type="button"  aria-label="Toggle navigation">
					<a class="navbar-brand" href="{{url('')}}">
						<img src="{{ url('public/images/brand/logo2.png')}}" alt="logo">
					</a>
					</button>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="true" aria-label="Toggle navigation">
					<i class="fa fa-map-marker paddingcust text-white" area-hidden="true"></i>
					</button>
					<button class="navbar-toggler" @if(Auth::guard('web')->check()) style="background: #5ba835;color: #fff;" @else style="color: #27282c;background: #fff;"    @endif type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="true" aria-label="Toggle navigation">
					<i class="fa fa-user paddingcust" area-hidden="true"></i>
					</button>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar3">
					<span class="fa fa-home text-white paddingcust" area-hidden="true"></span>
					</button>
					<div class="navbar-collapse collapse" id="navbarsExample03">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item ">
								<div class="loc-home">
									<i class="fa fa-dot-circle-o" aria-hidden="true"></i>
									<div class="location d-inline-block ml-2 text-muted p-2" style="font-size: 14px;">
										<span id="city">Current City</span>
									</div>
								</div>
							</li>
							<li class="nav-item">
								<div class="cart-home">
									<i class="fa fa-map-marker" aria-hidden="true"></i>
									<div class="location d-inline-block ml-2 text-muted p-2" style="font-size: 14px;">
										<span id="state">Current State</span>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</nav>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-12">
				<nav class="navbar navbar-expand-md navbar-dark p-0">
					<div class="navbar-collapse collapse accountarea" id="navbarsExample04">
						<ul class="navbar-nav">
							@if(Auth::user()=='')
							<li class="nav-item">
								<a href="{{ url('login') }}" class="btn"><i class="fa fa-sign-out icon-margin"></i>LOGIN</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('register') }}" class="btn"><i class="fa fa-unlock icon-margin"></i>REGISTER</a>
							</li>
							@else
							<li class="nav-item">
								<a href="{{ url('profile') }}" class="btn"><i class="fa fa-user icon-margin"></i>MY PROFILE</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('logout') }}" class="btn"><i class="fa fa-sign-out icon-margin"></i>LOGOUT</a>
							</li>
							@endif
							<li class="nav-item">
								<a href="//{{$fb}}" class="btn facebook" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li class="nav-item">
								<a href="//{{$tw}}" class="btn twitter"  target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li class="nav-item">
								<a href="//{{$gp}}" class="btn google"   target="_blank"><i class="fa fa-google-plus"></i></a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</section>
<section class="home-head">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<nav class="navbar navbar-expand-md navbar-light">
					<div class="collapse navbar-collapse" id="collapsibleNavbar3">
						<div class="logo mr-auto">
							<a class="navbar-brand" href="{{url('')}}">
							<img src="{{url(asset('images/brand/13077.png'))}}" width="90" height="50" alt="logo"></a>
						</div>
						<div class="header-right">
							<ul class="navbar-nav">
								<li class="nav-item">
									<a class="nav-link" href="{{url('')}}">HOME</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{url('home/alldeals')}}">ALL</a>
								</li>
								<li class="nav-item dropdown">
							      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">DEALS</a>
							      <div class="dropdown-menu" style="min-width: 15rem;top: 85%">
							      	@foreach($cats as $ct)
							        <a class="dropdown-item text-capitalize" 
								        style="font-size: 13px;font-weight:600;" 
								        onMouseOver="this.style.color='#fff',this.style.background='#5ba835'" 
								        onMouseOut="this.style.color='rgba(0, 0, 0, 0.85)',this.style.background='#fff'" 
								        href="{{ url('home/deals?cat=') }}{{$ct->id}}">{{$ct->cat}}
								        <span class="count pull-right">{{$ct->count}}</span>
							        </a>
							        @endforeach
							      </div>
							    </li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">COUPONS</a>
									<div class="dropdown-menu" style="min-width: 15rem;top: 85%">
							      	@foreach($catscp as $cts)
							        <a class="dropdown-item text-capitalize" 
								        style="font-size: 13px;font-weight:600;" 
								        onMouseOver="this.style.color='#fff',this.style.background='#5ba835'" 
								        onMouseOut="this.style.color='rgba(0, 0, 0, 0.85)',this.style.background='#fff'" 
								        href="{{ url('home/coupons?cat=') }}{{$cts->id}}">{{$cts->cat}}
								        <span class="count pull-right">{{$cts->cpo}}</span>
							        </a>
							        @endforeach
							      </div>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{url('home/allstore')}}">STORES</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{url('home/blog')}}">BLOG</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="{{url('/contactus')}}">CONTACT US</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
</section>