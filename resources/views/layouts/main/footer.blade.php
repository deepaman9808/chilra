<div class="footer">
  <div class="widget-footer home-section-eight">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="widget white-block">
            <div class="widget-title">
              <h5 class="text-white mt-5 mb-3">User Area Pages</h5>
            </div>
            <div class="white-block-content">
              <ul class="list-unstyled">
                <li><a href="{{url('/login')}}" class="hj">Sign In</a></li>
                <li><a href="{{url('/register')}}" class="hj">Register</a></li>
                <li><a href="{{url('/profile')}}" class="hj">Profile</a></li>
                <li><a href="{{url('/local')}}" class="hj">Local Deals</a></li>
                <li><a href="{{url('/dealhunters')}}" class="hj">All Deal Hunters</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="widget white-block">
            <div class="widget-title">
              <h5 class="text-white mt-5 mb-3">Custom Navigation</h5>
              <div class="white-block-content">
                <ul class="list-unstyled">
                  <li><a href="{{url('')}}" class="hj">Home</a></li>
                  <li><a href="{{url('/contactus')}}" class="hj">Contact Us</a></li>
                  <li><a href="{{url('/home/blog')}}" class="hj">Blogs</a></li>
                  <li><a href="{{url('/home/allstore')}}" class="hj">All Stores</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="widget white-block">
            <div class="widget-title">
              <h5 class="text-white mt-5 mb-3">Merchant Area Pages</h5>
              <div class="white-block-content">
                <ul class="list-unstyled">
                  <li><a href="{{url('/merchant/login')}}" class="hj">Sign In</a></li>
                  <li><a href="{{url('/merchant/register')}}" class="hj">Register</a></li>
                  <li><a href="{{url('home/alldeals')}}" class="hj">All coupons/deals</a></li>
                  <li><a href="{{url('/home/deals')}}" class="hj">Deals</a></li>
                  <li><a href="{{url('/home/coupons')}}" class="hj">Coupons</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="to-top">
            <a><i class="fa fa-angle-double-up top"></i></a>
          </div>
          <div class="pull-lefts">
            © All rights reserved <a href="{{url('')}}"class="powr">Chilra</a> Laravel Code by <a href="#" class="powr">Xtreem Web Solution</a>.
          </div>
          <div class="pull-rights mt-3 text-center">
            <a href="//{{$fb}}" class="btn facebook" target="_blank"><i class="fa fa-facebook k2"></i></a>
            <a href="//{{$tw}}" class="btn twitter" target="_blank"><i class="fa fa-twitter k2"></i></a>
            <a href="//{{$gp}}" class="btn google" target="_blank"><i class="fa fa-google-plus k2"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>