@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
    @include('layouts.merchant.navbar')
    <div class="page-wrapper">
        @include('layouts.merchant.breadcrumb')
        <div class="container-fluid">
            <div class="card-group">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <i class="fa fa-smile-o font-20 text-muted"></i>
                                        <p class="font-16 m-b-5">Deals View</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h1 class="font-light text-right">{{$sum}}</h1>
                                    </div>
                                </div>
                            </div>
                          {{--   <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 75%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <i class="fa fa-picture-o font-20  text-muted"></i>
                                        <p class="font-16 m-b-5">Deals Consumed</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h1 class="font-light text-right">{{ $deal }}/{{ $feature->$plan}}</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ $deal*2 }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <i class="fa fa-inr font-20 text-muted"></i>
                                        <p class="font-16 m-b-5">Pending Invoice</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h1 class="font-light text-right">{{$invoice}}</h1>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-purple" role="progressbar" style="width: {{$invoice}}%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="d-flex no-block align-items-center">
                                    <div>
                                        <i class="fa fa-ticket font-20 text-muted"></i>
                                        <p class="font-16 m-b-5">Coupons Consumed</p>
                                    </div>
                                    <div class="ml-auto">
                                        <h1 class="font-light text-right">{{ $coupon}}</h1>
                                    </div>
                                </div>
                            </div>
                           {{--  <div class="col-12">
                                <div class="progress">
                                    <div class="progress-bar bg-danger" role="progressbar" style="width:{{ $coupon*2}}%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Sales Ratio</h4>
                            <div class="mt-3" id="saless" style="height:250px;width: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title m-b-5">Total Earnings</h5>
                            <h3 class="font-light">₹{{$er}}</h3>
                            <div class="m-t-20 text-center">
                                <div id="earnings"></div>
                            </div>
                        </div>
                    </div>
                    <div class="card ">
                        <div class="card-body">
                            <h5 class="card-title m-b-5">Membership Plan</h5>
                            <i class="fa fa-trophy" style="font-size: 35px;"></i>
                            <div class="ml-auto">
                                <h1 class="font-light text-right text-capitalize">{{ $plan }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- column -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Latest Sales</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">NAME</th>
                                        <th class="border-top-0">DEAL ID</th>
                                        <th class="border-top-0">PAYMENT ID</th>
                                        <th class="border-top-0">AMOUNT</th>
                                        <th class="border-top-0">EMAIL</th>
                                        <th class="border-top-0">PHONE</th>
                                        <th class="border-top-0">DATE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order as $or)
                                    <tr>
                                        <td>{{$or->name}}</td>
                                        <td>{{$or->deal_id}}</td>
                                        <td>{{$or->payment_id}}</td>
                                        <td><span class="font-medium">₹{{$or->amount}}</span></td>
                                        <td>{{$or->email}}</td>
                                        <td>{{$or->phone}}</td>
                                        <td>{{$or->created_at}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- column -->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Recent Reviews/Comments</h4>
                        </div>
                        <div class="comment-widgets" >
                            @foreach($comm as $key=>$value)
                            <div class="d-flex flex-row comment-row m-t-0">
                                <div class="p-2">
                                    <img src="{{url(asset($value->image))}}" alt="user" width="50" height="50" class="rounded-circle">
                                </div>
                                <div class="comment-text w-100">
                                    <h6 class="font-medium">{{$value->name}}</h6>
                                    <span class="m-b-15 d-block">{{$value->comment}}</span>
                                    <div class="comment-footer">
                                        <span class="text-muted float-right">{{$value->created_at->format('d-M-Y g:ia')}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Recent Ratings</h4>
                        </div>
                        <div class="comment-widgets" >
                            @foreach($rate as $key=>$del)
                            <div class="d-flex flex-row comment-row m-t-0">
                                <div class="p-2">
                                    <img src="{{url(asset($del->image))}}" alt="user" width="50" height="50" class="rounded-circle">
                                </div>
                                <div class="comment-text w-100">
                                    <h6 class="font-medium">{{$del->name}}</h6>
                                    <span class="m-b-15 d-block">Gives 
                                        @if(1 || 2 || 3 || 4 || 5 == $del->review)
                                          @for($i=1;$i<=$del->review;$i++)
                                            <i class="fa fa-star" style="color: #FF912C;" backup-class="fa fa-star"></i>
                                          @endfor
                                          @for($i=$del->review;$i<5;$i++)
                                          <i class="fa fa-star-o" aria-hidden="true"></i>
                                          @endfor
                                          @endif ratings</span>
                                    <div class="comment-footer">
                                        <span class="text-muted float-right">{{$del->created_at->format('d-M-Y g:ia')}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
        </div>
    </div>
    @include('layouts.merchant.footer');
</div>
<input type="hidden" value="{{$i=0}}">
<input type="hidden" value="{{$z=1}}">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
              ['Count', 'Devices'],
               @foreach($py as $cpp)
                   ['{{ $cpp[$i] }}', {{ $cpp[$z] }}],
               @endforeach
        ]);
          

        var options = {
          title: '{{ date('F') }} Month Sales',
          hAxis: {title: 'Month',  titleTextStyle: {color: '#333'}},
          is3D: true,
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('saless'));
        chart.draw(data, options);
      }
    </script>
{{-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script type="text/javascript">
window.onload = function () {
var charts = new CanvasJS.Chart("saless", {
theme: "light1", // "light2", "dark1", "dark2"
axisY: {
title: "Sales"
},
animationEnabled: true, // change to true
title:{
text: "{{ date('F') }} Month Sales"
},
data: [
{
// Change type to "bar", "area", "spline", "pie",etc.
type: "spline",
dataPoints: [
@foreach($py as $cpp)
{ label: "{{ $cpp[$i] }}",  y: {{ $cpp[$z] }}   },
@endforeach

]

}
]
});
charts.render();
}
</script> --}}
@endsection