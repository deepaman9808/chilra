@extends('layouts.dashboard.master')
@section('content')
@include('layouts.dashboard.nav')
@include('layouts.dashboard.header')

<div class="container-fluid mt--7">
	<div class="row">
		<div class="col-xl-12 mb-5 mb-xl-0">
			<div class="card  shadow">
				<div class="card-header bg-transparent">
					<div class="row align-items-center">
						<div class="col">
							<h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
							<h2 class="text-white mb-0">Sales value</h2>
							<div id="paymentcontainer" style="height: 370px; width: 100%;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-xl-7 mb-5 mb-xl-0">
			<div class="card shadow">
				<div class="card-header border-0">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0">Recent Users</h3>
						</div>
						<div class="col text-right">
							<a style="color:#fff" href="{{ url('users') }}" class="btn btn-sm bg-gradient-blue ">See all</a>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<!-- Projects table -->
					<table class="table align-items-center table-flush">
						<thead class="thead-light">
							<tr>
								<th scope="col">Avatar</th>
								<th scope="col">User Name</th>
								<th scope="col">Email</th>
								<th scope="col">Social Login</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($userdata as $user)
							<tr>
								<th scope="row">
									<div class="avatar-group">
										<a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="{{ $user->name }}">
											<img alt="Image placeholder" style="height: 100%" src="{{ asset($user->image) }}" class="rounded-circle">
										</a>
									</div>
								</th>
								<td>
									{{ $user->name }}
								</td>
								<td>
									<span class="badge badge-dot mr-4">
										<i class="bg-warning"></i>
										@if(!$user->email)
										Null
										@else
										{{ $user->email }}
										@endif
									</span>
								</td>
								<td>
									<div class="media-body">
										<span class="mb-0 text-sm">{{ $user->provider }}</span>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xl-5 mb-5 mb-xl-0">
			<div class="card  shadow">
				<div class="card-header bg-transparent">
					<div class="row align-items-center">
						<div class="col">
							<h6 class="text-uppercase text-light ls-1 mb-1">Overview</h6>
							<h2 class="text-white mb-0">Sales value</h2>
							<div id="devices" style="height: 370px; width: 100%;"></div>
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div>
	<!-- Footer -->
	@include('layouts.dashboard.footer')
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        	  ['Year', 'Sales'],
          ['Jan', {{ $val[1] }}],
          ['Feb',  {{ $val[2] }}],
          ['March',  {{ $val[3] }}],
          ['April',  {{ $val[4] }}],
          ['June',  {{ $val[5] }}],
          ['July',  {{ $val[6] }}],
          ['August',  {{ $val[7] }}],
          ['September',  {{ $val[8] }}],
          ['October',  {{ $val[9] }}],
          ['November',  {{ $val[10] }}],
          ['December',  {{ $val[11] }}],
        ]);
          

        var options = {
          title: 'This Year Revenue',
          hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('paymentcontainer'));
        chart.draw(data, options);
      }
    </script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
        	  ['Count', 'Devices'],
	           @foreach($device as $cpp)
	               ['{{ $cpp->device }}', {{ $cpp->count }}],
		       @endforeach
        ]);
          

        var options = {
          title: 'User Devices',
          hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
          is3D: true,
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.PieChart(document.getElementById('devices'));
        chart.draw(data, options);
      }
    </script>
{{-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
<script type="text/javascript">
window.onload = function () {
	
var chart = new CanvasJS.Chart("paymentcontainer", {
theme: "light2", // "light2", "dark1", "dark2"
axisY: {
         title: "Sales"
       },
       animationEnabled: true, // change to true
    title:{
           text: "Sales This Year"
         },
data: [
{
// Change type to "bar", "area", "spline", "pie",etc.
type: "spline",
dataPoints: [
    @if($val==0)
         { label: "No Payments Found",   },
    @else
		 { label: "Jan",  y: {{ $val[1] }}  },
		 { label: "Feb", y: {{ $val[2] }}  },
		 { label: "March", y: {{ $val[3] }}  },
		 { label: "April",  y: {{ $val[4] }}  },
		 { label: "May",  y: {{ $val[5] }}  },
		 { label: "June",  y: {{ $val[6] }}  },
		 { label: "July", y: {{ $val[7] }}  },
		 { label: "August", y: {{ $val[8] }}  },
		 { label: "September",  y: {{ $val[9] }}  },
		 { label: "October",  y: {{ $val[10] }}  },
		 { label: "November",  y: {{ $val[11] }}  },
		 { label: "December",  y: {{ $val[12] }}  }
    @endif
  ]
 }
]
});
chart.render();


//devices
var charts = new CanvasJS.Chart("devices", {
theme: "light1", // "light2", "dark1", "dark2"
axisY: {
		title: "Sales"
		},
		animationEnabled: true, // change to true
	title:{
		text: "User Devices"
		},
	data: [
		{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "pie",
	dataPoints: [
		         @if($device->isEmpty())
		           { label: "No Results",   },
		         @else
		         @foreach($device as $cpp)
		           { label: "{{ $cpp->device }}",  y: {{ $cpp->count }}   },
		         @endforeach
		         @endif
		       ]
       }
    ]
});
charts.render();

</script> --}}
@endsection