@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
    @include('layouts.merchant.navbar')
    <div class="page-wrapper">
        @include('layouts.merchant.breadcrumb')
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-7">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">STARTER</h4>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $del)
                                        <tr id="memdata">
                                            <td class="txt-oflo text-capitalize">{{ $del->name }} : <div  class="pull-right"> {{ $del->starter }}</div></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                      @if($ship->plan=='starter')
                                        <th class="border-top-0 text-center">
                                            <a href="javascript:void(0)" class="btn btn-success" role="button" >ACTIVE</a>
                                        </th>
                                        @else
                                         <th class="border-top-0 text-center">
                                            <a href="javascript:void(0)" disabled class="btn btn-warning" role="button">Other Plan Active</a>
                                        </th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">COMPETITOR</h4>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $del)
                                        <tr id="memdata">
                                            <td class="txt-oflo text-capitalize">{{ $del->name }} : <div  class="pull-right">{{ $del->competitor }}</div></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        @if($ship->plan=='competitor')
                                        <th class="border-top-0 text-center">
                                            <a href="javascript:void(0)" class="btn btn-success" role="button" >ACTIVE</a>
                                        </th>
                                        @elseif($ship->plan=='champion')
                                         <th class="border-top-0 text-center">
                                            <a href="javascript:void(0)" disabled class="btn btn-warning" role="button">Other Plan Active</a>
                                        </th>
                                        @else
                                         <th class="border-top-0 text-center">
                                            <a href="javascript:void(0)" disabled class="btn btn-danger buy_now" data-plan="competitor" data-image="{{ url(asset('img/13077.png')) }}" data-plan="competitor" data-email="{{ auth::guard('vendor')->user()->email }}" data-id='{{ auth::guard('vendor')->user()->merchant_id }}' data-phone="{{ auth::guard('vendor')->user()->phone }}"   data-amount='{{$price->competitor}}'  role="button">UPGRADE</a>
                                        </th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">CHAMPION</h4>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $del)
                                        <tr id="memdata">
                                            <td class="txt-oflo text-capitalize">{{ $del->name }} : <div  class="pull-right">{{ $del->champion }}</div></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        @if($ship->plan=='champion')
                                        <th class="border-top-0 text-center">
                                            <a href="javascript:void(0)" class="btn btn-success" role="button">ACTIVE</a>
                                        </th>
                                        @else
                                        <th class="border-top-0 text-center">
                                            <a href="javascript:void(0)" disabled class="btn btn-danger buy_now" data-plan="champion" 
                                            data-image="{{ url(asset('img/13077.png')) }}" data-plan="champion" data-email="{{ auth::guard('vendor')->user()->email }}" data-id='{{ auth::guard('vendor')->user()->merchant_id }}' data-phone="{{ auth::guard('vendor')->user()->phone }}"   data-amount='{{$price->champion}}'  role="button">UPGRADE</a>
                                        </th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   @include('layouts.merchant.footer');
</div>
@endsection