@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
    @include('layouts.merchant.navbar')
    <div class="page-wrapper">
        @include('layouts.merchant.breadcrumb')
        <div class="container-fluid">
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <i class="fa fa-smile-o font-20 text-muted"></i>
                                            <p class="font-16 m-b-5">Deal View (Today)</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h1 class="font-light text-right">{{$view}}</h1>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 75%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <i class="fa fa-image font-20  text-muted"></i>
                                            <p class="font-16 m-b-5">Deals Purchased (Today)</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h1 class="font-light text-right">{{ $st }}</h1>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                  {{--   <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <i class="mdi mdi-currency-eur font-20 text-muted"></i>
                                            <p class="font-16 m-b-5">Pending Invoice (Today)</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h1 class="font-light text-right">0</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-purple" role="progressbar" style="width: 65%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>--}}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <i class="fa fa-ticket font-20 text-muted"></i>
                                            <p class="font-16 m-b-5">Coupons Used (Today)</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h1 class="font-light text-right">{{$ts}}</h1>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar bg-danger" role="progressbar" style="width:45%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Top Grossing Deals</h4>
                            <div id="dealcontainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
                 <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Top Grossing Coupons</h4>
                            <div id="couponcontainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Cross Platform Share</h4>
                            <div id="sharedcontainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Payment Details</h4>
                            <div id="paymentcontainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.merchant.footer');
</div>

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
<script type="text/javascript">
window.onload = function () {

var charts = new CanvasJS.Chart("dealcontainer", {
theme: "light1", // "light2", "dark1", "dark2"
axisY: {
        title: "Sales"
      },
animationEnabled: true, // change to true
title:{
        text: "Top 5 Deals Of The {{date('F')}}"
      },
data: [
       {
         // Change type to "bar", "area", "spline", "pie",etc.
        type: "column",

dataPoints: [
                @if($stl->isEmpty())
                { label: "No Deals Found",   },
                @else
                  @foreach($stl as $cpp)
                   { label: "{{ $cpp->name }}",  y: {{ $cpp->five }}   },
                  @endforeach
                @endif
            ]
               
         }        
       ]
});
charts.render();


var charti = new CanvasJS.Chart("couponcontainer", {
theme: "light1", // "light2", "dark1", "dark2"
axisY: {
        title: "Sales"
      },
animationEnabled: true, // change to true
title:{
        text: "Top 5 Coupons Of The {{date('F')}}"
      },
data: [
       {
         // Change type to "bar", "area", "spline", "pie",etc.
        type: "column",

dataPoints: [
               
                @if($lts->isEmpty())
                { label: "No Coupons Found",   },
                @else
                  @foreach($lts as $cpp)
                   { label: "{{ $cpp->name }}",  y: {{ $cpp->count }}   },
                  @endforeach
                @endif
               
            ]
               
         }        
       ]
});
charti.render();


var chart = new CanvasJS.Chart("paymentcontainer", {
theme: "light2", // "light2", "dark1", "dark2"
axisY: {
        title: "Sales"
    },
animationEnabled: true, // change to true
title:{
text: "Sales This Year"
},
data: [
{
// Change type to "bar", "area", "spline", "pie",etc.
type: "spline",

dataPoints: [
                @if($val==0)
                { label: "No Payments Found",   },
                @else
                { label: "jan",  y: {{ $val[0] }}  },
                { label: "feb", y: {{ $val[1] }}  },
                { label: "march", y: {{ $val[2] }}  },
                { label: "april",  y: {{ $val[3] }}  },
                { label: "may",  y: {{ $val[4] }}  },
                { label: "june",  y: {{ $val[5] }}  },
                { label: "july", y: {{ $val[6] }}  },
                { label: "august", y: {{ $val[7] }}  },
                { label: "september",  y: {{ $val[8] }}  },
                { label: "october",  y: {{ $val[9] }}  },
                { label: "november",  y: {{ $val[10] }}  },
                { label: "december",  y: {{ $val[11] }}  }
                @endif
            ]
}
]
});
chart.render();

var chart = new CanvasJS.Chart("sharedcontainer", {
theme: "light2", // "light2", "dark1", "dark2"
axisY: {
        title: "Sales"
    },
animationEnabled: true, // change to true
title:{
text: "Shared Deals Data"
},
data: [
{
// Change type to "bar", "area", "spline", "pie",etc.
type: "pie",

dataPoints: [
                @if($share->isEmpty())
                { label: "No Data Found",   },
                @else
                  @foreach($share as $cpp)
                   { label: "{{ $cpp->platform }}",  y: {{ $cpp->count }}   },
                  @endforeach
                @endif
            ]
}
]
});
chart.render();
}

</script>
@endsection