@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
    @include('layouts.merchant.navbar')
    <div class="page-wrapper">
        @include('layouts.merchant.breadcrumb')
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-7">
                    @if (count($errors->deal) > 0)
                    @foreach ($errors->deal->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                    @endforeach
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <!-- edit Modal -->
            <div class="modal" id="editmodaldl">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Enter Deal Info</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form class="form-horizontal m-t-30" method="POST" action="{{ url('merchant/dealupdate') }}" enctype="multipart/form-data">
                                {{ @csrf_field() }}
                                <input type="hidden" name="deal_id" id="deal_id">
                                <div class="form-group">
                                    <label for="example-text">Deal Name</label>
                                    <input type="text"  name="name" id="dealname" class="form-control">
                                </div>
                                
                                <div class="form-group">
                                    <label for="example-text">Discount(%)</label>
                                    <input type="text"  name="discount" id="discount" class="form-control">
                                </div>
                                {{-- <div class="form-group">
                                    <label for="example-text">Quantity(per purchase)</label>
                                    <input type="text"  name="quantity" class="form-control">
                                </div> --}}
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="custom-select col-12 text-capitalize" name="cat" id="inlineFormCustomSelect" >
                                        <option selected>Choose...</option>
                                        @foreach($cat as $ca)
                                        <option  value="{{$ca->id}}">{{$ca->cat}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Location</label>
                                    <input type="text"  name="location" class="form-control" id="location" value="{{$loc->address}}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Validity(Max 30 days)</label>
                                    <input type="number"  name="validity" class="form-control" id="validity" min="1" max="30">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Deal price</label>
                                    <input type="text"  name="price" class="form-control" id="price">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Deal Description</label>
                                    <input type="text"  name="description" class="form-control" id="desc">
                                </div>
                                <div class="form-group">
                                    <label>Deal Image</label>
                                    <input type="file" class="form-control" name="image">
                                </div>
                                @if($feature>$fc)
                                <div class="form-check">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="featured" value="1" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Set as featured deal</label>
                                    </div>
                                </div>
                                @elseif($fc!=0)
                                <div class="form-check">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="featured" value="0" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Set as featured deal</label>
                                    </div>
                                </div>
                                @else

                                @endif
                                <br>
                                <button type="submit" class="btn waves-effect waves-light btn-success">Save</button>
                            </form>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add Modal -->
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Enter Deal Info</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form class="form-horizontal m-t-30" method="POST" action="{{ url('merchant/dealsave') }}" enctype="multipart/form-data">
                                {{ @csrf_field() }}
                                <input type="hidden" name="merchant_id" value="{{ Auth::guard('vendor')->user()->merchant_id }}">
                                <div class="form-group">
                                    <label for="example-text">Deal Name</label>
                                    <input type="text"  name="deal" class="form-control">
                                </div>
                                
                                <div class="form-group">
                                    <label for="example-text">Discount(%)</label>
                                    <input type="text"  name="discount" class="form-control">
                                </div>
                                {{-- <div class="form-group">
                                    <label for="example-text">Quantity(per purchase)</label>
                                    <input type="text"  name="quantity" class="form-control">
                                </div> --}}
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="custom-select col-12 text-capitalize" name="category" id="inlineFormCustomSelect" >
                                        <option selected>Choose...</option>
                                        @foreach($cat as $ca)
                                        <option @if($loc->category==$ca->id) selected @endif  value="{{$ca->id}}">{{$ca->cat}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Location</label>
                                    <input type="text"  name="location" class="form-control" value="{{$loc->address}}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Validity(Max 30 days)</label>
                                    <input type="number"  name="validity" class="form-control" min="1" max="30">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Deal price</label>
                                    <input type="text"  name="price" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Deal Description</label>
                                    <input type="text"  name="description" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Deal Image</label>
                                    <input type="file" class="form-control" name="image">
                                </div>
                                @if($feature>$fc)
                                <div class="form-check">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="featured" value="1" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2">Set as featured deal</label>
                                    </div>
                                </div>
                                @else
                                @endif
                                <br>
                                <button type="submit" class="btn waves-effect waves-light btn-success">Save</button>
                            </form>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="deletemodal" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">DELETE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </div>
                        <div class="modal-body"> Do you want to delete this? </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                            <a href=""  id="deletes" class="btn btn-primary yes">YES</a> </div>
                        </div>
                    </div>
                </div>
                <div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <i class="fa fa-smile-o font-20 text-muted"></i>
                                            <p class="font-16 m-b-5">Deals Used</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h1 class="font-light text-right">{{$count}}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <i class="fa fa-picture-o font-20  text-muted"></i>
                                            <p class="font-16 m-b-5">Deals Left</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h1 class="font-light text-right">{{ $limit-$count }}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex no-block align-items-center">
                                        <div>
                                            <i class="fa fa-thumbs-o-up font-20 text-muted"></i>
                                            <p class="font-16 m-b-5">Featured Deal Left</p>
                                        </div>
                                        <div class="ml-auto">
                                            <h1 class="font-light text-right">{{$feature-$fc}}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- deal table --}}
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <h4 class="card-title pull-left">All Deals</h4>
                                    <span class="pull-right">
                                        @if($limit-$count>0)
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">ADD</button>
                                        @else
                                        <a href="{{ url('merchant/membership') }}">
                                            <button type="button" class="btn btn-primary">UPGRADE PLAN</button>
                                        </a>
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">DEAL IMAGE</th>
                                        <th class="border-top-0">NAME</th>
                                        <th class="border-top-0">DISCOUNT</th>
                                        {{-- <th class="border-top-0">QUANTITY</th> --}}
                                        <th class="border-top-0">VALIDITY LEFT</th>
                                        <th class="border-top-0">PRICE</th>
                                        <th class="border-top-0">DESCRIPTION</th>
                                        <th class="border-top-0">RATINGS</th>
                                        <th class="border-top-0">STATUS</th>
                                        <th class="border-top-0">FEATURED</th>
                                        <th class="border-top-0">ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($deals as $del)
                                    <tr id="dealsdata">
                                        <td>
                                            <div class="avatar">
                                                <img alt="Image placeholder" style="height: 35px;width: 55px;" src="{{ url(asset($del->image)) }}" class="thumbnail">
                                            </div>
                                        </td>
                                        <td class="txt-oflo">@if (strlen($del->name) <=20){{$del->name}}
                                            @else{{substr($del->name, 0, 20) . '...'}}
                                        @endif</td>
                                        <td class="txt-oflo">{{ $del->discount }} %</td>
                                        {{-- <td class="txt-oflo">{{ $del->quantity }}</td> --}}
                                        <td class="txt-oflo">{{ now()->diffInDays($del->validity) }} Days</td>
                                        <td><span class="font-medium">₹{{ $del->price }}</span></td>
                                        <td class="txt-oflo">
                                            @if (strlen($del->description) <=20){{$del->description}}
                                            @else{{substr($del->description, 0, 20) . '...'}}
                                            @endif
                                        </td>
                                        <td>
                                            @if(1 || 2 || 3 || 4 || 5 == $del->star)
                                            @for($i=1;$i<=$del->star;$i++)
                                            <i class="fa fa-star" style="color: #FF912C;" backup-class="fa fa-star"></i>
                                            @endfor
                                            @for($i=$del->star;$i<5;$i++)
                                            <i class="fa fa-star-o" aria-hidden="true"></i>
                                            @endfor
                                            @endif
                                        </td>
                                        @if($del->validity>now())
                                        <td><span class="label label-success label-rounded">Active</span> </td>
                                        @else
                                        <td><span class="label label-danger label-rounded">Expired</span> </td>
                                        @endif
                                        @if($del->featured==1)
                                        <td><span class="label label-warning label-rounded">Featured Deal</span> </td>
                                        @else
                                        <td><span class="label label-info label-rounded">Not Featured</span> </td>
                                        @endif
                                        <td>
                                            <a class="editdl" data-id='{{$del->deal_id}}' data-name='{{$del->name}}' data-discount='{{$del->discount}}'data-desc='{{$del->description}}' data-price='{{$del->price}}' data-validity='{{ now()->diffInDays($del->validity) }}' data-cat='{{$del->cat}}' data-featured='@if($del->featured==0) 0 @else 1 @endif' data-checked='@if($del->featured==0) false @else true @endif'>
                                                <i class="fa fa-pencil" style='cursor: pointer;' aria-hidden="true"></i>
                                            </a>
                                            &nbsp;
                                            <a class="deletedl"style='cursor: pointer;'  data-id="{{ $del->id }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $deals->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.merchant.footer');
    </div>
    @endsection