@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
    @include('layouts.merchant.navbar')
    <div class="page-wrapper">
        @include('layouts.merchant.breadcrumb')
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-7">
                    @if (count($errors->res) > 0)
                    @foreach ($errors->res->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                    @endforeach
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-7">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Business Details</h4>
                            <form class="form-horizontal m-t-30" method="POST" action="{{ url('merchant/resave') }}" enctype="multipart/form-data">
                                {{ @csrf_field() }}
                                <input type="hidden" name="merchant_id" value="{{ Auth::guard('vendor')->user()->merchant_id }}">
                                <div class="form-group">
                                    <label for="example-text">Name</label>
                                    <input type="text"  name="name" class="form-control" value="{{ $mr->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Store Image</label>
                                    <input type="file"  name="store[]" multiple class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Website Name(optional)</label>
                                    <input type="text"  name="site" class="form-control" value="{{ $mr->site }}">
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="custom-select col-12 text-capitalize" name="category" id="inlineFormCustomSelect" >
                                        <option selected>Choose...</option>
                                        @foreach($cat as $ca)
                                        <option  @if($mr->cat==$ca->cat) selected @endif  value="{{$ca->id}}">{{$ca->cat}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Store Description</label>
                                    <textarea type="text" rows="5"  name="descr" class="form-control">{{ $mr->descr }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Facebook Page Link (optional)</label>
                                    <input type="text"   name="fb" class="form-control" value="{{ $mr->fb }}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Twitter Page Link (optional)</label>
                                    <input type="text"  name="tw" class="form-control" value="{{ $mr->tw }}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Google+ Page Link (optional)</label>
                                    <input type="text"  name="gp" class="form-control" value="{{ $mr->gp }}">
                                </div>
                                <button type="submit" class="btn waves-effect waves-light btn-success">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.merchant.footer');
    </div>
</div>
@endsection