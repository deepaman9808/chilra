@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
    @include('layouts.merchant.navbar')
    <div class="page-wrapper">
        @include('layouts.merchant.breadcrumb')
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-7">
                    @if (count($errors->coupon) > 0)
                    @foreach ($errors->coupon->all() as $error)
                    <div class="alert alert-warning">
                        {{ $error }}
                    </div>
                    @endforeach
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                </div>
            </div>
            {{-- edit modal --}}
            <div class="modal" id="editmodalcp">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Enter Coupon Info</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form class="form-horizontal m-t-30" method="POST" action="{{ url('merchant/couponupdate') }}" enctype="multipart/form-data">
                                {{ @csrf_field() }}
                                <input type="hidden" name="coupon_id" id="coupon_id">
                                <div class="form-group">
                                    <label for="example-text">Coupon Name</label>
                                    <input type="text"  name="name" id="name" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Coupon Code (A-Z || 0-9)</label>
                                    <input type="text"  name="code" id="code" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Discount(%)</label>
                                    <input type="text"  name="discount" id="discount" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Validity(Max 30 days)</label>
                                    <input type="text"  name="validity" id="validity" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="custom-select col-12 text-capitalize" name="cat" id="inlineFormCustomSelect" >
                                        <option selected>Choose...</option>
                                        @foreach($cat as $ca)
                                        <option value="{{$ca->id}}">{{$ca->cat}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Location</label>
                                    <input type="text"  name="location" value="{{$loc->address}}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Coupon Description</label>
                                    <input type="text"  name="description" id="desc" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Coupon Image</label>
                                    <input type="file" class="form-control" name="image">
                                </div>
                                <button type="submit" class="btn waves-effect waves-light btn-success">Save</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add Modal -->
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Enter Coupon Info</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form class="form-horizontal m-t-30" method="POST" action="{{ url('merchant/couponsave') }}" enctype="multipart/form-data">
                                {{ @csrf_field() }}
                                <input type="hidden" name="merchant_id" value="{{ Auth::guard('vendor')->user()->merchant_id }}">
                                <div class="form-group">
                                    <label for="example-text">Coupon Name</label>
                                    <input type="text"  name="coupon" class="form-control" placeholder="Eg. foodcoupon" value="{{ old('coupon')}}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Coupon Code (A-Z || 0-9)</label>
                                    <input type="text"  name="code" class="form-control" placeholder="Eg. GET50" value="{{ old('code')}}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Discount(%)</label>
                                    <input type="text"  name="discount" placeholder="Eg. 10-90" class="form-control" value="{{ old('discount')}}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Validity(Max 30 days)</label>
                                    <input type="text"  name="validity" placeholder="Eg. 10" class="form-control" value="{{ old('validity')}}">
                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="custom-select col-12 text-capitalize" name="category" id="inlineFormCustomSelect" >
                                        <option selected>Choose...</option>
                                        @foreach($cat as $ca)
                                        <option @if($loc->category==$ca->id) selected @endif  value="{{$ca->id}}">{{$ca->cat}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Location</label>
                                    <input type="text"  name="location" class="form-control" value="{{$loc->address}}">
                                </div>
                                <div class="form-group">
                                    <label for="example-text">Coupon Description</label>
                                    <input type="text"  name="description" placeholder="You can use this coupen at checkout" class="form-control" value="{{ old('description')}}">
                                </div>
                                <div class="form-group">
                                    <label>Coupon Image</label>
                                    <input type="file" class="form-control" name="image">
                                </div>
                                <button type="submit" class="btn waves-effect waves-light btn-success">Save</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="deletemodal" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">DELETE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </div>
                        <div class="modal-body"> Do you want to delete this? </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                            <a href=""  id="deletes" class="btn btn-primary dele">YES</a> </div>
                        </div>
                    </div>
                </div>
                {{-- Coupons table --}}
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-11">
                                    <h4 class="card-title">All Coupons</h4>
                                </div>
                                <div class="col-1">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">ADD</button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th class="border-top-0">COUPON IMAGE</th>
                                        <th class="border-top-0">NAME</th>
                                        <th class="border-top-0">CODE</th>
                                        <th class="border-top-0">DISCOUNT</th>
                                        <th class="border-top-0">VALIDITY LEFT</th>
                                        <th class="border-top-0">DESCRIPTION</th>
                                        <th class="border-top-0">STATUS</th>
                                        <th class="border-top-0">ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($coup as $cp)
                                    <tr id="coupondata">
                                        <td>
                                            <div class="avatar">
                                                <img alt="Image placeholder" style="height: 35px;width: 55px;" src="{{ url(asset($cp->image)) }}" class="thumbnail">
                                            </div>
                                        </td>
                                        <td class="txt-oflo">@if (strlen($cp->name) <=20){{$cp->name}}
                                            @else{{substr($cp->name, 0, 20) . '...'}}
                                        @endif</td>
                                        <td class="txt-oflo">{{ $cp->code }}</td>
                                        <td class="txt-oflo">{{ $cp->discount }} %</td>
                                        <td class="txt-oflo">{{ now()->diffInDays($cp->validity)}} Days</td>
                                        <td class="txt-oflo">@if (strlen($cp->description) <=20){{$cp->description}}
                                            @else{{substr($cp->description, 0, 20) . '...'}}
                                            @endif
                                        </td>
                                        @if($cp->validity>now())
                                        <td><span class="label label-success label-rounded">Active</span> </td>
                                        @else
                                        <td><span class="label label-danger label-rounded">Expired</span> </td>
                                        @endif
                                        <td>
                                            <a class="editcp" data-id='{{$cp->coupon_id}}' data-name='{{$cp->name}}' data-discount='{{$cp->discount}}'data-desc='{{$cp->description}}' data-cat='{{$cp->cat}}' data-code='{{$cp->code}}' data-validity='{{ now()->diffInDays($cp->validity) }}'>
                                                <i class="fa fa-pencil" style='cursor: pointer;' aria-hidden="true"></i>
                                            </a>
                                            &nbsp;
                                            <a class="deletecp" style='cursor: pointer;'data-id="{{ $cp->id }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $coup->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.merchant.footer');
        </div>
        @endsection