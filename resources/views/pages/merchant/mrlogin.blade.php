@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="image-container set-full-height" style="background-image: url(https://coloredbrain.com/wp-content/uploads/2016/07/login-background.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-5">
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="green" id="wizardProfile">
                        <form   action="{{ url('vendorlog') }}" method="POST" >
                            {{ csrf_field() }}
                            <div class="wizard-header">
                                <h3 class="wizard-title">Login Here</h3>
                                <hr>
                                <h5>Enter The Login Details Here</h5>
                            </div>
                            <div class="wizard-body" style="padding:20px;">
                                @if (count($errors->danger) > 0)
                                @foreach ($errors->danger->all() as $error)
                                <div class="alert alert-warning">
                                    {{ $error }}
                                </div>
                                @endforeach
                                @endif
                                @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="input-group">
                                        <span class="input-group-addon mt-4">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email <small>(required)</small></label>
                                                <input name="email" type="text" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon mt-4">
                                            <i class="material-icons">fingerprint</i>
                                        </span>
                                        <div class="col-sm-10">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Password <small>(required)</small></label>
                                                <input name="password" type="password" class="form-control" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="text-center">
                                    <button type='submit' class='btn btn-success' name='login'>Login</button>
                                    <a href="{{url('merchant/register')}}"><button type='button' class='btn btn-info' name='register'>Register</button> </a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="text-right">
                        <a href="{{ ('forget') }}" style="color: #ff9800;" class="my-4">Forgot Password</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection