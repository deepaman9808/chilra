@extends('layouts.merchant.master')<!-- main layout file -->
@section('content') 
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div class="modal fade" id="deletemodal" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">DELETE</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        </div>
                        <div class="modal-body"> Do you want to delete this? </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                            <a href=""  id="deletes" class="btn btn-primary dele">YES</a> </div>
                        </div>
                    </div>
                </div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
      @include('layouts.merchant.navbar')
        <div class="page-wrapper">
          @include('layouts.merchant.breadcrumb')
            <div class="container-fluid">
                 <div class="row">
                <div class="col-xl-7">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                </div>
            </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                             <div class="card-body">
                                <h4 class="card-title">All INVOICES</h4>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                      <thead>
                                        <tr>
                                            <th class="border-top-0">MERCHANT ID</th>
                                            <th class="border-top-0">NAME</th>
                                            <th class="border-top-0">EMAIL</th>
                                            <th class="border-top-0">PHONE</th>
                                            <th class="border-top-0">AMOUNT</th>
                                            <th class="border-top-0">STATUS</th>
                                            <th class="border-top-0">RECEIVED DATE</th>
                                            <th class="border-top-0">ACTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($invoice as $data)
                                        <tr>
                                            <td>{{$data->merchant_id}}</td>
                                            <td>{{auth::guard('vendor')->user()->vendor_name}}</td>
                                            <td>{{auth::guard('vendor')->user()->email}}</td>
                                            <td>{{auth::guard('vendor')->user()->phone}}</td>
                                            <td><span class="font-medium">₹{{$data->invoice}}</span></td>
                                            <td>
                                                @if($data->inv_status==1)
                                                  <span class="label label-success label-rounded">Received</span></td>
                                                @else
                                                  <span class="label label-danger label-rounded">Not Received</span></td>
                                                @endif
                                            <td>{{$data->updated_at->format('d-M-Y g:ia')}}</td>
                                            <td>
                                            <a class="delete" style='cursor: pointer;'>
                                                <i class="fa fa-trash deletecv" data-url="{{url('merchant/invoice?id=')}}{{$data->merchant_id}}" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                   </table>
                                 {{ $invoice->links() }} 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
           @include('layouts.merchant.footer');
        </div>
    </div>
@endsection