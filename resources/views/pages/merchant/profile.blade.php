@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<div class="preloader">
    <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
    </div>
</div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
    @include('layouts.merchant.navbar')
    <div class="page-wrapper">
        @include('layouts.merchant.breadcrumb')
        <div class="container-fluid">
            <div class="row">
                <!-- Column -->
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <center class="m-t-30"> <img src="{{ url(asset(Auth::guard('vendor')->user()->image)) }}" class="rounded-circle"height="180" width="180" />
                            <h4 class="card-title m-t-10">{{ Auth::guard('vendor')->user()->vendor_name }}</h4>
                            <h6 class="card-subtitle">Chilra Vendor</h6>
                            </center>
                        </div>
                        <div>
                        <hr> </div>
                        <div class="card-body">
                            <small class="text-muted">Email address </small>
                            <h6>{{ Auth::guard('vendor')->user()->email }}</h6>
                            <small class="text-muted p-t-30 db">Phone</small>
                            <h6>{{ Auth::guard('vendor')->user()->phone }}</h6>
                            <small class="text-muted p-t-30 db">Address</small>
                            <h6>{{ Auth::guard('vendor')->user()->address }}</h6>
                            <small class="text-muted p-t-30 db">Social Profile</small>
                            <br/>
                           <a href="//{{$social->fb}}"> <button class="btn btn-circle btn-secondary"><i class="fa fa-facebook"></i></button></a>
                           <a href="//{{$social->tw}}"> <button class="btn btn-circle btn-secondary"><i class="fa fa-twitter"></i></button></a>
                           <a href="//{{$social->gp}}"> <button class="btn btn-circle btn-secondary"><i class="fa fa-google-plus"></i></button></a>
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card" style="margin-bottom: 16px;">
                        <div class="card-body">
                            @if (count($errors->prof) > 0)
                            @foreach ($errors->prof->all() as $error)
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $error }}</strong>
                            </div>
                            @endforeach
                            @endif
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                        </div>
                        <form class="form-horizontal form-material" method="post" action="{{url('merchant/profile/update')}}" enctype="multipart/form-data">
                            {{ @csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-12">Name</label>
                                <div class="col-md-12">
                                    <input type="text" value="{{ Auth::guard('vendor')->user()->vendor_name }}" class="form-control form-control-line" name="vendor_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <input type="file" value="{{ Auth::guard('vendor')->user()->email }}" class="form-control form-control-line" name="image" id="example-email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Address</label>
                                <div class="col-md-12">
                                    <input type="text" value="{{ Auth::guard('vendor')->user()->address }}" class="form-control form-control-line" name="address">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            @if (count($errors->pswd) > 0)
                            @foreach ($errors->pswd->all() as $error)
                            <div class="alert alert-warning">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $error }}</strong>
                            </div>
                            @endforeach
                            @endif
                            @if ($message = Session::get('successs'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            @endif
                            <form class="form-horizontal form-material" method="post" action="{{url('merchant/profile/password')}}">
                                {{ @csrf_field() }}
                                <div class="form-group">
                                    <label class="col-md-12">Old Password</label>
                                    <div class="col-md-12">
                                        <input type="password" name="password" class="form-control form-control-line" id="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input type="password" name="password_confirmation" class="form-control form-control-line" id="cpassword">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-muted text-italic"><small>Password Strength: <span id="password-strength-status" class="text-danger font-weight-700">weak</span></small>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.merchant.footer');
</div>
</div>
@endsection