@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<style type="text/css">.image-container {
    min-height: 100vh;
    background-position: center center;
    background-size: cover;
    position: relative;
}</style>
<div class="image-container set-full-height" style="background-image: url(https://coloredbrain.com/wp-content/uploads/2016/07/login-background.jpg)">

  <div class="main-content">
    <!-- Header -->
    <div class="header  py-7 py-lg-8"></div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7" style="margin-top:15%;">
          <div class="card bg-transparent border-0">
            <div class="card-header bg-transparent pb-3">
              <div class="text-muted text-center mt-2 mb-3"><h1 style="color: white">New Password</h1></div>
               @if (count($errors->danger) > 0)                       
                    @foreach ($errors->danger->all() as $error)
                          <div class="alert alert-warning alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $error }}</strong>
                         </div>
                    @endforeach                            
                @endif 
            </div>
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-white mb-4">
                <h6>Welcome vendor</h6>
              </div>
              <form role="form" method="POST" action="{{ url('merchant/setpassword') }}">
               {{ @csrf_field() }}
                <input type="hidden" name="code" value="{{ $code }}">
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    </div>
                    <input class="form-control" name="password" id="password" placeholder="Password" type="password"> 
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-lock"></i></span>
                    </div>
                    <input class="form-control" name="password_confirmation" id="cpassword" placeholder="Confirm Password" type="password" > 
                  </div>
                </div>
                <div class="text-muted text-italic"><small>Password Strength: <span id="password-strength-status" class="text-success font-weight-700">strong</span></small></div>
                <div class="text-center">
                  <button type="submit" class="btn btn-success my-4">Save</button>
                   <a href="{{ url('merchant/login') }}" class="btn btn-info" role="button">Back</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection