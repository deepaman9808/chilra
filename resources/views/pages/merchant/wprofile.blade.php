@extends('layouts.merchant.master')<!-- main layout file -->
@section('content')
<style type="text/css">
.field-icon
{
float: right;
margin-top: -25px;
position: relative;
z-index: 2;
}
.none
{
	display: none;
}
.txt
{
color: green;
font-weight: 500;
}
</style>
<div class="image-container set-full-height" style="background-image: url({{ url(asset('img/wizard-profile.jpg')) }}">
	<!--   Creative Tim Branding   -->
	<a href="#">
		<div class="logo-container">
			{{-- <div class="logo">
				<img src="img/brand/1584020785.png">
			</div> --}}
			<div class="brand">
				Chilra
			</div>
		</div>
	</a>
	<!--   Big container   -->
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<!--      Wizard container        -->
				<div class="wizard-container">
					<div class="card wizard-card" data-color="orange" id="wizardProfile">
						<form action="{{ url('merchant_reg') }}" id="getmer" method="POST"  enctype="multipart/form-data">
							{{ csrf_field() }}
							
							<div class="wizard-header">
								<h3 class="wizard-title">
								You Are 30 Seconds Away From Become A Merchant...
								</h3>
								<h5>This information will let us know more about you.</h5>
							</div>
							<div class="wizard-navigation">
								<ul>
									<li><a href="#about" data-toggle="tab">About</a></li>
									<li><a href="#account" data-toggle="tab">Category</a></li>
									<li><a href="#address" data-toggle="tab">Address</a></li>
								</ul>
							</div>
							<div class="tab-content">
								<div class="tab-pane" id="about">
									<div class="row">
										<h4 class="info-text"> Let's start with the basic information (with validation)</h4>
										<div class="col-sm-4 col-sm-offset-1 ">
											<div class="picture-container">
												<div class="picture">
													<img src="{{ url(asset('img/default-avatar.png')) }}" class="picture-src" id="wizardPicturePreview" title="You can upload lator"/>
													<input type="file" required="" name="image" id="image">
												</div>
												<h6>Choose Picture</h6>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">face</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Full Name <small>(required)</small></label>
													<input name="vendor_name" type="text" class="form-control" required="">
												</div>
											</div>
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">record_voice_over</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Business Name <small>(required)</small></label>
													<input name="name" type="text" class="form-control" required="">
												</div>
											</div>
										</div>
										<div class="col-sm-10 col-sm-offset-1">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">email</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Email <small>(required)</small></label>
													<input name="email" type="email" class="form-control">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="account">
									<h4 class="info-text"> Business Category? </h4> <h6 style="color:red;">*Required</h6>
									<div class="row">
										<div class="col-sm-10 col-sm-offset-1 categoryjs">
											@foreach($cat as $ct)
											<div class="col-sm-4">
												<div class="choice" data-toggle="wizard-radio">
													<input type="radio" name="category" required="" value="{{ $ct->id }}">
													<div class="icon">
														<i class="{{ $ct->icon }}"></i>
													</div>
													<h6>{{ $ct->cat }}</h6>
												</div>
											</div>
											@endforeach
										</div>
									</div>
								</div>
								<div class="tab-pane" id="address">
									<div class="row">
										<div class="col-sm-10 col-sm-offset-1">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">note</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Address <small>(required)</small></label>
													<input name="address" type="text" required="" class="form-control">
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-5 col-sm-offset-1">
											<div class="input-group">
												<span class="input-group-addon">
													<i style="margin-top: -11px !important;" class="material-icons">phone</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">phone <small>(required)</small></label>
													<input name="phone" type="tel" id="mrphone" class="form-control" required>
													<button type="button"  id="verifyphone" disabled="true" class="btn btn-sm btn-primary">Verify</button>
													<div class="none txt pull-right" style="padding: 12px 6px;" id="sendotpsuccess">Otp Sent Successfully</div>
												</div>
											</div>
										</div>
										<div class="col-sm-5">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">redeem</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Otp <small>(required)</small></label>
													<input name="otp" disabled="true" required="" id="otp" type="text" class="form-control" required>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-5 col-sm-offset-1">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">fingerprint</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Password <small>(required)</small></label>
													<input name="password" id="password" type="password" class="form-control" required>
													
												</div>
											</div>
										</div>
										<div class="col-sm-5">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="material-icons">fingerprint</i>
												</span>
												<div class="form-group label-floating">
													<label class="control-label">Confirm Password <small>(required)</small></label>
													<input name="password" id="cpassword" type="password" class="form-control" required>
													<span toggle="#cpassword" class="fa fa-fw fa-eye field-icon toggle-password" ></span>
													<div class="text-muted text-italic"><small>Password Strength: <span id="password-strength-status" class="text-success font-weight-700">strong</span></small></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<div class="wizard-footer">
							<div class="pull-right">
								<input type='button' class='btn btn-next btn-fill btn-warning btn-wd' name='next' value='Next' />
								<input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd' name='finish' value='Finish' />
							</div>
							<div class="pull-left">
								<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
				</div> <!-- wizard container -->
			</div>
			</div><!-- end row -->
			</div> <!--  big container -->
			<div class="footer">
				<div class="container text-center">
					Made with <i class="fa fa-heart heart"></i> by <a href="https://chilra.com">Chilra</a>
				</div>
			</div>
		</div>
		@endsection