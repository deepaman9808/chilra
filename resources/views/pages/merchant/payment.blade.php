@extends('layouts.merchant.master')<!-- main layout file -->
@section('content') 
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
<div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
      @include('layouts.merchant.navbar')
        <div class="page-wrapper">
          @include('layouts.merchant.breadcrumb')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                             <div class="card-body">
                                <h4 class="card-title">All Deals</h4>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                      <thead>
                                        <tr>
                                            <th class="border-top-0">NAME</th>
                                            <th class="border-top-0">DEAL ID</th>
                                            <th class="border-top-0">PAYMENT ID</th>
                                            <th class="border-top-0">AMOUNT</th>
                                            <th class="border-top-0">EMAIL</th>
                                            <th class="border-top-0">PHONE</th>
                                            <th class="border-top-0">PAYMENT STATUS</th>
                                            <th class="border-top-0">ORDER STATUS</th>
                                            <th class="border-top-0">DATE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!$order->isEmpty())
                                        @foreach($order as $or)
                                        <tr>
                                            <td>{{$or->name}}</td>
                                            <td>{{$or->deal_id}}</td>
                                            <td>{{$or->payment_id}}</td>
                                            <td><span class="font-medium">₹{{$or->amount}}</span></td>
                                            <td>{{$or->email}}</td>
                                            <td>{{$or->phone}}</td>
                                            <td><span class="label label-success label-rounded">{{$or->status}}</span> </td>
                                            <td>
                                                @if($or->order_status=='0')
                                                <div class="form-group border m-0">
                                                    <select class="custom-select order_status text-capitalize" name="category" id="inlineFormCustomSelect">
                                                       <option  @if($or->order_status=='0') selected @endif >Choose...</option>
                                                       <option  class="order_value" data-value="{{$or->payment_id}}" value="completed">Completed</option>
                                                    </select>
                                                </div>
                                                @else
                                                <span class="label label-info label-rounded">Completed</span> 
                                                @endif
                                            </td>
                                            <td>{{$or->created_at}}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                   </table>
                                 {{ $order->links() }} 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
           @include('layouts.merchant.footer');
        </div>
    </div>
@endsection