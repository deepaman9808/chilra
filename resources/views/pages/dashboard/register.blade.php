@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<style type="text/css">
body
{
   background: url('img/brand/login-background.png');
   background-size: cover;
}
</style>
    <div class="header py-7 py-lg-8"></div>
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header bg-transparent pb-3">
              <div class="text-muted text-center mt-2 mb-2"><h1>Register</h1></div>
               @if (count($errors->danger) > 0)                       
                    @foreach ($errors->danger->all() as $error)
                          <div class="alert alert-warning alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $error }}</strong>
                         </div>
                    @endforeach                            
                @endif 
              <div class="card-body px-lg-5 py-lg-5">
               <div class="text-center text-muted mb-4">
                <small>Or sign up with credentials</small>
              </div> 
               <form role="form" method="POST" action="{{ ('adminregister') }}">
                  {{ @csrf_field() }}
                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" name="name" placeholder="Name" value="{{ old('name')}}" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" name="email" placeholder="Email"  value="{{ old('email')}}" type="email">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" name="password" placeholder="Password"  id="password"  type="password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                    </div>
                    <input class="form-control" id="cpassword" name="password_confirmation"   placeholder="Confirm Password" type="password">
                  </div>
                </div>
                <div class="text-muted text-italic"><small>Password Strength: <span id="password-strength-status" class="text-success font-weight-700">strong</span></small></div>
                <div class="row my-4">
                  <div class="col-12">
                    <div class="custom-control custom-control-alternative custom-checkbox">
                      <input class="custom-control-input" id="customCheckRegister" type="checkbox">
                      <label class="custom-control-label" for="customCheckRegister">
                        <span class="text-muted">I agree with the <a href="#!">Privacy Policy</a></span>
                      </label>
                    </div>
                  </div>
                </div>
                <div class="text-center">
                  <button type="submit"  id="create_account" class="btn btn-success mt-4">Create account</button>
                  <a href="{{ ('adminlogin') }}" class="btn btn-info mt-4" role="button">Back</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

