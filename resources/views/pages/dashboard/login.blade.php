@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<style type="text/css">
body
{
background: url('public/images/brand/login-background.png');
background-size: cover;
}
</style>
<div class="main-content">
  <div class="header  py-7 py-lg-8"></div>
  <!-- Page content -->
  <div class="container mt--8 pb-5">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-7">
        <div class="card bg-secondary shadow border-0">
          <div class="card-header bg-transparent pb-3">
            <div class="text-muted text-center mt-2 mb-3"><h1>Login</h1></div>
            @if (count($errors->login) > 0)
            @foreach ($errors->login->all() as $error)
            <div class="alert alert-warning">
              {{ $error }}
            </div>
            @endforeach
            @endif
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
          </div>
          <div class="card-body px-lg-5 py-lg-5">
            <div class="text-center text-muted mb-4">
              <small>Welcome Admin</small>
            </div>
            <form role="form" method="POST" action="{{ ('adminlog') }}">
              {{ @csrf_field() }}
              <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                  </div>
                  <input class="form-control" name="email" placeholder="Email" type="email" value="{{ old('email')}}">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                  </div>
                  <input class="form-control" name="password" placeholder="Password" type="password" value="{{ old('password')}}">
                </div>
              </div>
              <div class="custom-control custom-control-alternative custom-checkbox">
                <input class="custom-control-input" id=" customCheckLogin" name="remember" type="checkbox">
                <label class="custom-control-label" for=" customCheckLogin">
                  <span class="text-muted">Remember me</span>
                </label>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-success my-4">Sign in</button>
              </div>
            </form>
          </div>
        </div>
        <div class="text-right">
          <a href="{{ ('forgotadmin') }}" class="my-4 text-light">Forgot Password</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection