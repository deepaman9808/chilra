@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<style type="text/css">
.font
{
font-weight: 300;
}
</style>
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<div class="modal fade" id="viewpay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Payment Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <center>
        {{--  <img src="" id="img" name="aboutme" width="140" height="140" border="0" class="rounded-circle"> --}}
        </center>
        <br>
        <h4>Payment Id :  <span class="id font">      </span></h4>
        <h4>Email :       <span class="email font">      </span></h4>
        <h4>Phone :       <span class="phone font">      </span></h4>
        <h4>Plan :        <span class="plan font">       </span></h4>
        <h4>Amount :      <span class="amount font">     </span></h4>
        <h4>Date :        <span class="date font">       </span></h4>
        <hr>
        {{--  <center>
        <p class="text-left "><strong>Description: </strong><br>
        <span class="description"></p>
        <br>
        </center> --}}
      </div>
      <div class="modal-footer">
        <center>
        <button type="button" class="btn btn-default" data-dismiss="modal">Okk</button>
        </center>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE PAYMENT DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a class="payconfirm" href=""><button class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col">
      @if (count($errors->pro) > 0)
      @foreach ($errors->pro->all() as $error)
      <div class="alert alert-danger">
        {{ $error }}
      </div>
      @endforeach
      @endif
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
    </div>
  </div>
  <!-- Table -->
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Payment Details</h3>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Merchant Name</th>
                <th scope="col">Payment ID</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Plan</th>
                <th scope="col">Amount</th>
                <th scope="col">Status</th>
                <th scope="col">Date</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($meme as $data)
              <tr>
                <td>
                  {{ $data->vendor_name }}
                </td>
                <td>
                  {{ $data->payment_id }}
                </td>
                <td>
                  <span class="badge badge-dot mr-4">
                    @if(!$data->email)
                    <i class="bg-gradient-danger"></i> Null
                    @else
                    <i class="bg-gradient-success"></i>    {{ $data->email }}
                    @endif
                  </span>
                </td>
                <td>
                  {{ $data->phone }}
                </td>
                <td>
                  {{ $data->plan }}
                </td>
                <td>
                  ₹ {{ $data->amount }}
                </td>
                <td>
                  <span class="text-success">{{ $data->status }}</span>
                </td>
                <td>
                  {{ $data->created_at->format('d-M-Y g:ia') }}
                </td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"style="line-height: 2.5;"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow adminpay">
                      {{--  <a class="dropdown-item" id="viewpay"  data-id='{{ $data->payment_id }}'  data-email='{{ $data->email }}' data-phone='{{ $data->phone }}' data-plan='{{ $data->plan }}' data-amount='{{ $data->amount }}' data-date='{{ $data->created_at }}' data-status='{{ $data->status }}'>View</a> --}}
                      {{--  <a class="dropdown-item" href="#">Suspend</a> --}}
                      <a class="dropdown-item" data-toggle="modal" data-target="#confirm-delete" onclick="$('.payconfirm').attr('href','{{ ('deletepayment/')}}{{ $data->payment_id }}')" style="cursor: pointer;">Delete</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination vendor_bus justify-content-end text-light mb-0">
              {{ $meme->links() }}
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
</div>
<!--   Core   -->
@endsection