@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')

<style type="text/css">
body
{
   background: url('public/images/brand/login-background.png');
   background-size: cover;
}
</style>

  <div class="main-content">
    <!-- Header -->
    <div class="header  py-7 py-lg-8">
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header bg-transparent pb-3">
                <div class="text-muted text-center mt-2 mb-3"><h1>Forgot Password</h1></div>
                @if ($message = Session::get('success'))
                 <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                         <strong>{{ $message }}</strong>
                 </div>
                 @elseif($message = Session::get('danger'))
                 <div class="alert alert-warning alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                         <strong>{{ $message }}</strong>
                 </div>
                 @else
                 @endif
            </div>
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <small>Enter your email and we shall send you a link to reset your password</small>
              </div>
              <form role="form" method="post" action="{{ ('sendemail') }}">
               {{ @csrf_field() }}
                <div class="form-group mb-3">
                  <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" name="email" placeholder="Email" type="email" value="{{ old('email')}}">
                  </div>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-success my-4">Reset Password</button>
                  <a href="{{ ('adminlogin') }}" class="btn btn-info" role="button">Back</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection