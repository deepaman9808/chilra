@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE ORDER DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a id="deletes" class="orconfirm" ><button class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="container-fluid mt--7">
  <div class="responce d-none">
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Updated Successfully</strong>
  </div>
</div>
<div class="row">
    <div class="col">
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
    </div>
  </div>
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Invoice Details</h3>
        </div>
        
        <div class="table-responsive" style="overflow-x:visible;">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Merchant Id</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Amount</th>
                <th scope="col">Invoice Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($invoice as $data)
              <tr>
                <td>
                  {{ $data->merchant_id }}
                </td>
                <td>
                  {{ $data->vendor_name }}
                </td>
                <td>
                  {{ $data->email }}
                </td>
                <td>
                  {{ $data->phone }}
                </td>
                <td>
                  ₹ {{ $data->invoice }}
                </td>
                @if($data->inv_status==1)
                <td class="text-danger">Paid</td>
                @else
                <td>
                  <div class="form-group">
                    <label class="form-control-label" for="exampleFormControlSelect2"></label>
                    <select class="form-control inv_status" id="exampleFormControlSelect2" name="inv_status">
                      <option selected value="0">Choose One</option>
                      <option class="Merchant" data-id="{{ $data->merchant_id }}" value="1">Paid</option>
                    </select>
                  </div>
                </td>
                @endif
                <td class="text-center">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"style="line-height: 2.5;"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow deleteorder">
                      <a class="dropdown-item" id="deletecp" data-url="{{url('invoices?id=')}}{{$data->merchant_id}}">Delete</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination vendor_bus justify-content-end text-light mb-0">
              {{ $invoice->links() }}
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
</div>
<!--   Core   -->
@endsection