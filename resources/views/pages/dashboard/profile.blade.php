@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Header-end -->
<style type="text/css">
.cuts{line-height: 2.5; }
.profile-img .file input {top:6px;}
</style>
<!-- Page content -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-xl-8 order-xl-1">
      <div class="card  shadow">
        <div class="card-header bg-white border-0">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">My account</h3>
            </div>
            <div class="col-4 text-right">
              <button class="btn btn-sm btn-primary" id="adminpro">Edit Your Profile</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ ('adminpro') }}">
            {{ @csrf_field() }}
            <input type="hidden" name="user_id" value="{{ Auth::guard('admin')->user()->user_id}}">
            <h6 class="heading-small text-muted mb-4">User information</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Username</label>
                    <input type="text"  class="form-control form-control-alternative adminprofl" disabled="true"  name="name" placeholder="Username" value="{{ Auth::guard('admin')->user()->name}}">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Email address</label>
                    <input type="email"  class="form-control form-control-alternative adminprofl"disabled="true"  name="email"
                    value="{{ Auth::guard('admin')->user()->email}}" placeholder="Email">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">First name</label>
                    <input type="text" class="form-control form-control-alternative adminprofl" disabled="true" name="firstname" placeholder="First name" value="{{ Auth::guard('admin')->user()->firstname}}">
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="form-control-label">Last name</label>
                    <input type="text"  class="form-control form-control-alternative adminprofl" disabled="true" name="lastname" placeholder="Last name" value="{{ Auth::guard('admin')->user()->lastname}}">
                  </div>
                </div>
              </div>
            </div>
            <hr class="my-4" />
            <!-- Address -->
            <h6 class="heading-small text-muted mb-4">Contact information</h6>
            <div class="pl-lg-4">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-control-label">Address</label>
                    <input  class="form-control form-control-alternative adminprofl" placeholder="Home Address"disabled="" name="address" value="{{ Auth::guard('admin')->user()->address}}" type="text">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">City</label>
                    <input type="text"  class="form-control form-control-alternative adminprofl" name="city" disabled="true" placeholder="City" value="{{ Auth::guard('admin')->user()->city}}">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Country</label>
                    <input type="text"  class="form-control form-control-alternative adminprofl" name="country" disabled="true" placeholder="Country" value="{{ Auth::guard('admin')->user()->country}}">
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="form-group">
                    <label class="form-control-label">Postal code</label>
                    <input type="number"  class="form-control form-control-alternative adminprofl" name="postal" disabled="true" value="{{ Auth::guard('admin')->user()->postal}}" placeholder="Postal code">
                  </div>
                </div>
              </div>
            </div>
            <hr class="my-4" />
            <!-- Description -->
            <h6 class="heading-small text-muted mb-4">About me</h6>
            <div class="pl-lg-4">
              <div class="form-group">
                <label class="form-control-label">About Me</label>
                <textarea rows="4" class="form-control form-control-alternative adminprofl" name="about" disabled="true" placeholder="A few words about you ...">{{ Auth::guard('admin')->user()->about}}</textarea>
              </div>
              <button type="submit" disabled="true" name="save" class="btn text-white bg-gradient-primary adminprofl">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-xl-4 order-xl-2  mb-xl-0">
      <div class="card">
        <!-- Card body -->
        <div class="card-body">
          @if (count($errors->pro) > 0)
          @foreach ($errors->pro->all() as $error)
          <div class="alert alert-danger">
            {{ $error }}
          </div>
          @endforeach
          @endif
          <form method="POST" action="{{ ('adminproimage') }}" enctype="multipart/form-data">
            {{ @csrf_field() }}
            <input type="hidden" name="user_id" value="{{ auth::guard('admin')->user()->user_id }}">
            <div class="profile-img">
              @if(Auth::guard('admin')->user()->image)
              <img src="{{ url(asset(Auth::guard('admin')->user()->image))}}" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 140px;height: 140px;">
              @else
              <img src="{{ asset('/img/theme/team-1-800x800.jpg')}}" class="rounded-circle img-center img-fluid shadow shadow-lg--hover" style="width: 140px;height: 140px;">
              @endif
              <div class="file btn  btn-primary">Change Photo
                <input type="file" onchange="this.form.submit()" name="profile-img" >
              </div>
            </div>
          </form>
          <div class="pt-4 text-center">
            <h5 class="h3 title">
            <span class="d-block mb-1">{{ Auth::guard('admin')->user()->name}}</span>
            <small class="h4 font-weight-light text-muted">Chilra Admin</small>
            </h5>
          </div>
        </div>
      </div>
      <div class="card mt-3">
        <div class="card-header">
          <h5 class="h3 mb-0">Change Password</h5>
        </div>
        <div class="card-body">
          @if (count($errors->danger) > 0)
          
          @foreach ($errors->danger->all() as $error)
          <div class="alert alert-warning">
            {{ $error }}
          </div>
          @endforeach
          @endif
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif
          <form method="POST" action="{{ ('adminpassword') }}">
            {{ @csrf_field() }}
            <input type="hidden" name="user_id" value="{{Auth::guard('admin')->user()->user_id}}">
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label class="form-control-label">Current Password</label>
                  <input type="Password" class="form-control form-control-alternative" name="currentpassword" placeholder="Enter our current password">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">
                  <label class="form-control-label">New Password</label>
                  <input type="Password"  class="form-control form-control-alternative"  name="password" placeholder="Enter new password">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">
                  <label class="form-control-label">Confirm New Password</label>
                  <input type="Password"  class="form-control form-control-alternative"  name="password_confirmation" placeholder="Confirm password">
                </div>
              </div>
            </div>
            <button type="submit" name="save" class="btn text-white bg-gradient-primary adminprofl">Save</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
@endsection