@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
  <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
      <div class="container-fluid">
        <div class="header-body">
         @include('layouts.dashboard.breadcrumb')
      </div>
  </div>
</div>
<!-- Header-end -->
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col-xl-8 col-md-8">
          <div class="card  shadow">
            <div class="card-header bg-white">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Site Settings</h3>
                </div>
              </div>
            </div>
            <div class="card-body">
                @if(count($errors->danger) > 0)                                             
                    @foreach ($errors->danger->all() as $error)
                          <div class="alert alert-warning">   
                                {{ $error }}
                          </div>
                    @endforeach                            
                @endif 
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                    </div>
                @endif 
                @if ($message = Session::get('warning'))
                  <div class="alert alert-warning alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>{{ $message }}</strong>
                  </div>
                @endif 
              <form method="POST" action="{{ ('banner') }}" enctype="multipart/form-data">
                 {{ @csrf_field() }}
                <h6 class="heading-small text-muted mb-4">Site information</h6>
                <div class="p-lg-4">
                  <div class="row">
                      <div class="col-md-12">
                    <div class="profile-img">
                      <img src="{{url('/public')}}/img/va-marketing-tile-background.jpg" width="100%" class="shadow">
                      </div>
                    </div>
                    <div class="col-lg-8 mt-4">
                      <div class="form-group">
                        <label class="form-control-label">Homepage Banner</label>
                         <input type="file" class="form-control form-control-alternative" name="banner">
                      </div>
                    </div>
                    <div class="col-lg-8 mt-4">
                      <div class="form-group">
                        <label class="form-control-label">Homepage Text</label>
                         <textarea class="form-control form-control-alternative" rows="5" name="text">{{$text}}</textarea>
                      </div>
                    </div>
                  </div>
                   <button type="submit"  name="save" class="btn text-white bg-gradient-primary">Update</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     @include('layouts.dashboard.footer')
</div>
 @endsection