@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE DEAL</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a class="dealconfirm" href=""><button class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col">
      @if (count($errors->pro) > 0)
      @foreach ($errors->pro->all() as $error)
      <div class="alert alert-danger">
        {{ $error }}
      </div>
      @endforeach
      @endif
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
    </div>
  </div>
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Comment Details</h3>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">No.</th>
                <th scope="col">User Name</th>
                <th scope="col">Deal Name</th>
                <th scope="col">Comment</th>
                <th scope="col">Dropped On</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <input type="hidden" value="{{$id=1}}">
              @foreach ($comment as $data)
              <tr>
                <td>{{ $id++ }}</td>
                <td>{{ $data->username }}</td>
                <td>{{ $data->dealname }}</td>
                <td>{{ $data->comment }}</td>
                <td>{{ $data->created_at->format('d-M-Y g:ia') }}</td>
                <td>
                  <i class="fa fa-trash text-danger" style="cursor: pointer;" area-hidden='true'  data-toggle="modal" data-target="#confirm-delete"
                       onclick="$('.dealconfirm').attr('href','comments?id={{ $data->id }}')">
                  </i>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end text-light mb-0">
              {{ $comment->links() }}
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
</div>
<!--   Core   -->
@endsection