
@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
          @include('layouts.dashboard.breadcrumb')
        </div>
    </div>
</div>



<!-- Header-end -->
<!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">Membership</h3>
                </div>
                <div class="col text-right">
                  <button style="color:#fff" href="#"  id="memedit" class="btn btn-sm bg-gradient-blue" >Edit</button>
                  {{-- <button style="color:#fff" href="#" data-toggle="modal" data-target="#myModal" class="btn btn-sm bg-gradient-blue ">Add</button> --}}
                </div>
              </div>
            </div>
            <div class="table-responsive" style="text-transform: capitalize;" id="memtable">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Features</th>
                    <th scope="col">Starter</th>
                    <th scope="col">Competitor</th>
                    <th scope="col">Champion</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    @foreach ($data as $mem)
                    <th scope="row">
                      <div class="media align-items-center">
                        <div class="media-body">
                          <span class="mb-0 text-sm">{{ $mem->name }}</span>
                        </div>
                      </div>
                    </th>
                    <td id="{{ $mem->id }}" class="memeditable" contenteditable="false" field='starter'>
                      @if($mem->starter=='no')
                      <i class="fa fa-times text-red"  aria-hidden="true"></i>
                      @elseif($mem->starter=='yes')
                        <i class="fa fa-check text-green"  aria-hidden="true"></i>
                      @else
                        <a  data-toggle="tooltip" >{{ $mem->starter }}</a>
                      @endif
                    </td>
                    <td id="{{ $mem->id }}" class="memeditable" contenteditable="false"  field='competitor'>
                      @if($mem->competitor=='no')
                         <i class="fa fa-times text-red"  aria-hidden="true"></i>
                      @elseif($mem->competitor=='yes')
                        <i class="fa fa-check text-green"  aria-hidden="true"></i>
                      @else
                       <a  >{{ $mem->competitor }}</a>
                      @endif
                    </td>
                    <td id="{{ $mem->id }}" class="memeditable" contenteditable="false"  field='champion'>
                        @if($mem->champion=='no')
                         <i class="fa fa-times text-red"  aria-hidden="true"></i>
                        @elseif($mem->champion=='yes')
                        <i  class="fa fa-check text-green" aria-hidden="true"></i>
                        @else
                        <a  >{{ $mem->champion }}</a>
                        @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
             <div class="card-footer py-4">
            </div>
          </div>
        </div>
      </div>              
      @include('layouts.dashboard.footer')
    </div>
@endsection