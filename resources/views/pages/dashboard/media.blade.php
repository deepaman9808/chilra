@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<style type="text/css">
.thumb #medias
{
position: relative;
}
.thumb .overlay
{
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
.thumb:hover .overlay {
  opacity: 1;
}
.thumb .overlay .text {
    background-color: red;
    color: white;
    font-size: 16px;
    padding: 8px 13px;
}
.thumb .overlay .link {
    background-color: green;
    color: white;
    font-size: 16px;
    padding: 8px 13px;
}
</style>
{{-- add media --}}
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Image</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ ('addmedia')}}" enctype="multipart/form-data">
          {{ @csrf_field() }}
          {{--           <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput1">Name</label>
            <input type="text" class="form-control" name="name">
          </div> --}}
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput2">Image</label>
            <input type="file" class="form-control"  name="image">
          </div>
          <button type="submit" class="btn btn-success" >Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col">
      @if (count($errors->pro) > 0)
      @foreach ($errors->pro->all() as $error)
      <div class="alert alert-danger">
        {{ $error }}
      </div>
      @endforeach
      @endif
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
      <div class="alert alert-success alert-block d-none">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Successfully Copied</strong>
      </div>
    </div>
  </div>
  <!-- Table -->
  <div class="row">
    <div class="col-xl-12 col-md-12">
      <div class="card  shadow">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Add Media</h3>
            </div>
            <div class="col text-right">
              <button style="color:#fff" data-toggle="modal" data-target="#myModal" class="btn btn-sm bg-gradient-blue ">Add Media</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            @foreach($image as $img)
            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
              <img class="img-thumbnail" style="position: relative;" id="medias" src="{{url(asset($img->image))}}">
              <div class="overlay">
                <div class="text d-inline-block">
                  <a href="{{url('media?id=')}}{{$img->id}}"><i class="fa fa-trash text-white" area-hidden="true"></i></a>
                </div>
                <div class="link d-inline-block">
                  <a href="javascript:void(0)"><i src="{{url(asset($img->image))}}" class="fa fa-clone copy-link text-white" area-hidden="true"></i></a>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- Footer -->
@include('layouts.dashboard.footer')
</div>
@endsection