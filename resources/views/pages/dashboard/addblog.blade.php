@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col">
      @if (count($errors->pro) > 0)
      @foreach ($errors->pro->all() as $error)
      <div class="alert alert-danger">
        {{ $error }}
      </div>
      @endforeach
      @endif
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
    </div>
  </div>
  <!-- Table -->
  <div class="row">
    <div class="col-xl-12 col-md-12">
      <div class="card  shadow">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">Add Blog</h3>
            </div>
            <div class="col text-right">
              <a href="{{url('/showblog')}}"><button style="color:#fff" class="btn btn-sm bg-gradient-blue ">Show All Blogs</button></a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ ('saveblog') }}" enctype="multipart/form-data">
            {{ @csrf_field() }}
            <input type="hidden" name="author_image" value="{{Auth::guard('admin')->user()->image}}">
            <h6 class="heading-small text-muted mb-4">Blog content</h6>
            <div class="row">
              <div class="col-md-7 mt-2">
                <div class="form-group">
                  <label class="form-control-label">Blog Name</label>
                  <input type="text" class="form-control form-control-alternative" name="name" required="true">
                </div>
              </div>
              <div class="col-md-7 mt-2">
                <div class="form-group">
                  <label class="form-control-label">Author Name</label>
                  <input type="text" class="form-control form-control-alternative" value="{{Auth::guard('admin')->user()->name}}" name="author_name" required="true">
                </div>
              </div>
              <div class="col-md-7 mt-2">
                <div class="form-group">
                  <label class="form-control-label" for="exampleFormControlSelect1">Category</label>
                  <select class="form-control text-capitalize" name="category" id="exampleFormControlSelect1">
                    @foreach($cats as $cat)
                      <option value="{{$cat->cat}}">{{$cat->cat}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-7 mt-2">
                <div class="form-group">
                  <label class="form-control-label">Blog Thumbnail</label>
                  <input type="file" class="form-control form-control-alternative" name="image" required="true">
                </div>
              </div>
              <div class="col-md-12 mt-2">
                <div class="form-group">
                  <label class="form-control-label">Blog Content</label>
                  <textarea class="form-control form-control-alternative" name="content" id="editor1" rows="10" cols="80">
                    This is my textarea to be replaced with CKEditor.
                  </textarea>
                </div>
              </div>
            </div>
            <button type="submit" class="btn text-white bg-gradient-primary">Save</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
<script>CKEDITOR.replace('editor1');</script>
@endsection