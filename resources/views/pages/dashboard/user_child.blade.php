            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Avatar</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Social Login</th>
                    <th scope="col">Completion</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                   @foreach ($userdata as $user)
                  <tr>
                    <th scope="row">
                        <div class="avatar-group">
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="{{ $user->name }}">
                          <img alt="Image placeholder" style="height: 100%" src="{{ asset($user->image) }}" class="rounded-circle">
                        </a>
                      </div>
                    </th>
                    <td>
                       {{ $user->name }}
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                        
                         @if(!$user->email)
                        <i class="bg-gradient-danger"></i> Null
                        @else
                          <i class="bg-gradient-success"></i>    {{ $user->email }}
                        @endif
                      </span>
                    </td>
                    <td>
                       {{ $user->provider }}
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                         @if($user->profile=='20')
                        <span class="mr-2">20%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-warning" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;"></div>
                          </div>
                        </div>
                        @else
                         <span class="mr-2">100%</span>
                        <div>
                          <div class="progress">
                            <div class="progress-bar bg-gradient-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                          </div>
                        </div>
                        @endif
                      </div>
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v" style="line-height: 2.5;"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow deleteuser">
                          {{-- <a class="dropdown-item" id="viewuser" data-img="{{ asset($user->image) }}" data-name="{{ $user->name }}" data-email="{{ $user->email }}" data-social_login="{{ $user->provider }}" data-completion="{{ $user->profile }}">View</a> 
                          <a class="dropdown-item" href="{{ $user->name }}">Edit</a> --}}
                          <a class="dropdown-item" id="deletecp" data-url="{{url('users?deleteid=')}}{{$user->user_id}}">Delete</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                   @endforeach
                </tbody>
              </table>
            </div>
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination user_bus justify-content-end mb-0">
                  <li class="page-item">
                    {{ $userdata->links() }}
                  </li> 
              </nav>
            </div>