            <div class="table-responsive" >
              <table class="table align-items-center table-flush table-striped">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Avatar</th>
                    <th scope="col">Vendor Name</th>
                    <th scope="col">Business Name</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Email</th>
                    <th scope="col">Plan</th>
                    <th scope="col">Address</th>
                    <th scope="col">Site</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                   @foreach ($vendordata as $data)
                  <tr>
                    <th scope="row">
                        <div class="avatar-group">
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="{{ $data->vendor_name }}">
                          <img alt="Image placeholder" style="height: 100%" src="{{ asset($data->image) }}" class="rounded-circle">
                        </a>
                      </div>
                    </th>
                    <td>
                       {{ $data->vendor_name }}
                    </td>
                    <td>
                      {{ $data->name }}
                    </td>
                    <td>
                       {{ $data->phone }}
                    </td>
                      <td>
                        <span class="badge badge-dot mr-4">
                          @if(!$data->email)
                          <i class="bg-gradient-danger"></i> Null
                          @else
                          <i class="bg-gradient-success"></i>    {{ $data->email }}
                          @endif
                        </span>
                      </td>
                    <td>
                      {{ $data->plan }}
                    </td>
                    <td>
                      {{ $data->address }}
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                         @if(!$data->site)
                        <i class="bg-gradient-danger"></i> Null
                        @else
                          <i class="bg-gradient-success"></i>    {{ $data->site }}
                        @endif
                      </span>
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"style="line-height: 2.5;"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow deletevendor">
                         {{--  <a class="dropdown-item" href="#">View</a>
                          <a class="dropdown-item" href="#">Suspend</a> --}}
                         <a class="dropdown-item" id="deletecp" data-url="{{url('vendors?deleteid=')}}{{$data->merchant_id}}">Delete</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                   @endforeach
                </tbody>
              </table>
            </div>
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination vendor_bus justify-content-end text-light mb-0">
                 {{ $vendordata->links() }}
                </ul>
              </nav>
            </div>
      