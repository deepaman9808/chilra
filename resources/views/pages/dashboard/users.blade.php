@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
	<div class="container-fluid">
		<div class="header-body">
		@include('layouts.dashboard.breadcrumb')
		</div>
	</div>
</div>
{{-- delete modal --}}
<div class="modal fade" id="deletemodal" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE USER DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">NO</button>
        <a id="deletes" class="btn btn-danger yes">YES</a> </div>
      </div>
    </div>
  </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">User Details</h3>
            </div>
               <div id="user_data">
                 @include('pages.dashboard.user_child')
               </div>
            </div>
          </div>
        </div>         
   <!-- Footer -->
     @include('layouts.dashboard.footer')
    </div>
  <!--   Core   -->
 @endsection