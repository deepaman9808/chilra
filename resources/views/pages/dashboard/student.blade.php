@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="modal fade" id="viewusermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">User Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <center>
        <img src="" id="img" name="aboutme" width="140" height="140" border="0" class="rounded-circle">
        </center>
        <br>
        <h4>Name :             <span class="name">       </span></h4>
        <h4>Email :            <span class="email">      </span></h4>
        <h4>Phone :            <span class="phone">      </span></h4>
        <h4>Dob Proof :        <a target="_blank" style="padding: 0px 15px;border-radius:4px;" class="btn-default dob">view</a></span></h4>
        <h4>College :          <span class="college">    </span></h4>
        <h4>Course Studying :  <span class="course">     </span></h4>
        <h4>Address :          <span class="address">    </span></h4>
        <h4>Interest :         <span class="interest">   </span></h4>
        <h4>About :            <span class="urself">     </span></h4>
      </div>
      <div class="modal-footer">
        <center>
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
        </center>
      </div>
    </div>
  </div>
</div>
{{-- delete modal --}}
<div class="modal fade" id="deletemodal" id="" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE STUDENT DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">NO</button>
        <a id="deletes" class="btn btn-danger yes">YES</a> </div>
      </div>
    </div>
  </div>
  <!-- Page content -->
  <div class="container-fluid mt--7">
    <div class="row">
      <div class="col">
        <div class="card shadow">
          <div class="card-header border-0">
            <h3 class="mb-0">Student Details</h3>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush Student">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Avatar</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Approve</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($userdata as $user)
                <tr>
                  <th scope="row">
                    <div class="avatar-group">
                      <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="{{ $user->name }}">
                        <img alt="Image placeholder" style="height: 100%" src="{{ asset($user->image) }}" class="rounded-circle">
                      </a>
                    </div>
                  </th>
                  <td>
                    {{ $user->name }}
                  </td>
                  <td>
                    <span class="badge badge-dot mr-4">
                      @if(!$user->email)
                      <i class="bg-gradient-danger"></i> Null
                      @else
                      <i class="bg-gradient-success"></i>    {{ $user->email }}
                      @endif
                    </span>
                  </td>
                  <td>
                    {{ $user->mobile }}
                  </td>
                  <td>
                    @if($user->status==0)
                    <a href="{{url('/student?action=2&user_id=')}}{{$user->user_id}}"><i class="fa fa-ban text-danger" data-toggle="tooltip" data-original-title="Reject" aria-hidden="true"></i></a> /
                    <a href="{{url('/student?action=1&user_id=')}}{{$user->user_id}}"> <i class="fa fa-check text-success"  data-toggle="tooltip" data-original-title="Accept" aria-hidden="true"></i></a>
                    @elseif($user->status==1)
                    <span class="text-success">Approved</span>
                    @else
                    <span class="text-danger">Rejected</span>
                    @endif
                  </td>
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v" style="line-height: 2.5;"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow deleteuser">
                        <a class="dropdown-item" id="viewuser" data-img="{{ asset($user->image) }}" data-name="{{ $user->name }}" data-email="{{ $user->email }}" data-dob="{{ $user->dob }}" data-phone="{{ $user->mobile }}" data-address="{{ $user->address }}" data-college="{{ $user->college }}" data-course="{{ $user->course }}" data-interest="{{ $user->interest }}" data-urself="{{ $user->urself }}">View</a>
                        {{-- <a class="dropdown-item" href="{{ $user->name }}">Edit</a> --}}
                        <a class="dropdown-item" id="deletecp" data-url="{{url('student?deleteid=')}}{{$user->user_id}}">Delete</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="card-footer py-4">
            <nav aria-label="...">
              <ul class="pagination user_bus justify-content-end mb-0">
                <li class="page-item">
                  {{ $userdata->links() }}
                </li>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      @include('layouts.dashboard.footer')
    </div>
    <!--   Core   -->
    @endsection