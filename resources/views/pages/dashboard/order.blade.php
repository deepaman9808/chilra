@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE ORDER DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a id="deletes" class="orconfirm" ><button class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="container-fluid mt--7">
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Order Details</h3>
        </div>
        <div class="table-responsive" >
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Deal Name</th>
                <th scope="col">Payment Id</th>
                <th scope="col">Amount</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Payment Status</th>
                <th scope="col">Order Status</th>
                <th scope="col">Purchased Date</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($order as $data)
              <tr>
                <td>
                  @if (strlen($data->name) <=30){{$data->name}}
                  @else{{substr($data->name, 0, 30) . '...'}}
                  @endif
                </td>
                <td>
                  {{ $data->payment_id }}
                </td>
                <td>
                  ₹ {{ $data->amount }}
                </td>
                <td>
                  <span class="badge badge-dot mr-4">
                    @if(!$data->email)
                    <i class="bg-gradient-danger"></i> Null
                    @else
                    <i class="bg-gradient-success"></i>    {{ $data->email }}
                    @endif
                  </span>
                </td>
                <td>
                  {{ $data->phone }}
                </td>
                <td>
                  <span class="text-success">{{ $data->status }}</span>
                </td>
                <td>
                  @if($data->order_status=='0')
                  <span class="text-danger">Pending</span>
                  @else
                  <span class="text-success">Completed</span>
                  @endif
                </td>
                <td>
                  {{ $data->created_at->format('d-M-Y g:ia') }}
                </td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"style="line-height: 2.5;"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow deleteorder">
                      {{--  <a class="dropdown-item" href="#">View</a> --}}
                      {{--  <a class="dropdown-item" href="#">Suspend</a> --}}
                      <a class="dropdown-item" id="deletecp" data-url="{{url('orders?deleteid=')}}{{$data->deal_id}}">Delete</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination vendor_bus justify-content-end text-light mb-0">
              {{ $order->links() }}
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
</div>
<!--   Core   -->
@endsection