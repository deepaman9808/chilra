@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE LOCALDEAL HUNTER DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a id="deletes" class="orconfirm" ><button class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="container-fluid mt--7">
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Top 10 Deal Hunters</h3>
        </div>
        <div class="table-responsive" >
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">Avatar</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Followers</th>
                <th scope="col">Address</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($hunter as $data)
              <tr>
                <th scope="row">
                    <div class="avatar-group">
                      <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="{{ $data->name }}">
                        <img alt="Image placeholder" style="height: 100%" src="{{ asset($data->image) }}" class="rounded-circle">
                      </a>
                    </div>
                  </th>
                <td>
                  @if (strlen($data->name) <=30){{$data->name}}
                  @else{{substr($data->name, 0, 30) . '...'}}
                  @endif
                </td>
                <td>
                  <span class="badge badge-dot mr-4">
                    @if(!$data->email)
                    <i class="bg-gradient-danger"></i> Null
                    @else
                    <i class="bg-gradient-success"></i>    {{ $data->email }}
                    @endif
                  </span>
                </td>
                <td>
                  {{ $data->mobile }}
                </td>
                <td>
                  {{ $data->follower }}
                </td>
                <td>
                  {{ $data->address }}
                </td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"style="line-height: 2.5;"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow deleteorder">
                      <a class="dropdown-item" style="cursor: pointer;" id="deletecp" data-url="{{url('dealhunters?deleteid=')}}{{$data->user_id}}">Delete</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
          {{-- <nav aria-label="...">
            <ul class="pagination vendor_bus justify-content-end text-light mb-0">
              {{ $hunter->links() }}
            </ul>
          </nav> --}}
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
</div>
<!--   Core   -->
@endsection