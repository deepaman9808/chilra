@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<style type="text/css">
.modal-body span
{
font-weight: 300;
}
</style>
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<div class="modal fade" id="viewcoumodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Coupon Details</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <center>
          <img src="" id="img" name="aboutme" width="140" height="140" border="0" class="rounded-circle"> 
        </center>
        <br>
        <h4>Coupon Name : <span class="couname">   </span></h4>
        <h4>Coupon Id :   <span class="cou_id">    </span></h4>
        <h4>Code :        <span class="code">      </span></h4>
        <h4>Discount :    <span class="discount">  </span></h4>
        <h4>Validity :    <span class="validity">  </span></h4>
        <hr>
        <center>
        <p class="text-left "><strong>Description: </strong><br>
        <span class="description"></p>
        <br>
        </center>
      </div>
      <div class="modal-footer">
        <center>
        <button type="button" class="btn btn-default" data-dismiss="modal">Okk</button>
        </center>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE COUPON</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a class="couponconfirm" href=""><button class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>
{{-- edit table --}}
<div class="modal" id="editmodalcou">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Coupon</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form method="post" action="{{ ('admincoupon/edit')}}">
          <input type="hidden" name="coupon_id" id="id" value="">
          {{ @csrf_field() }}
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput1">Name</label>
            <input type="text" class="form-control" name="name" id="name" >
          </div>
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput2">Code</label>
            <input type="text" class="form-control"  name="code" id="code">
          </div>
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput3">Discount</label>
            <input type="text" class="form-control"  name="discount" id="discount">
          </div>
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput5">Validity</label>
            <input type="text" class="form-control"  name="validity" id="validity">
          </div>
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput6">Description</label>
            <input type="text" class="form-control"  name="description" id="description">
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col">
      @if (count($errors->pro) > 0)
      @foreach ($errors->pro->all() as $error)
      <div class="alert alert-danger">
        {{ $error }}
      </div>
      @endforeach
      @endif
      @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @endif
    </div>
  </div>
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">Coupon Details</h3>
        </div>
        <div class="table-responsive">
          <table class="table align-items-center table-flush">
            <thead class="thead-light">
              <tr>
                <th scope="col">No.</th>
                <th scope="col">Image</th>
                <th scope="col">Merchant Id</th>
                <th scope="col">Name</th>
                <th scope="col">Code</th>
                <th scope="col">Discount</th>
                <th scope="col">validity Left</th>
                <th scope="col">Description</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <input type="hidden" value="{{$id=1}}">
              @foreach ($coupon as $data)
              <tr>
                <td>
                  {{ $id++ }}
                </td>
                 <th scope="row">
                  <div class="avatar-group">
                    <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="{{ $data->name }}">
                      <img alt="Image placeholder" style="height: 100%" src="{{ asset($data->image) }}" class="rounded-circle">
                    </a>
                  </div>
                </th>
                <td>
                  {{ $data->merchant_id }}
                </td>
                <td>
                  {{ $data->name }}
                </td>
                <td>
                  {{ $data->code }}
                </td>
                <td>
                  {{ $data->discount }} %
                </td>
                <td>
                  {{ now()->diffInDays($data->validity) }} Days
                </td>
                <td>
                  @if (strlen($data->description) <=20){{$data->description}}
                                            @else{{substr($data->description, 0, 20) . '...'}}
                                            @endif
                </td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"style="line-height: 2.5;"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow couadmin">
                      <a class="dropdown-item" id="viewcou" data-img='{{ url(asset($data->image)) }}'  data-id='{{ $data->coupon_id }}'  data-name='{{ $data->name }}' data-code='{{ $data->code }}' data-discount='{{ $data->discount }}' data-validity='{{ $data->validity }}' data-desc='{{ $data->description }}' style="cursor: pointer;">View</a>
                      <a class="dropdown-item" id="editcou"  data-id='{{ $data->coupon_id }}'  data-name='{{ $data->name }}' data-code='{{ $data->code }}' data-discount='{{ $data->discount }}' data-validity='{{ now()->diffInDays($data->validity) }}' data-desc='{{ $data->description }}' style="cursor: pointer;">Edit</a>
                      <a class="dropdown-item" data-toggle="modal" data-target="#confirm-delete" onclick="$('.couponconfirm').attr('href','admin/deletecoupon/{{ $data->coupon_id }}')" style="cursor: pointer;" >Delete</a>
                    </div>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <div class="card-footer py-4">
          <nav aria-label="...">
            <ul class="pagination justify-content-end text-light mb-0">
              {{ $coupon->links() }}
            </ul>
          </nav>
        </div>
        
      </div>
    </div>
  </div>
  <!-- Footer -->
  @include('layouts.dashboard.footer')
</div>
</div>
<!--   Core   -->
@endsection