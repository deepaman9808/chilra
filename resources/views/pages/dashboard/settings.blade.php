@extends('layouts.dashboard.master')<!-- main layout file -->
@section('content')
<!-- Navbar -->
@include('layouts.dashboard.nav')
<!-- End Navbar -->
<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
  <div class="container-fluid">
    <div class="header-body">
      @include('layouts.dashboard.breadcrumb')
    </div>
  </div>
</div>
<!-- Header-end -->
<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">DELETE CATEGORY DETAILS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body"> Do you want to delete this? </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
        <a id="deletes" class="orconfirm" ><button class="btn btn-danger">Delete</button></a>
      </div>
    </div>
  </div>
</div>
{{-- edit table --}}
<div class="modal" id="editmodalcat">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Edit Category</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form method="post" action="{{ ('category/edit')}}">
          <input type="hidden" name="cat_id" id="id" value="">
          {{ @csrf_field() }}
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput2">Name</label>
            <input type="text" class="form-control"  name="cat" id="cat">
          </div>
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput3">Icon</label>
            <input type="text" class="form-control"  name="icon" id="icon">
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
{{-- add cat --}}
<div class="modal" id="addmodalcat">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add New Category</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <form method="post" action="{{ ('category/add')}}">
          {{ @csrf_field() }}
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput2">Name</label>
            <input type="text" class="form-control"  name="cat" id="cat">
          </div>
          <div class="form-group">
            <label class="form-control-label" for="exampleFormControlInput3">Icon</label>
            <input type="text" class="form-control"  name="icon" id="icon">
          </div>
          
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-success" >Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--7">
  <!-- Table -->
  <div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <div class="row align-items-center mb-4">
            <div class="col">
              <h3 class="mb-0">Category Details</h3>
            </div>
            <div class="col text-right">
              <a style="color:#fff" href="javascript:void(0)" class="btn btn-sm bg-gradient-blue addcat">Add</a>
            </div>
          </div>
          @if(count($errors->danger) > 0)
            @foreach ($errors->danger->all() as $error)
            <div class="alert alert-warning">
              {{ $error }}
            </div>
            @endforeach
            @endif
            @if ($message = Session::get('edited'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
             @if ($message = Session::get('added'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
             @if ($message = Session::get('deleted'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
          <div class="table-responsive" style="overflow-x:visible;">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Category Id</th>
                  <th scope="col">Name</th>
                  <th scope="col">Icon</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($cat as $data)
                <tr>
                  <td>
                    {{ $data->id }}
                  </td>
                  <td>
                    {{ $data->cat }}
                  </td>
                  <td>
                    {{ $data->icon }}
                  </td>
                  <td class="text-center">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"style="line-height: 2.5;"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow deleteuser">
                        <a class="dropdown-item" style="cursor: pointer;" id="editcat" data-id="{{ $data->id }}" data-cat="{{ $data->cat }}" data-icon="{{ $data->icon }}" href="#">Edit</a>
                        <a class="dropdown-item" style="cursor: pointer;" id="deletecp" data-url="{{url('category/edit?deleteid=')}}{{$data->id}}">Delete</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div class="card-footer py-4">
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="row mt-5">
      <div class="col-xl-6 col-md-6">
        <div class="card  shadow">
          <div class="card-header bg-white">
            <div class="row align-items-center">
              <div class="col-7">
                <h3 class="mb-0">Site Settings</h3>
              </div>
            </div>
          </div>
          <div class="card-body">
            @if(count($errors->danger) > 0)
            @foreach ($errors->danger->all() as $error)
            <div class="alert alert-warning">
              {{ $error }}
            </div>
            @endforeach
            @endif
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
            <form method="POST" action="{{ ('icons') }}" enctype="multipart/form-data">
              {{ @csrf_field() }}
              <h6 class="heading-small text-muted mb-4">Site information</h6>
              <div class="p-lg-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="profile-img">
                      <img src="{{url(asset($site->banner)) }}" width="100%" class="shadow">
                    </div>
                  </div>
                  <div class="col-lg-8 mt-4">
                    <div class="form-group">
                      <label class="form-control-label">Homepage Banner</label>
                      <input type="file" class="form-control form-control-alternative" name="banner">
                    </div>
                  </div>
                  <div class="col-lg-8 mt-4">
                    <div class="form-group">
                      <label class="form-control-label">Homepage Text</label>
                      <textarea class="form-control form-control-alternative" rows="5" name="text">{{$site->text}}</textarea>
                    </div>
                  </div>
                </div>
                <button type="submit"  class="btn text-white bg-gradient-primary">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-md-6">
        <div class="card  shadow">
          <div class="card-header bg-white">
            <div class="row align-items-center">
              <div class="col-7">
                <h3 class="mb-0">Contact Us Page Settings</h3>
              </div>
            </div>
          </div>
          <div class="card-body">
            @if(count($errors->dangers) > 0)
            @foreach ($errors->dangers->all() as $error)
            <div class="alert alert-warning">
              {{ $error }}
            </div>
            @endforeach
            @endif
            @if ($message = Session::get('successs'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
            <form method="POST" action="{{ ('cont') }}">
              {{ @csrf_field() }}
              <h6 class="heading-small text-muted mb-4">Contact Us information</h6>
              <div class="p-lg-3">
                <div class="row">
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Email</label>
                      <input class="form-control form-control-alternative"  name="email" value="{{$site->email}}">
                    </div>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Phone</label>
                      <input class="form-control form-control-alternative"  name="phone" value="{{$site->phone}}">
                    </div>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Description</label>
                      <textarea class="form-control form-control-alternative" rows="5" name="descr">{{$site->descr}}</textarea>
                    </div>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Address</label>
                      <textarea class="form-control form-control-alternative" rows="3" name="address">{{$site->address}}</textarea>
                    </div>
                  </div>
                </div>
                <button type="submit"  class="btn text-white bg-gradient-primary">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-xl-6 col-md-6">
        <div class="card  shadow">
          <div class="card-header bg-white">
            <div class="row align-items-center">
              <div class="col-7">
                <h3 class="mb-0">Social Media Links Settings</h3>
              </div>
            </div>
          </div>
          <div class="card-body">
            @if(count($errors->social) > 0)
            @foreach ($errors->social->all() as $error)
            <div class="alert alert-warning">
              {{ $error }}
            </div>
            @endforeach
            @endif
            @if ($message = Session::get('succ'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($message = Session::get('warn'))
            <div class="alert alert-warning alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
            <form method="POST" action="{{ ('social') }}">
              {{ @csrf_field() }}
              <h6 class="heading-small text-muted mb-4">Social Links Information</h6>
              <div class="p-lg-3">
                <div class="row">
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Facebook</label>
                      <input class="form-control form-control-alternative"  name="fb" value="{{$site->fb}}">
                    </div>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Twitter</label>
                      <input class="form-control form-control-alternative"  name="tw" value="{{$site->tw}}">
                    </div>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Google Plus</label>
                      <input class="form-control form-control-alternative"  name="gp" value="{{$site->gp}}">
                    </div>
                  </div>
                </div>
                <button type="submit"  class="btn text-white bg-gradient-primary">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
       <div class="col-xl-6 col-md-6">
        <div class="card  shadow">
          <div class="card-header bg-white">
            <div class="row align-items-center">
              <div class="col-7">
                <h3 class="mb-0">Call To Action Button Settings</h3>
              </div>
            </div>
          </div>
          <div class="card-body">
            @if(count($errors->call) > 0)
            @foreach ($errors->call->all() as $error)
            <div class="alert alert-warning">
              {{ $error }}
            </div>
            @endforeach
            @endif
            @if ($message = Session::get('callset'))
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
            </div>
            @endif
            <form method="POST" action="{{ ('calltoaction') }}">
              {{ @csrf_field() }}
              <h6 class="heading-small text-muted mb-4">Call To Action Buttons</h6>
              <div class="p-lg-3">
                <div class="row">
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Button 1</label>
                      <input class="form-control form-control-alternative"  name="btn_one" value="{{$site->btn_one}}">
                    </div>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <div class="form-group">
                      <label class="form-control-label">Button 2</label>
                      <input class="form-control form-control-alternative"  name="btn_two" value="{{$site->btn_two}}">
                    </div>
                  </div>
                </div>
                <button type="submit"  class="btn text-white bg-gradient-primary">Update</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    @include('layouts.dashboard.footer')
  </div>
  @endsection