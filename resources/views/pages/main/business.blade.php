@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
	{{-- breadcrumb --}}
	<section class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>{{$deal->name}}</h1>
					<p>Chilra deal &amp; view or buy page</p>
				</div>
			</div>
		</div>
	</section>
	<input type="hidden" id="user_id" value="{{auth::user()->user_id}}">
	<input type="hidden" id="logname" value="{{auth::user()->name}}">
	<input type="hidden" id="avatar" value="{{url(asset(auth::user()->image))}}">
	<input type="hidden" id="page" value="merchant">
	<div class="modal fade" id="paysModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
				</div>
				<div class="modal-body" id="modalbody">
					<div class="thank-you-pop">
						<img src="http://goactionstations.co.uk/wp-content/uploads/2017/03/Green-Round-Tick.png" alt="">
						<h1>Thank You!</h1>
						<p>Your Payment is received.Go to nearest merchant store And show this Voucher code to merchant</p>
						<div class="couponend">
							<span class="primi">Your Voucher Code  </span><br>
							<div class="ego">
								<span id="dcopy" class="text-success paym_id">₹111 </span>
							</div>
							<div class="copycode p-2">
								<button id="dclip" class="btn btn-warning">CopyCode</button>
							</div>
						</div>
						<p class="hide" style="display: none;color: green">Successfully Copied To Clipboard</p>
						<p>You only need this voucher code to redeem this deal purchase..</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" disabled="true"  id="print">Print</button>
					<a href="{{ url('home/store') }}?id={{$deal->merchant_id}}" class="btn2">View Store</a>
				</div>
			</div>
		</div>
	</div>
	{{-- deal view area --}}
	<div class="container sectiontwo">
		<div class="row mb-3">
			<div class="col-md-8 mt-3">
				<div class="inner">
					<div class="image shadow">
						<img src="{{url(asset($deal->image)) }}" width="100%" height="470">
					</div>
					<div class="white-block bg-white shadow">
						<div class="white-block-content">
							<div class="slider-left">
								<ul class="list-unstyled list-inline bottom-meta">
									<li>
										<div class="item-ratings">
											<i class="fa fa-map-marker icon-margin"></i> <a href="javascript:void(0)" style="font-size: 14px;" class="mr-3">{{$deal->address}}</a>
											
											<div class="rating-stars pull-right">
												@if($dreview==0)
												<div class="before-rate">
													<ul id='stars' class="d-flex" style="list-style-type: none;">
														<li class='star' title='Poor' data-value='1'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='Fair' data-value='2'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='Good' data-value='3'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='Excellent' data-value='4'>
															<i class='fa fa-star fa-fw'></i>
														</li>
														<li class='star' title='WOW!!!' data-value='5'>
															<i class='fa fa-star fa-fw'></i>
														</li>
													</ul>
													<span class="rs mr-4 pull-right"> ({{ $review }} ratings)</span>
												</div>
												<div class='success-box'>
													<div class='text-message'></div>
												</div>
												@else
												<div class="after-rate">
													@if(1 || 2 || 3 || 4 || 5 == $star)
													@for($i=1;$i<=$star;$i++)
													<i class="fa fa-star" backup-class="fa fa-star"></i>
													@endfor
													@for($i=$star;$i<5;$i++)
													<i class="fa fa-star-o" aria-hidden="true"></i>
													@endfor
													@endif
													<span class="rs"> (Based on {{ $review }} ratings)</span>
												</div>
											</div>
											@endif
										</div>
									</li>
								</ul>
								<h6><a href="javascript:void(0)" class="text-body"></a></h6>
								<div class="dealinfo pl-3 pt-0"><a  href="javascript:void(0)">{{$deal->name}}</a></div>
								<div class="content">
									<p>{{$deal->description}}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 mt-3">
				<div class="inner shadow bg-white">
					<div class="slider-right">
						<div class="widget-tile">
							<h1 class="price">₹{{$deal->price-$deal->price*$deal->discount/100}} <span class="price-sale">₹{{$deal->price}}</span></h1>
						</div>
						<div class="price text-center mt-4">
							<a  class="btn2 buy_now" href="javascript:void(0)" data-email="{{Auth::user()->email}}" data-phone="{{$user->mobile}}" data-image="{{ url(asset('images/brand/13077.png')) }}" >BUY NOW</a>
						</div>
						<div class="expire">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							<div class="info">
								<p>TIME LEFT - LIMITED OFFER!</p>
								<h4>
								<span id="clock"></span><input type="hidden" id="validity" value="{{$deal->validity}}">
								</h4>
							</div>
						</div>
						<div class="bought">
							<i class="fa fa-users" aria-hidden="true"></i>
							<div class="info">
								<h4>Bought <span class="bout">{{$deal->bought}}</span></h4>
							</div>
						</div>
						<ul class="wrap list-unstyled list-inline d-flex">
							<li>
								<p>VALUE</p>
								<h6>₹{{$deal->price}}</h6>
							</li>
							<li>
								<p>DISCOUNT</p>
								<h6>{{$deal->discount}}%</h6>
							</li>
							<li>
								<p>YOU SAVE</p>
								<h6>₹{{$deal->price/100*$deal->discount}}</h6>
							</li>
						</ul>
						<ul class="social list-unstyled list-inline d-flex">
							<li>
								<a href="https://www.facebook.com/sharer/sharer.php?u={{request()->Fullurl()}}" target="_blank"
									onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=no,height=300,width=500');return false;"
									 title="Share on Facebook">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="https://api.whatsapp.com/send?text={{url('/home/business?id=')}}{{request()->id}}{{urlencode('&whatsapp=true')}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=no,height=350,width=500');return false;">
									<i class="fa fa-whatsapp"></i>
								</a>
							</li>
							<li>
								<a class="clipb" href="javascript:void(0)" id="{{request()->Fullurl()}}&link=true">
									<i class="fa fa-link"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="store">
					<div class="inner shadow bg-white">
						<div class="view">
							<a href="{{ url('home/store') }}?id={{$deal->merchant_id}}" class="btn2">View Store</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-8 mt-3">
				<div class="inner">
					<div class="comment-wrapper bg-white shadow">
						<div class="panel panel-info">
							<div class="panel-heading">Comments</div>
							<div class="panel-body">
								<div class="textarea">
									<textarea class="form-control" id="commentarea" placeholder="write a comment..." rows="3"></textarea>
								</div>
								<div class="button text-right mt-3">
									<button type="button" id="postcm" class="btn btn-success">Post</button>
								</div>
								<div class="comment-area-section mt-5">
									<ul class="media-list">
										<div class="appending"></div>
										@foreach($comment as $key=>$value)
										<hr>
										<li class="media p-4">
											<a href="javascript:void(0)" class="img">
												<img src="{{url(asset($value->image))}}" alt="" class="img-rounded">
											</a>
											<div class="media-body ml-3 py-2">
												<a href="{{url('userprofile?id=')}}{{$value->user_id}}"><strong class="text-success">{{$value->name}}</strong></a>
												<span> made a comment</span>
												<br>
												<span class="text-muted">
													<small class="text-muted">{{$value->created_at->diffForHumans()}}</small>
												</span>
												<p>{{$value->comment}}</p>
												@if($value->user_id == auth::guard('web')->user()->user_id)
												<div class="text-right " style="cursor: pointer;">
													<a href="{{url('/home/business?id=')}}{{request()->id}}{{('&enc=')}}{{$value->id}}{{('&user_id=')}}{{($value->user_id)}}">
														<i class="fa fa-trash orange" area-hidden="true"></i>
													</a>
												</div>
												@endif
											</div>
										</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.main.footer')
@endsection