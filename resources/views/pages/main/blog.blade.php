@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area">
	{{-- breadcrumb --}}
	<section class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Blog</h1>
					<p>Chilra Blog Page</p>
				</div>
			</div>
		</div>
	</section>
	<section class="sectiontwo sectionfour sectionfifth">
		<div class="card-section">
			{{-- =================================================section deals ================================================== --}}
			<div class="container blog">
				<div class="row mt-3">
					<div class="col-md-12">
						<div class="section-divider bg-white shadow" style="box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important">
							<div class="white-block-title">
								<i class="fa fa-rss"></i><h2>Recent Blogs</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container sectionfour blog">
				<div class="row mt-5">
					@foreach($blog as $key=>$data)
					<div class="col-md-4 mb-5">
						<div class="thumbnail">
							<img src="{{url(asset($data->image))}}" class="img-fluid rounded mb-4">
						</div>
						<div class="white-block-contents ml-3 mr-3">
							<span class="cats mb-3 {{$data->color}}">{{$data->category}}</span>
							<a href="{{url('home/blogpage?id=')}}{{$data->blog_id}}"><h2>{{$data->name}}</h2></a>
							<ul class="list-unstyled list-inline d-flex bottom-meta mb-0">
								<li>
									<img src="{{url(asset($data->author_image))}}" class="img-fluid rounded mb-4">
								</li>
								<li class="mt-1 text-dim ">
									<span class="by">By </span><span class="text-dark">{{$data->author_name}}</span><span class="date ml-2">  -  {{$data->created_at->diffForHumans()}}</span>
								</li>
							</ul>
							{{-- <div class="white-block-footer bottom-meta">
								<div class="text-p">
									{!! html_entity_decode($data->content) !!}
									<p>
										<a href="#">Read More</a>
									</p>
								</div>
							</div> --}}
						</div>
					</div>
					@endforeach
				</div>
				<nav aria-label="...">
					<ul class="pagination vendor_bus justify-content-center text-light">
						{{ $blog->links() }}
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>
</div>
@include('layouts.main.footer')
@endsection