@extends('layouts.main.master')<!-- main layout file -->
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
  {{-- breadcrumb --}}
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Recover Password</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="main-content login">
    <div class="header  py-7 py-lg-8"></div>
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12">
          <div class="card bg-white logcard shadow">
            <div class="card-body bg-transparent">
              <div class="white-block-title bored">
                <i class="fa fa-lock"></i>
                <h2>Recover Password</h2>
              </div>
              <div class="row">
                <div class="col-md-6 p-3">
                    @if ($message = Session::get('danger'))
                    <div class="alert alert-warning">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>{{ $message }}</strong>
                    </div>
                    @endif
                  </div>
                <div class="col-md-8">
                  <form role="form" method="POST" action="{{ ('sendemailuser') }}">
                    {{ @csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="email" class="form-control" name="email" placeholder="EMAIL" id="email">
                        </div>
                      </div>
                      <div class="col-md-4 mt-2 side">
                        <button type="submit" class="btn reg-btn">RECOVER PASSWORD</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('layouts.main.footer')
  @endsection