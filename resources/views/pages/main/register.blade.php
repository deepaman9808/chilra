@extends('layouts.main.master')<!-- main layout file -->
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
  {{-- breadcrumb --}}
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Register</h1>
        </div>
      </div>
    </div>
  </section>
  <div class="main-content login register">
    <div class="header  py-7 py-lg-8"></div>
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-11 col-md-11">
          <div class="card bg-white regcard shadow">
            <div class="card-body bg-transparent">
              <div class="white-block-title bored">
                <i class="fa fa-lock"></i>
                <h2>Register</h2>
                <div class="row">
                  <div class="col-md-6 ml-4">
                    @if (count($errors->register) > 0)
                    @foreach ($errors->register->all() as $error)
                    <div class="alert alert-warning alert-block">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>{{ $error }}</strong>
                    </div>
                    @endforeach
                    @endif
                  </div>
                </div>
              </div>
              <div class="row mb-4 mt-3">
                <div class="col-md-12">
                  <form role="form" method="POST" id="form" action="{{ url('registered') }}" enctype="multipart/form-data">
                    {{ @csrf_field() }}
                    <div class="row">
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="name">NAME</label>
                          <input type="text" class="form-control" name="name"  id="name" value="{{ old('name') }}">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="email">EMAIL</label>
                          <input type="email" class="form-control" name="email"  id="email" value="{{ old('email') }}">
                        </div>
                      </div>
                      <div class="col-md-10">
                        <div class="form-group">
                          <label for="image">IMAGE</label>
                          <input type="file" class="form-control" name="image"  id="image" >
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="password">PASSWORD</label>
                          <input type="password" class="form-control" name="password"  id="password">
                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="image">REPEAT PASSWORD</label>
                          <input type="password" class="form-control" name="password_confirmation"  id="cpassword">
                        </div>
                      </div>
                    </div>
                    <div class="text-muted text-italic"><small>Password Strength: <span id="password-strength-status" class="text-danger font-weight-700">weak</span></small>
                    </div>
                    <div class="row my-4">
                      <div class="col-12">
                        <div class="custom-control custom-control-alternative custom-checkbox">
                          <input class="custom-control-input" id="customCheckRegister" name="policy" type="checkbox">
                          <label class="custom-control-label" for="customCheckRegister">
                            <span class="text-muted">I agree with the <a href="{{url('/privacypolicy')}}">Privacy Policy</a></span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12 mt-3 p-0">
                      <button type="submit" class="btn login-btn">REGISTER</button>
                      <a href="{{ url('auth/redirect/facebook') }}">
                        <button type="button" class="btn fb-btn"><i class="fa fa-facebook"></i> Facebook</button>
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('layouts.main.footer')
  @endsection