@extends('layouts.main.master')<!-- main layout file -->
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
	{{-- breadcrumb --}}
	<section class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Profile</h1>
				</div>
			</div>
		</div>
	</section>
	@include('pages.main.user.modal')
	<div class="main-content login profile">
		<div class="header  py-7 py-lg-8"></div>
		<div class="container mt--8 pb-5">
			<div class="row justify-content-center">
				<div class="col-lg-12 col-md-12">
					<div class="card bg-white prcard shadow">
						<div class="card-body bg-transparent">
							<div class="white-block-title bored">
								<i class="fa fa-lock"></i>
								<h2>Profile</h2>
								<div class="pull-right">
									<ul class="list-unstyled d-flex list-inline">
										{{-- <li><a href="{{url('/logout')}}" class="log-out "><i class="fa fa-power-off"></i></a></li> --}}
										{{-- <li><p style="padding:20px 2px; ">{{Auth::user()->name }}</p></li> --}}
										<li><img src="{{url(asset(Auth::user()->image)) }}" class="avatar " alt="avatar"></li>
									</ul>
								</div>
							</div>
							<div class="card w-100 p-2 mt-0" style="border: 0px;">
								<input type="hidden" {{$p=auth::user()->profile}} {{$s=auth::user()->status}}>
								<div class="card-header bg-white">
									<div class="tabbable-responsive">
										<div class="tabbable">
											<ul class="nav nav-tabs profilearea" id="myTab" role="tablist">
												<li class="nav-item">
													<a class="nav-link" id="first-tab" data-toggle="tab" data-id='1' href="#first" role="tab" aria-controls="first" aria-selected="true">
														<i class="fa fa-tachometer" aria-hidden="true"></i>
														<h4>My Dashboard</h4>
														{{-- <p>Quick information about your profile</p> --}}
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" id="second-tab" data-toggle="tab" data-id='2' href="#second" role="tab" aria-controls="second" aria-selected="false">
														<i class="fa fa-clone" aria-hidden="false"></i>
														<h4>Local Deals</h4>
														{{-- <p>Check here deas you have submitted and add new one</p> --}}
													</a>
												</li>
												<li class="nav-item ">
													<a class="nav-link " id="third-tab" data-toggle="tab" data-id='3' href="#third" role="tab" aria-controls="third" aria-selected="false">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
														<h4>Apply</h4>
														{{-- <p>Before activating your account you have to submit this info</p> --}}
													</a>
												</li>
												<li class="nav-item ">
													<a class="nav-link" id="fourth-tab" data-toggle="tab" data-id='4' href="#fourth" role="tab" aria-controls="fourth" aria-selected="false">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
														<h4>My Purchases</h4>
														{{-- <p>Check here deals you have purchased</p> --}}
													</a>
												</li>
												<li class="nav-item">
													<a class="nav-link" id="fifth-tab" data-toggle="tab" data-id='5' href="#fifth" role="tab" aria-controls="fifth" aria-selected="false">
														<i class="fa fa-cog" aria-hidden="true"></i>
														<h4>Settings</h4>
														{{-- <p>Change your profile details here</p> --}}
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="card-body pills inner">
									<div class="tab-content">
										<div class="tab-pane fade" id="first" role="tabpanel" aria-labelledby="first-tab">
											@if($p==20 || $p==100 &&  $s==0 || $s==2)
											<h5 class="card-title">Your Report Here</h5>
											<p class="card-text">You are not verified yet to see dashboard</p>
											@else
											<ul class="list-unstyled dash item-ratings">
												<li><i class="fa fa-tags"></i>My Followers:            <span class="badge text-success">{{$follower}}</span></li>
												<li><i class="fa fa-hand-o-right"></i> Deals Posted:    <span class="badge text-danger">{{$mydeals}}</span></li>
												<li><i class="fa fa-bar-chart"></i> Sales:       <span class="badge" style="color: orangered">{{$sales}}</span></li>
												<li><i class="fa fa-star-o"></i> Average Deal Ratings:
													<span class="badge" style="font-size: 16px;">@if(1 || 2 || 3 || 4 || 5 == $rate)
														@for($i=1;$i<=$rate;$i++)
														<i class="fa fa-star" backup-class="fa fa-star"></i>
														@endfor
														@for($i=$rate;$i<5;$i++)
														<i class="fa fa-star-o" aria-hidden="true"></i>
														@endfor
													@endif</span></li>
													<li class="due"><i class="fa fa-dollar"></i> Total Money Saved:
														<span class="badge">₹ @if(!$data->isEmpty()) {{$saving}}  @else 0.00 @endif</span></li>
														<li class="sent"><i class="fa fa-money"></i> Total Money spent: <span class="badge">₹ {{$sum}}</span></li>
													</ul>
													@endif
												</div>
												<div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
													@if($p==20 || $p==100  && $s==0 || $s==2)
													<h5 class="card-title">Add Local Deals</h5>
													<p class="card-text">You can add local deals here after your profile has been verified by admin</p>
													@else
													<div class="container">
														<div class="button" data-toggle="modal" data-target="#addlocmodal" style="cursor: pointer;">
															<h5 class="add-deals"><i class="fa fa-plus"  area-hidden="true"></i> Add Deal</h5>
														</div>
														<div class="row">
															<div class="col-xl-7">
																@if (count($errors->deal) > 0)
																@foreach ($errors->deal->all() as $error)
																<div class="alert alert-warning">
																	{{ $error }}
																</div>
																@endforeach
																@endif
																@if ($message = Session::get('success'))
																<div class="alert alert-success alert-block">
																	<button type="button" class="close" data-dismiss="alert">×</button>
																	<strong>{{ $message }}</strong>
																</div>
																@endif
															</div>
															<div class="col-md-12 inner">
																@include('pages.main.user.mylocal')
															</div>
														</div>
													</div>
													@endif
												</div>
												<div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
													@if($p==20  &&  $s==0)
													@include('pages.main.user.apply')
													@elseif($p==100 &&  $s==2)
													<h5 class="card-title">Your Review Status</h5>
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 justify-content-center">
														<div class="box-part text-center">
															<i class="fa fa-ban fa-3x text-danger" aria-hidden="true"></i>
															<div class="title"><h4>Rejected</h4></div>
															<div class="text">
																<span>You profile is rejected by admin.</span>
															</div>
														</div>
													</div>
													@elseif($p==100 &&  $s==1)
													<h5 class="card-title">Your Review Status</h5>
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 justify-content-center">
														<div class="box-part text-center">
															<i class="fa fa-check fa-3x text-success" aria-hidden="true"></i>
															<div class="title"><h4>Accepted</h4></div>
															<div class="text">
																<span>You profile is Accepted by admin.You can add local deals and explore site freely.</span>
															</div>
														</div>
													</div>
													@else
													<h5 class="card-title">Your Review Status</h5>
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 justify-content-center">
														<div class="box-part text-center">
															<i class="fa fa-clock-o fa-3x orange" aria-hidden="true"></i>
															<div class="title"><h4>Pending</h4></div>
															<div class="text">
																<span>You profile is in review.we will notify you when it's approved.</span>
															</div>
														</div>
													</div>
													@endif
												</div>
												<div class="tab-pane fade" id="fourth" role="tabpanel" aria-labelledby="fourth-tab">
													@if($p==20 || $p==100  && $s==0 || $s==2)
													<h5 class="card-title">You purchased Deals Here</h5>
													<p class="card-text">Nothing for now.. Come back lator</p>
													@else
													<div class="container">
														<div class="row">
															<div class="col-md-12">
																<table class="table table-striped tablepr table-responsive fixed-table-body">
																	<thead>
																		<tr>
																			<th>Deal</th>
																			<th>Voucher Code</th>
																			<th>Status</th>
																			<th>Purchase Date</th>
																			<th>Price</th>
																			<th>Payment</th>
																		</tr>
																	</thead>
																	<tbody>
																		@foreach($data as $deal)
																		<tr>
																			<td><a target="_blank" href="{{url('home/business?id=')}}{{$deal->deal_id}}">{{$deal->name}}</a></td>
																			<td>{{$deal->payment_id}}</td>
																			<td>@if($deal->order_status==0)Not used @else Used @endif</td>
																			<td>{{$deal->created_at->format('d-M-Y g:i A')}}</td>
																			<td>₹{{$deal->amount}}</td>
																			<td>{{$deal->status}}</td>
																		</tr>
																		@endforeach()
																	</tbody>
																</table>
															</div>
														</div>
													</div>
													@endif
												</div>
												<div class="tab-pane fade" id="fifth" role="tabpanel" aria-labelledby="fifth-tab">
													@if($p==20 || $p==100  && $s==0 || $s==2)
													  <h5 class="card-title">Your Profile settings Here</h5>
													  <p class="card-text">Nothing for now.. Come back lator</p>
													@else
													  @include('pages.main.user.setting')
													@endif
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@include('layouts.main.footer')
			@endsection