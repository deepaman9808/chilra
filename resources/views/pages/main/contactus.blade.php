@extends('layouts.main.master')<!-- main layout file -->
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
	{{-- breadcrumb --}}
	<section class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Contact Us</h1>
					<p>Chilra Contact Us Page</p>
				</div>
			</div>
		</div>
	</section>
	<div class="main-content login profile">
		<div class="header  py-7 py-lg-8"></div>
		<div class="container mt--8 pb-5">
			<div class="row justify-content-center">
				<div class="col-lg-12 col-md-12">
					<div class="card bg-white prcard shadow">
						<div class="card-body bg-transparent">
							<div class="row mb-3">
								<div class="col-md-12">
									<form role="form" method="POST" id="form" action="{{ url('contactdb') }}">
										{{ @csrf_field() }}
										<div class="row">
											<div class="col-md-6 mb-3">
												<div class="white-block-title bored">
													<i class="fa fa-envelope-o"></i>
													<h2>Contact Us</h2>
												</div>

												<div class="message-area mt-3">
													<div class="form-group">
														<input type="text" class="form-control" name="name" placeholder="NAME"  id="name">
													</div>
													<div class="form-group">
														<input type="email" class="form-control" name="email" placeholder="EMAIL" id="email">
													</div>
													<div class="form-group">
														<textarea type="password" id="message" rows="8" class="form-control" placeholder="MESSAGE" name="message"></textarea>
													</div>
													<button type="submit"  class="btn login-btn">Submit Message</button>
													<div class="mt-3">
													  @if (count($errors->contact) > 0)
													   @foreach ($errors->contact->all() as $error)
														<div class="alert alert-warning">
															<button type="button" class="close" data-dismiss="alert">×</button>
															<strong>{{ $error }}</strong>
														</div>
														@endforeach
														@endif
														@if ($message = Session::get('success'))
														<div class="alert alert-success">
															<button type="button" class="close" data-dismiss="alert">×</button>
															<strong>{{ $message }}</strong>
														</div>
														@endif
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="white-block-title bored">
													<i class="fa fa-map-marker"></i>
													<h2>Additional Info</h2>
												</div>
												<div class="address-area mt-3">
													<p>{{$con->descr}}</p>
													<h6 style="font-weight: 600;">Street Address</h6>
													<p>{{$con->address}}</p>
													<h6 style="font-weight: 600;">Contact Us</h6>
													<ul class="list-inline">
														<li style="font-size: 14px;">Email : {{$con->email}}</li>
														<li style="font-size: 14px;">Phone : {{$con->phone}}</li>
													</ul>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.main.footer')
@endsection