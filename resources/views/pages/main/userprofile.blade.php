@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
    {{-- breadcrumb --}}
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>User Profile</h1>
                    <p>Chilra Userprofile View Page</p>
                </div>
            </div>
        </div>
    </section>
    <input type="hidden" id="login_id" value="{{auth::user()->user_id}}">
    <div class="main-content sectionfifth sectiontwo login profile inner">
        <div class="header  py-7 py-lg-8 "></div>
        <div class="container mt--8 pb-5 item-ratings">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12">
                    <div class="card bg-white prcard shadow">
                        <div class="card-body bg-transparent">
                            <div class="white-block-title bored">
                                <i class="fa fa-lock"></i>
                                <h2>Profile</h2>
                            </div>
                            <div class="card emp-profile">
                                <div class="row">
                                    <div class="col-md-4 mb-3">
                                        <div class="profile-img">
                                            <img src="{{url(asset($profile->image))}}" alt="avatar">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="profile-head" >
                                            <div class="Personal mb-3">
                                                <h5>
                                                {{$profile->name}}
                                                </h5>
                                                <p class="proile-rating">Followers : <span>{{$follower}}</span></p>
                                                <p class="proile-rating item-ratings">Deal Ratings : <span>
                                                    @if(1 || 2 || 3 || 4 || 5 == $avg)
                                                    @for($i=1;$i<=$avg;$i++)
                                                    <i class="fa fa-star" backup-class="fa fa-star"></i>
                                                    @endfor
                                                    @for($i=$avg;$i<5;$i++)
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    @endfor
                                                    @endif
                                                </span>
                                            </p>
                                            <p class="proile-rating">Deals Posted : <span>{{$count}}</span></p>
                                            @if(auth::guard('web')->user()->user_id!=request()->id)
                                            <p class="proile-rating" >Follow : <a href="javascript:void(0)" style="padding: 0px 10px;" id="Follow" class="btn btn-success"><i class="fa fa-user"></i> Follow</a> <span class="followdone text-success"></span></p>
                                            @else
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="profile-work">
                                        <p>Profile Info</p>
                                        <ul class="list-inline">
                                            <li><a href="">Profile : <span class="font orange">Verified</span>
                                        <i class="fa fa-check text-success" aria-hidden="true"></i></a>
                                    </li>
                                    <li><a href="">Gender : <span class="orange text-capitalize">{{$profile->gender}}</span></a></li>
                                    <li><a href="">Joined On : <span class="orange font">{{$profile->created_at->format('d-M-Y')}}</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 mt-2">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active orange" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link orange" id="deals-tab" data-toggle="tab" href="#deals" role="tab" aria-controls="deals" aria-selected="false">Deals</a>
                                </li>
                            </ul>
                            <div class="tab-content profile-tab" id="myTabContent">
                                <div class="tab-pane fade show active p-2" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Name</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="orange">{{$profile->name}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Interest</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{$profile->interest}}</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>About Me</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>{{$profile->urself}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="deals" role="tabpanel" aria-labelledby="deals-tab">
                                    @if($local)
                                    @foreach($local as $del)
                                    <div class="row mb-3">
                                        <div class="col-md-4">
                                            <div class="image mt-2">
                                                <img src="{{url(asset($del->image))}}">
                                            </div>
                                        </div>
                                        <div class="col-sm-8 inner bg-white">
                                            <div class="white-block-content" style="padding: 10px 0px 0 0;">
                                                <div class="item-ratings">
                                                    @if(1 || 2 || 3 || 4 || 5 == $del->star)
                                                    @for($i=1;$i<=$del->star;$i++)
                                                    <i class="fa fa-star" backup-class="fa fa-star"></i>
                                                    @endfor
                                                    @for($i=$del->star;$i<5;$i++)
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                    @endfor
                                                    @endif
                                                    <span class="rs"> ({{$del->rating}} Ratings)</span>
                                                    <div class="pull-right">
                                                        @if(now()->diffInDays($del->validity)==0)
                                                        <span class="rs">Expires on:</span>
                                                        <span class="red-meta"> Today </span>
                                                        @else
                                                        <span class="rs">Expires in:</span>
                                                        <span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <h6><a href="javascript:void(0)" class="text-body">{{$del->name}}</a></h6>
                                                <ul class="list-unstyled list-inline bottom-meta pb-0">
                                                    <li>
                                                        <i class="fa fa-map-marker"></i><a href="javascript:void(0)" class="mr-3">{{$del->loc}}</a>
                                                    </li>
                                                    <li>
                                                        <span class="rs" style="font-size: 14px;">{{$del->description}}</span>
                                                    </li>
                                                </ul>
                                                <div class="slider-right">
                                                    <div class="slider-action-button">
                                                        <div class="row mt-2">
                                                            <div class="col-md-6 ">
                                                                <h2 class="price">₹{{$del->price-$del->price*$del->discount/100}} <span class="price-sale">₹{{$del->price}}</span></h2>
                                                            </div>
                                                            <div class="col-md-6" style="margin-top: 3px;">
                                                                <div class="local">
                                                                    <a href="{{ url('localbuy?id') }}={{$del->deal_id}}" class="btn2">VIEW DEAL</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="row">
                                        <div class="col-md-12 mb-5">
                                            <div class="inner shadow bg-white">
                                                <div class="white-block p-3">
                                                    <div class="white-block-c">
                                                        <p class="nothing-found">Currently there are no local deals available.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@include('layouts.main.footer')
@endsection