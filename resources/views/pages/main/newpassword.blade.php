@extends('layouts.main.master')<!-- main layout file -->
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
	{{-- breadcrumb --}}
	<section class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>New Password</h1>
				</div>
			</div>
		</div>
	</section>
	<div class="main-content login">
		<div class="header  py-7 py-lg-8"></div>
		<div class="container mt--8 pb-5">
			<div class="row justify-content-center">
				<div class="col-lg-12 col-md-12">
					<div class="card bg-white logcard shadow">
						<div class="card-body bg-transparent">
							<div class="white-block-title bored">
								<i class="fa fa-lock"></i>
								<h2>Set New Password</h2>
							</div>
							<div class="row mt-4 p-4">
								<div class="col-md-6">
									@if (count($errors->danger) > 0)
									@foreach ($errors->danger->all() as $error)
									<div class="alert alert-warning">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>{{ $error }}</strong>
									</div>
									@endforeach
									@endif
									@if ($message = Session::get('success'))
									<div class="alert alert-success">
										<button type="button" class="close" data-dismiss="alert">×</button>
										<strong>{{ $message }}</strong>
									</div>
									@endif
								</div>
								<div class="col-md-8">
									<form role="form" id="form" method="POST" action="{{ url('setpassworduser') }}">
										{{ @csrf_field() }}
										<input type="hidden" name="code" value="{{ $code }}">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input type="password" class="form-control" name="password" placeholder="PASSWORD" id="password" value="{{ old('password')}}">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input class="form-control" name="password_confirmation" id="cpassword" placeholder="REPEAT PASSWORD" type="password" >
												</div>
											</div>
										</div>
										<div class="text-muted text-italic"><small>Password Strength: <span id="password-strength-status" class="text-danger font-weight-700">Weak</span></small></div>
								</div>
								<div class="col-md-4 ml-2 mt-2 side">
									<button type="submit" class="btn reg-btn">SAVE</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('layouts.main.footer')
		@endsection