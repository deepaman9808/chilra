@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
  {{-- breadcrumb --}}
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>All Deal Hunters</h1>
          <p>Chilra All Deal Hunters View Page</p>
        </div>
      </div>
    </div>
  </section>
  {{-- featured deals section --}}
  <div class="container sectiontwo sectionfour sectionfifth">
    <div class="row">
      <div class="col-md-12">
        @if(!$hunters->isEmpty())
          <div class="row">
            <div class="col-md-12 col-xs-12">
              <div class="section-divider bg-white shadow">
                <div class="white-block-title">
                  <i class="fa fa-user" area-hidden="true"></i><h2>All Deal Hunters</h2>
                </div>
              </div>
            </div>
          </div>
        <div class="row mt-5 mb-5">
          @foreach($hunters as $key=> $us)
          <div class="col-md-4 mb-5">
            <div class="inner bg-white shadow">
              <img src="{{url(asset($us->image))}}" width="100%" height="300">
              <div class="white-block-content">
                <ul class="list-unstyled list-inline item-ratings">
                  <li>
                    <span class="orange"> Verified <i class="fa fa-check text-success"></i></span>
                    <div class="pull-right"><span class="rs">Followers:</span> <span class="red-meta">{{$us->follower}}</span></div>
                  </li>
                </ul>
                <h6 class="text-body">{{$us->name}}</h6>
                <ul class="list-unstyled list-inline bottom-meta">
                  <li>
                    <i class="fa fa-map-marker"></i><a href="#"class="mr-3">{{$us->address}}</a>
                  </li>
                </ul>
                <hr>
                <i class="fa fa-user rr" aria-hidden="true"></i><a href="{{url('userprofile?id=')}}{{$us->user_id}}" class="btn3">Visit Profile</a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      @else
      <div class="row">
        <div class="col-md-12 mb-5">
          <div class="inner shadow bg-white">
            <div class="white-block p-3">
              <div class="white-block-c">
                <p class="nothing-found">Currently there is no store or this category has no results.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      {{-- all deal section end --}}
    </div>
  </div>
</div>
</div>
</div>
@include('layouts.main.footer')
@endsection