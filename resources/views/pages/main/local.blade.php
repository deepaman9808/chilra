@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
  {{-- breadcrumb --}}
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>Local Deals</h1>
          <p>Chilra local deal search page</p>
        </div>
      </div>
    </div>
  </section>
  {{-- featured deals section --}}
  <div class="container sectiontwo sectionfour sectionfifth">
    <div class="row">
      <div class="col-md-3">
        <div class="inner bg-white shadow">
          <ul class="list-unstyled">
            <li class="nav-item secli border border-light">
              <h4 class="looking">Categories</h4>
            </li>
            <li class="nav-item active secli border-0">
              <a href="{{ url('local') }}"><span style="position: unset;" class="catname">All Categories</span></a>
            </li>
            @foreach($cat as $ct)
            <li class="nav-item @if(request()->cat== $ct->id ) active @endif secli border-0">
              <a href="{{ url('local?cat=') }}{{$ct->id}}"><span class="catname text-capitalize">{{$ct->cat}}</span></a>   <span class="count">{{$ct->count}}</span> 
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        @if(!$local->isEmpty())
        <div class="viewmode bg-white mb-5 shadow">
          <div class="white-block offer-filter ">
            <div class="white-block-title2 pt-3">
              <div class="row">
                <div class="col-sm-3">
                  <ul class="list-unstyled list-inline">
                    <li class="view">
                      <a href="javascript:void(0)" class="acti">Local Deals</a>
                      <a href="javascript:void(0)" class="active2"><i class="fa fa-th-large"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        @foreach($local as $del)
        <div class="row mb-5 localdeal">
          <div class="col-md-4">
            <div class="image">
              <img src="{{url(asset($del->image))}}">
            </div>
          </div>
          <div class="col-sm-8 inner localblock">
            <div class="white-block-content bg-white">
              <div class="item-ratings">
                  @if(1 || 2 || 3 || 4 || 5 == $del->star)
                  @for($i=1;$i<=$del->star;$i++)
                  <i class="fa fa-star" backup-class="fa fa-star"></i>
                  @endfor
                  @for($i=$del->star;$i<5;$i++)
                  <i class="fa fa-star-o" aria-hidden="true"></i>
                  @endfor
                  @endif
                  <span class="rs"> ({{$del->rating}} rates)</span>
                  <div class="pull-right">
                    @if(now()->diffInDays($del->validity)==0)
                      <span class="rs">Expires on:</span>
                      <span class="red-meta"> Today </span>
                    @else
                      <span class="rs">Expires in:</span>
                      <span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
                    @endif
                  </div>
                </div>
                <h6><a href="javascript:void(0)" class="text-body">{{$del->name}}</a></h6>
                <ul class="list-unstyled list-inline bottom-meta">
                  <li>
                    <i class="fa fa-map-marker"></i>
                      <a href="javascript:void(0)" class="mr-3">{{$del->loc}}</a>
                  </li>
                  <li>
                      <a href="javascript:void(0)" class="text-body" style="font-size: 14px;">{{$del->description}}</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-12" style="border: 1px solid #f2f2f2">
              <div class="inner bg-white white-block-content">
                <div class="slider-right">
                  <div class="slider-action-button">
                    <div class="row">
                      <div class="col-md-4 mt-2">
                    <h2 class="price">₹{{$del->price-$del->price*$del->discount/100}} <span class="price-sale">₹{{$del->price}}</span></h2>
                  </div>
                  <div class="col-md-8">
                    <div class="pull-right mt-2">
                      <a href="{{ url('localbuy?id') }}={{$del->deal_id}}" class="btn2">VIEW DEAL</a>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        @else
          <div class="row">
            <div class="col-md-12 mb-5">
              <div class="inner shadow bg-white">
                <div class="white-block p-3">
                  <div class="white-block-c">
                    <p class="nothing-found">Currently there are no local deals available.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endif 
          {{-- all deal section end --}}
        </div>
      </div>
    </div>
  </div>
</div>
@include('layouts.main.footer')
@endsection