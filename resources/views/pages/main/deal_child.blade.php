<input type="hidden" value="{{$i=0}}">
<div id="demo" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    @foreach($featured as $key=>$del)
    <div class="carousel-item @if($i==0) active @endif">
      <img src="{{url(asset($del->image))}}" class="corousel-image" width="100%" height="450">
      <div class="inner bg-white shadow mt-3">
        <div class="white-block">
          <div class="rating-sec">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="slider-left">
                  <ul class="list-unstyled list-inline top-meta">
                    <li>
                      <div class="item-ratings">
                        @if(1 || 2 || 3 || 4 || 5 == $del->star)
                        @for($i=1;$i<=$del->star;$i++)
                        <i class="fa fa-star" backup-class="fa fa-star"></i>
                        @endfor
                        @for($i=$del->star;$i<5;$i++)
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                        @endfor
                        @endif
                        <span class="rs mr-4"> ({{$del->rating}} ratings)</span>
                        @if(now()->diffInDays($del->validity)==0)
                        <span class="rs">Expires on:</span>
                        <span class="red-meta"> Today </span>
                        @else
                        <span class="rs">Expires in:</span>
                        <span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
                        @endif
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-xs-12 col-md-9 slider-left">
                  <div class="dealinfo pl-3 pt-0">
                    <a  href="javascript:void(0)">{{$del->name}}</a>
                  </div>
                  <ul class="list-unstyled list-inline bottom-meta ml-3">
                    <li>
                      <i class="fa fa-map-marker icon-margin"></i>
                      <a href="javascript:void(0)" class="mr-3">{{$del->location}}</a>
                    </li>
                    <li>
                      <p>{{$del->description}}</p>
                    </li>
                  </ul>
                </div>
              <div class="col-md-3 col-xs-12">
                <div class="slider-right ml-3 mb-4">
                  <div class="slider-action-button">
                    <h2 class="price">₹{{$del->price-$del->price*$del->discount/100}} <span class="price-sale">₹{{$del->price}}</span></h2>
                    <a href="{{ url('home/business?id') }}={{$del->deal_id}}" class="btn2">VIEW DEAL</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>