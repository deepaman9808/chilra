<h5 class="card-title">Your Info</h5>
<p class="card-text">
	Fill this info And after verified by admin.You are free to go add local deals and get other features of the site.
</p>
@if (count($errors->login) > 0)
@foreach ($errors->login->all() as $error)
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<strong>{{ $error }}</strong>
</div>
@endforeach
@endif
<div class="col-md-12">
	<form class="form" id="form" method="POST" enctype="multipart/form-data" role="form" action="{{ ('update') }}">
		{{ @csrf_field() }}
		<input type="hidden" name="user_id" value="{{Auth::user()->user_id}}">
		<div class="row">
			<div class="col-md-5">
				<div class="form-group">
					<label for="name">NAME</label>
					<input type="text" class="form-control" name="name"  id="name" value="{{Auth::user()->name}}">
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label for="email">EMAIL</label>
					<input type="email" class="form-control" disabled="" name="email"  id="email" value="{{Auth::user()->email}}">
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label for="gender">GENDER</label>
					<select class="form-control" name="gender"  id="gender">
						<option value="">Select</option>
						<option value="male">Male</option>
						<option value="female">Female</option>
						<option value="other">Other</option>
					</select>
				</div>
			</div>
			{{-- <div class="col-md-5">
				<div class="form-group">
					<label for="image">IMAGE <small>(Upload genuine image of yourself)</small></label>
					<input type="file" class="form-control" name="image" id="image">
				</div>
			</div> --}}
			<div class="col-md-5">
				<div class="form-group">
					<label for="dob">DATE OF BIRTH PROOF <small>(e.g. Scan or click picture of your driving license, student ID card as a proof that 18+ and a student )</small></label>
					<input class="form-control" id="dob" name="dob" type="file" autocomplete="off">
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label for="mobile">MOBILE</label>
					<input class="form-control" name="mobile" id="mobile" type="text">
				</div>
				<input type="button" class="verifyphone btn" value="Verify Phone" disabled="true">
				<span class="sendsuccess text-success  d-none p-2"><small>Otp Sent Successfully</small></span>
			</div>
			<div id="otpverify" class="col-md-5 d-none">
				<div class="form-group">
					<label for="otp">VERIFY OTP</label>
					<input class="form-control" id="otp" type="text">
				</div>
				<input type="button" class="verifyotp btn d-none" value="Verify Otp" disabled="true">
				<span class="verified text-success  d-none p-2">Verified</span>
				<span class="wrongotp text-warning  d-none p-2">Wrong otp</span>
				<span class="otpexpire text-danger  d-none p-2">Otp Expired</span>
				
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label for="dob">COLLEGE</label>
					<input class="form-control" name="college" type="text">
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label for="mobile">COURSE STUDYING</label>
					<input class="form-control" name="course" type="text">
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label for="dob">ADDRESS</label>
					<input class="form-control" name="address" type="text">
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label for="mobile">INTEREST</label>
					<input class="form-control" name="interest" type="text">
				</div>
			</div>
			<div class="col-md-10">
				<div class="form-group">
					<label for="mobile">ABOUT YOURSELF</label>
					<textarea class="form-control" rows='4' name="urself" type="text"></textarea>
				</div>
			</div>
		</div>
		<div class="col-md-12 mt-3 p-0">
			<button type="submit" id="applyform" class="btn login-btn">APPLY</button>
		</div>
	</form>
</div>