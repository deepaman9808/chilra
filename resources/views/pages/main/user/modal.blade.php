	<!-- edit Modal -->
	<div class="modal" id="editlocal">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Enter Deal Info</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body">
					<form class="form-horizontal m-t-30" method="POST" action="{{ url('local/dealupdate') }}" enctype="multipart/form-data">
						{{ @csrf_field() }}
						<input type="hidden" name="user_id" value="{{ Auth::guard('web')->user()->user_id }}">
						<input type="hidden" name="deal_id" id="deal_id">
						<div class="form-group">
							<label for="example-text">Deal Name</label>
							<input type="text"  name="name" id="name" class="form-control">
						</div>
						<div class="form-group">
							<label for="example-text">Discount(%)</label>
							<input type="text"  name="discount" id="discount" class="form-control">
						</div>
						<div class="form-group">
							<label>Category</label>
							<select class="custom-select col-12 text-capitalize" name="cat" id="inlineFormCustomSelect" >
								<option selected>Choose...</option>
								@foreach($cat as $ca)
								   <option  value="{{$ca->id}}">{{$ca->cat}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="example-text">Location</label>
							<input type="text"  name="loc" class="form-control" id="loc">
						</div>
						<div class="form-group">
							<label for="example-text">Validity(Max 30 days)</label>
							<input type="text"  name="validity" class="form-control" id="validity">
						</div>
						<div class="form-group">
							<label for="example-text">Deal price</label>
							<input type="text"  name="price" class="form-control" id="price">
						</div>
						<div class="form-group">
							<label for="example-text">Deal Description</label>
							<input type="text"  name="description" class="form-control" id="desc">
						</div>
						<div class="form-group">
							<label>Deal Image</label>
							<input type="file" class="form-control" name="image">
						</div>
						<div class="form-group">
							<button type="submit" class="btn waves-effect waves-light btn-success">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Add Modal -->
	<div class="modal" id="addlocmodal">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Enter Deal Info</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body">
					<form class="form-horizontal m-t-30" method="POST" action="{{ url('user/localdeal') }}" enctype="multipart/form-data">
						{{ @csrf_field() }}
						<input type="hidden" name="user_id" value="{{ Auth::guard('web')->user()->user_id }}">

						<div class="form-group">
							<label for="example-text">Deal Name</label>
							<input type="text"  name="name" class="form-control">
						</div>
						<div class="form-group">
							<label for="example-text">Discount(%)</label>
							<input type="text"  name="discount" class="form-control">
						</div>
						<div class="form-group">
							<label>Category</label>
							<select class="custom-select col-12 text-capitalize" name="cat" id="inlineFormCustomSelect" >
								<option selected>Choose...</option>
								@foreach($cat as $ca)
								<option value="{{$ca->id}}">{{$ca->cat}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="example-text">Location</label>
							<input type="text"  name="loc" class="form-control">
						</div>
						<div class="form-group">
							<label for="example-text">Validity(Max 30 days)</label>
							<input type="text"  name="validity" class="form-control">
						</div>
						<div class="form-group">
							<label for="example-text">Deal price</label>
							<input type="text"  name="price" class="form-control">
						</div>
						<div class="form-group">
							<label for="example-text">Deal Description</label>
							<input type="text"  name="description" class="form-control">
						</div>
						<div class="form-group">
							<label>Deal Image</label>
							<input type="file" class="form-control" name="image">
						</div>
						<div class="form-group">
							<button type="submit" class="btn waves-effect waves-light btn-success">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">DELETE</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
				</div>
				<div class="modal-body"> Do you want to delete this? </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
					<a href=""  id="deletes" class="btn btn-primary yes">YES</a> </div>
				</div>
			</div>
		</div>