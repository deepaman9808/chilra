<div class="row">
	<div class="col-md-12 col-sm-12 col-lg-12">
		<form role="form" method="POST" id="form" action="{{ url('settingup') }}" enctype="multipart/form-data">
			{{ @csrf_field() }}
			<div class="row">
				<div class="col-md-8">
					@if (count($errors->prof) > 0)
					@foreach ($errors->prof->all() as $error)
					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>{{ $error }}</strong>
					</div>
					@endforeach
					@endif
					@if ($message = Session::get('suckcess'))
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>{{ $message }}</strong>
					</div>
					@endif
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label for="name">NAME</label>
						<input type="text" class="form-control" name="name"  id="name" value="{{ auth::user()->name }}">
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label for="email">EMAIL</label>
						<input type="email" class="form-control" name="email"  id="email" value="{{ auth::user()->email }}">
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label for="password">PASSWORD</label>
						<input type="password" class="form-control" name="password"  id="password">
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
						<label for="image">REPEAT PASSWORD</label>
						<input type="password" class="form-control" name="password_confirmation"  id="cpassword">
					</div>
				</div>
			</div>
			<div class="text-muted text-italic"><small>Password Strength: <span id="password-strength-status" class="text-danger font-weight-700">weak</span></small>
		</div>
		<div class="col-md-12 mt-3 p-0">
			<button type="submit" id="applyform" class="btn login-btn">Update Profile</button>
		</div>
	</form>
</div>
</div>