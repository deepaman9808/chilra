<table class="table table-striped item-ratings table-responsive fixed-table-body">
	<thead>
		<tr>
			<th>Deal</th>
			<th>Category</th>
			<th>Views</th>
			<th>Ratings</th>
			<th>Validity Left</th>
			<th>Price</th>
			<th>Description</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach($deal as $dl)
		<tr>
			<td>
				@if($dl->validity>now())
				<a href="{{url('localbuy?id=')}}{{$dl->deal_id}}" class="text-success">
					@if (strlen($dl->name) <=30){{$dl->name}}
					@else{{substr($dl->name, 0, 30) . '...'}}
					@endif
					@else
					@if (strlen($dl->name) <=30){{$dl->name}}
					@else{{substr($dl->name, 0, 30) . '...'}}
					@endif
					@endif
				</a>
			</td>
			<td class="text-capitalize">{{$dl->cats}}</td>
			<td>@if($dl->view){{$dl->view}}@else 0 @endif</td>
			<td>@if(1 || 2 || 3 || 4 || 5 == $$dl->star)
				@for($i=1;$i<=$dl->star;$i++)
				<i class="fa fa-star" style="font-size: 13px;" backup-class="fa fa-star"></i>
				@endfor
				@for($i=$dl->star;$i<5;$i++)
				<i class="fa fa-star-o" aria-hidden="true"></i>
				@endfor
			@endif</td>
			<td>{{ now()->diffInDays($dl->validity) }} Days</td>
			<td>₹{{$dl->price}}</td>
			<td>
				@if (strlen($dl->description) <=25){{$dl->description}}
				@else{{substr($dl->description, 0, 25) . '...'}}
				@endif
			</td>
			<td>@if($dl->validity>now())
				<span class="text-success">Active</span>
				@else
				<span class="text-danger">Expired</span>
				@endif
			</td>
			<td>
				<i class="fa fa-pencil editlocal" style="cursor: pointer;" data-id='{{$dl->deal_id}}' data-name='{{$dl->name}}' data-discount='{{$dl->discount}}'data-desc='{{$dl->description}}' data-price='{{$dl->price}}' data-validity='{{ now()->diffInDays($dl->validity) }}' data-loc='{{$dl->loc}}' data-cat='{{$dl->cat}}' aria-hidden="true"></i>
				<i style='cursor: pointer;' data-id="{{ $dl->id }}" class="fa fa-trash deletedl" data-url="{{url('profile?deleteid=')}}{{$dl->deal_id}}" aria-hidden="true"></i>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>