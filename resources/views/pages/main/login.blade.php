@extends('layouts.main.master')<!-- main layout file -->
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
	{{-- breadcrumb --}}
	<section class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Login</h1>
				</div>
			</div>
		</div>
	</section>
	<div class="main-content login">
		<div class="header  py-7 py-lg-8"></div>
		<div class="container mt--8 pb-5">
			<div class="row justify-content-center">
				<div class="col-lg-12 col-md-12">
					<div class="card bg-white logcard shadow">
						<div class="card-body bg-transparent">
							<div class="white-block-title bored">
								<i class="fa fa-lock"></i>
								<h2>Login</h2>
								<div class="row">
									<div class="col-md-6 ml-4">
										@if (count($errors->danger) > 0)
										@foreach ($errors->danger->all() as $error)
										<div class="alert alert-warning">
											<button type="button" class="close" data-dismiss="alert">×</button>
											<strong>{{ $error }}</strong>
										</div>
										@endforeach
										@endif
										@if ($message = Session::get('success'))
										<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert">×</button>
											<strong>{{ $message }}</strong>
										</div>
										@endif
									</div>
								</div>
							</div>
							<div class="row mb-4 mt-4">
								<div class="col-md-8">
									<form class="form" id="form" method="POST" action="{{ url('log') }}">
										{{ @csrf_field() }}
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input type="email" class="form-control" name="email" placeholder="EMAIL" id="email">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input type="password" class="form-control" name="password" placeholder="PASSWORD" id="pwd" value="{{ old('password')}}">
												</div>
											</div>
											<div class="col-md-6">
												<div class="custom-control custom-checkbox mb-3">
													<input type="checkbox" class="custom-control-input" id="customCheck" name="remember">
													<label class="custom-control-label" for="customCheck">Remember me</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="text-right">
													<a href="{{ url('forgotuser') }}" class="my-4 text-danger">Forgot Password?</a>
												</div>
											</div>
											<div class="col-md-12 mt-3">
												<button type="submit" class="btn login-btn">LOG IN</button>
												<a href="{{ url('auth/redirect/facebook') }}">
													<button type="button" class="btn fb-btn"><i class="fa fa-facebook"></i> Facebook</button>
												</a>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-3 side">
									<h2> Not a member?</h2>
									<a href="{{url('/register')}}">
										<button type="button" class="btn reg-btn">REGISTER HERE</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('layouts.main.footer')
		@endsection