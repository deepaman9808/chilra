@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
  {{-- breadcrumb --}}
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>All Coupons</h1>
          <p>Chilra Coupons View Page</p>
        </div>
      </div>
    </div>
  </section>
  {{-- featured deals section --}}
  <div class="container sectiontwo sectionfour sectionfifth">
    <div class="row">
      <div class="col-md-3">
        <div class="inner bg-white shadow">
          <h4 class="looking">I'm looking for:</h4>
          <ul class="list-unstyled list-inline offer-type-filter">
            <li class="nav-item fistli">
              <a href="{{ url('home/alldeals') }}" class="dd">All</a>
              <a href="{{ url('home/deals') }}" class="dd">Deals</a>
              <a href="{{ url('home/coupons') }}" class="active">Coupons</a>
            </li>
          </ul>
          <ul class="list-unstyled">
            <li class="nav-item secli border border-light">
              <h4 class="looking">Categories</h4>
            </li>
            <li class="nav-item active secli border-0">
              <a href="{{ url('home/coupons') }}"><span style="position: unset;" class="catname">All Categories</span></a>
            </li>
            @foreach($count as $ct)
            <li class="nav-item @if(request()->cat== $ct->id ) active @endif secli border-0">
              <a href="{{ url('home/coupons?cat=') }}{{$ct->id}}"><span class="catname text-capitalize">{{$ct->cat}}</span></a>  <span class="count">{{$ct->cpo}}</span>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      @if(auth::guard('web')->check())
      <input type="hidden" id="user_id" value="{{auth::guard('web')->user()->user_id }}">
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
            </div>
            <div class="modal-body" id="modalbody">
              <div class="thank-you-pop">
                <img class="imageavatara" src="{{url(asset('images/brand/tick.png'))}}" alt="tickimage">
                <h1>Thank You!</h1>
                <p class="desc">You can use this coupon at checkout</p>
                <p class="d-none coupon"></p>
                <div class="couponend">
                  <span class="primi">Your Coupon Code: </span><br>
                  <div class="ego">
                    <span id="ccopy" class="text-success promo">₹111 </span>
                  </div>
                  <div class="copycode p-2">
                    <button id="cclip" class="btn btn-warning">CopyCode</button>
                  </div>
                </div>
                <p class="hide" style="display: none;color: green">Successfully Copied To Clipboard</p>
                <p>You only need coupon code to redeem this Coupon</p>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success" disabled="true" id="print">Print</button>
            </div>
          </div>
        </div>
      </div>
      @else
      <input type="hidden" id="user_id" value="">
      <div class="modal" id="myModals">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">PLEASE LOGIN</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <p>You have to login or your account must be verified before accessing that site feature </p>
              <span id="timer">
                <span id="time" class="orange">7</span> Seconds
              </span>
            </div>
            <div class="modal-footer">
              <a href="{{url('login')}}">
                <button type="button" class="btn btn-success">Visit Login Page</button>
              </a>
            </div>
          </div>
        </div>
      </div>
      @endif
      @if(!$merge->isEmpty())
      <div class="col-md-9">
        <div class="viewmode bg-white shadow">
          <div class="white-block offer-filter ">
            <div class="white-block-title2 pt-3">
              <div class="row">
                <div class="col-sm-3">
                  <ul class="list-unstyled list-inline">
                    <li class="view">
                      <a href="#" class="acti">Coupons</a>
                      <a href="#" class="active2"><i class="fa fa-th-large"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          @foreach($merge as $key=>$cp)
          <div class="col-md-6 mt-5 mb-5">
            <img src="{{ url(asset($cp->image)) }}" width="100%" height="225">
            <div class="inner bg-white shadow">
              <div class="log deal">
                <a href="javascript:void(0)" style="background-color: orangered;" @if(auth::guard('web')->check()) data-image="{{ url(asset($cp->image)) }}" data-name="{{$cp->name}}" data-code="{{$cp->code}}" data-id='{{$cp->coupon_id}}' data-desc="{{$cp->description}}" data-discount="{{ $cp->discount }}" data-ex="{{ $cp->validity}}" class="prnt"  id="code" @else class="prnt"  id="codes" @endif  >VIEW CODE</a>
              </div>
              <div class="white-block-content">
                <ul class="list-unstyled list-inline top-meta">
                  <li>
                    <div class="item-ratings">
                      <span class="rs">Discount:</span>
                      <span class="red-meta">{{ $cp->discount }} %</span>
                      <div class="pull-right">
                        <span class="rs">Expires in:</span>
                        <span class="red-meta">{{ now()->diffInDays($cp->validity)}} days</span>
                      </div>
                    </div>
                  </li>
                </ul>
                <h6><a href="#" class="text-body"> {{$cp->name}}</a></h6>
                <ul class="list-unstyled list-inline bottom-meta">
                  <li>
                    <i class="fa fa-map-marker icon-margin"></i>
                    <a href="#" class="mr-3">{{$cp->location}}</a>
                  </li>
                  <li>
                    <span class="rs" style="font-size: 14px;">{{$cp->description}}</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        @endforeach()
      </div>
    </div>
    @else
    <div class="col-md-9">
      <div class="row">
        <div class="col-md-12">
          <div class="inner shadow bg-white">
            <div class="white-block p-3">
              <div class="white-block-c">
                <p class="nothing-found">Currently there are no coupons available for this category.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      {{-- all deal section end --}}
    </div>
  </div>
</div>
</div>
</div>
@include('layouts.main.footer')
@endsection