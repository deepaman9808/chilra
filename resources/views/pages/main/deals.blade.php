@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
  {{-- breadcrumb --}}
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>All Deals</h1>
          <p>Chilra All Deals View Page</p>
        </div>
      </div>
    </div>
  </section>
  {{-- featured deals section --}}
  <div class="container sectiontwo sectionfour sectionfifth">
    <div class="row">
      <div class="col-md-3">
        <div class="inner bg-white shadow">
          <h4 class="looking">I'm looking for:</h4>
          <ul class="list-unstyled list-inline offer-type-filter">
            <li class="nav-item fistli">
              <a href="{{ url('home/alldeals') }}" class="dd">All</a>
              <a href="{{ url('home/deals') }}" class=" active">Deals</a>
              <a href="{{ url('home/coupons') }}" class="dd">Coupons</a>
            </li>
          </ul>
          <ul class="list-unstyled">
            <li class="nav-item secli border border-light">
              <h4 class="looking">Categories</h4>
            </li>
            <li class="nav-item active secli border-0">
              <a href="{{ url('home/deals') }}"><span style="position: unset;" class="catname">All Categories</span></a>
            </li>
            @foreach($cats as $ct)
            <li class="nav-item @if(request()->cat== $ct->id ) active @endif secli border-0">
              <a href="{{ url('home/deals?cat=') }}{{$ct->id}}"><span class="catname text-capitalize">{{$ct->cat}}</span></a>   <span class="count">{{$ct->count}}</span>
            </li>
            @endforeach
          </ul>
          <ul class="list-unstyled ">
            <li class="nav-item secli border border-light">
              <h4 class="looking">Local Deals</h4>
            </li>
            <li class="nav-item secli border-0">
               <a href="{{url('/local')}}"> <span style="position: unset;" class="catname">All Local deals</span></a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        @include('pages.main.deal_child')
        @if(!$merge->isEmpty())
        <div class="viewmode bg-white  mt-5 shadow">
          <div class="white-block offer-filter ">
            <div class="white-block-title2 pt-3">
              <div class="row">
                <div class="col-sm-3">
                  <ul class="list-unstyled list-inline">
                    <li class="view">
                      <a href="javascript:void(0)" class="acti">Deals</a>
                      <a href="javascript:void(0)" class="active2"><i class="fa fa-th-large"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          @foreach($merge as $key=>$del)
          <div class="col-md-6 mt-5 mb-5">
            <div class="image shadow">
              <img src="{{ url(asset($del->image)) }}" width="100%" height="225">
            </div>
            <div class="inner bg-white shadow">
              <div class="deal">
                {{-- <a href="#" class="share"><i class="fa fa-share-alt"></i></a> --}}
                <a href="{{ url('home/business?id') }}={{$del->deal_id}}" class="prnt" style="top:47%;">VIEW DEAL</a>
              </div>
              <div class="white-block-content">
                <ul class="list-unstyled list-inline top-meta">
                  <li>
                    <div class="item-ratings">
                      @if(1 || 2 || 3 || 4 || 5 == $del->star)
                      @for($i=1;$i<=$del->star;$i++)
                      <i class="fa fa-star" backup-class="fa fa-star"></i>
                      @endfor
                      @for($i=$del->star;$i<5;$i++)
                      <i class="fa fa-star-o" aria-hidden="true"></i>
                      @endfor
                      @endif
                      <span class="rs"> ({{$del->rating}} rates)</span>
                      <div class="pull-right">
                        @if(now()->diffInDays($del->validity)==0)
                          <span class="rs">Expires on:</span>
                          <span class="red-meta"> Today </span>
                        @else
                          <span class="rs">Expires in:</span>
                          <span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
                        @endif
                      </div>
                    </div>
                  </li>
                </ul>
                <h6><a href="#" class="text-body">{{$del->name}}</a></h6>
                <ul class="list-unstyled list-inline bottom-meta">
                  <li>
                    <i class="fa fa-map-marker"></i><a href="#" class="mr-3">{{$del->location}}</a>
                  </li>
                </ul>
                <div class="white-block-footer border border-light">
                  <div class="white-block-content px-0 py-0">
                    <h2 class="price">₹{{$del->price-$del->price*$del->discount/100}} <span class="price-sale">₹{{$del->price}}</span></h2>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach()
        </div>
      </div>
      @else
      <div class="row">
        <div class="col-md-12 mt-3 mb-5">
          <div class="inner shadow bg-white">
            <div class="white-block p-3">
              <div class="white-block-c">
                <p class="nothing-found">Currently there are no deals available for this category.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      {{-- all deal section end --}}
    </div>
  </div>
</div>
</div>
</div>
@include('layouts.main.footer')
@endsection