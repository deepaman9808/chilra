@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area">
	{{-- breadcrumb --}}
	{{-- <section class="page-title mb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Blog</h1>
					<p>{{$data->name}}</p>
				</div>
			</div>
		</div>
	</section> --}}
	<section class="blogpage">
		<div class="card-section overlay" style="background-image: url(../public/{{$data->image}});">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="white-block-contents text-center">
							<span class="cats mb-3 {{$data->color}}">{{$data->category}}</span>
							<h2>{{$data->name}}</h2>
						</div>
						<ul class="list-unstyled list-inline  bottom-meta mb-0 text-center">
							<li>
								<img src="{{url(asset($data->author_image))}}" class="img-fluid rounded mb-3">
							</li>
							<li class="mt-1 text-dim ">
								<span class="by">By </span><span>{{$data->author_name}}</span><span class="date ml-2">  -  {{$data->created_at->diffForHumans()}}</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="container lastone">
			<div class="row mt-5">
				<div class="col-md-8 pr-3 pl-3">
					{!!$data->content!!}
				</div>
				<div class="col-md-4 pr-4 pl-4">
					<div class="sidebar" >
						<div class="recent" >
							<h5 class="posts">Recent Posts</h5>
							@foreach($rec as $post)
							<ul class="list-unstyled list-inline d-flex mt-3  mb-5 text-center">
								<li>
									<img src="{{url(asset($post->image))}}" class="img-fluid mb-3">
								</li>
								<li>
									<div class="text pr-3 pl-3">
										<span>{{$post->name}}</span>
									</div>
									<div class="text-dim">
										<span> {{$post->created_at->format('M d, Y')}}</span>
									</div>
								</li>
							</ul>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	@include('layouts.main.footer')
	@endsection