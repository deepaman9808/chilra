@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
	{{-- breadcrumb --}}
	<section class="page-title">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Store</h1>
					<p>Chilra Store View &amp; Buy Page</p>
				</div>
			</div>
		</div>
	</section>
	<section class="storebanner sectiontwo sectionfour sectionfifth">
		<div class="card-section">
			<div class="container">
				<div class="card-block mt-5">
					<div class="row">
						<div class="col-xl-8 col-md-8 col-sm-8">
							<div class="inner shadow bg-white p-4">
								<div class="section-title">
									<h2>{{$store->name}}</h2>
									<div class="storedetails">
										<ul class="list-inline bottom-meta">
											<li>
												<i class="fa fa-phone" aria-hidden="true"></i><span class="phone"> Phone No : {{$store->phone}}</span>
											</li>
											<li>
												<i class="fa fa-map-marker" aria-hidden="true"></i><span class="direction"> Location : {{$store->address}}</span>
											</li>
											<li>
												<i class="fa fa-cogs" aria-hidden="true"></i><span class="category text-capitalize"> Category : {{$cat}}</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 ">
							<img src="{{url(asset($store->store))}}" width="100%" height="214px"  class="storeimage">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 mt-5 pl-3">
							<div class="inner shadow bg-white">
								<div class="desc p-4">
									<p> {{$store->descr}}</p>
								</div>
								<div class="slider-right">
									<ul class="social list-unstyled list-inline d-flex">
										<li>
											<a href="//{{$store->fb}}" class="share">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="//{{$store->tw}}" class="share">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="//{{$store->gp}}" class="share">
												<i class="fa fa-google-plus"></i>
											</a>
										</li>
										<li>
											<a href="mailto:{{$store->email}}" class="share mail-share" >
												<i class="fa fa-envelope-o"></i>
											</a>
										</li>
									</ul>
								</div>
								<div class="category p-0">
									<a href="{{ url('home/store')}}?id={{request()->id}}" class="cats @if(request()->view=='viewdeal' || request()->view=='viewcoupon') @else active  @endif">All ({{$dl+$cp}})</a>
									<a href="{{ url('home/store') }}?id={{request()->id}}&view=viewdeal" class="cats @if(request()->view=='viewdeal') active @endif">Deals ({{$dl}})</a>
									<a href="{{ url('home/store') }}?id={{request()->id}}&view=viewcoupon" class="cats @if(request()->view=='viewcoupon') active @endif">Coupons ({{$cp}})</a>
								</div>
							</div>
						</div>
						@if(auth::guard('web')->check())
						<input type="hidden" id="user_id" value="{{auth::guard('web')->user()->id }}">
						<div class="modal fade" id="myModal" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
									</div>
									<div class="modal-body" id="modalbody">
										<div class="thank-you-pop">
											<img class="imageavatara" src="{{url(asset('images/brand/tick.png'))}}" alt="tickimage">
											<h1>Thank You!</h1>
											<p class="desc">You can use this coupon at checkout</p>
											<p class="d-none coupon"></p>
											<div class="couponend">
												<span class="primi">Your Coupon Code: </span><br>
												<div class="ego">
													<span id="ccopy" class="text-success promo">₹111 </span>
												</div>
												<div class="copycode p-2">
													<button id="cclip" class="btn btn-warning">CopyCode</button>
												</div>
											</div>
											<p class="hide" style="display: none;color: green">Successfully Copied To Clipboard</p>
											<p>You only need coupon code to redeem this Coupon</p>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-success" disabled="true" id="print">Print</button>
									</div>
								</div>
							</div>
						</div>
						@else
						<input type="hidden" id="user_id" value="">
						<div class="modal" id="myModals">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">PLEASE LOGIN</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>
									<div class="modal-body">
										<p>You have to login or your account must be verified before accessing that site feature </p>
										<span id="timer">
											<span id="time" class="orange">7</span> Seconds
										</span>
									</div>
									<div class="modal-footer">
										<a href="{{url('login')}}">
											<button type="button" class="btn btn-success">Visit Login Page</button>
										</a>
									</div>
								</div>
							</div>
						</div>
						@endif
						@if($merge)
						<div class="col-md-9">
							<div class="viewmode bg-white mt-5 shadow">
								<div class="white-block offer-filter ">
									<div class="white-block-title2 pt-3">
										<div class="row">
											<div class="col-sm-3">
												<ul class="list-unstyled list-inline">
													<li class="view">
														<a href="javascript:void(0)" class="acti">VIEW</a>
														<a href="javascript:void(0)" class="active2"><i class="fa fa-bars"></i></a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								@foreach($merge as $key=>$del)
								@if(request()->view=='viewcoupon')
								@if($del['coupon_id']!='')
								<div class="col-md-6 mt-5 mb-4">
									<img src="{{url(asset($del->image))}}" width="100%" height="225">
									<div class="inner bg-white shadow">
										<div class="log deal">
											<a href="javascript:void(0)" @if(auth::guard('web')->check()) data-image="{{ url(asset($del->image)) }}" data-name="{{$del->name}}" data-code="{{$del->code}}" data-id='{{$del->coupon_id}}' data-desc="{{$del->description}}" data-discount="{{ $del->discount }}" data-ex="{{ $del->validity}}" class="prnt" style="top:47%;background-color: orangered;" id="code" @else class="prnt" style="top:47%;background-color: orangered;" id="codes" @endif  >VIEW CODE</a>
										</div>
										<div class="white-block-content">
											<ul class="list-unstyled list-inline top-meta">
												<li>
													<div class="item-ratings">
														<span class="rs">Discount:</span>
														<span class="red-meta">{{ $del->discount }} %</span>
														<div class="pull-right">
															<span class="rs">Expires in:</span>
															<span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
														</div>
													</div>
												</li>
											</ul>
											<h6><a href="javascript:void(0)" class="text-body"> {{$del->name}}</a></h6>
											<ul class="list-unstyled list-inline bottom-meta">
												<li>
													<i class="fa fa-map-marker icon-margin"></i>
													<a href="javascript:void(0)" class="mr-3">{{$del->address}}</a>
												</li>
												<li>
													<span class="rs" style="font-size: 14px;">{{$del->description}}</span>
												</li>
											</ul>
										</div>
									</div>
								</div>
								@endif
								@elseif(request()->view=='viewdeal')
								@if($del['coupon_id']!='')
								@else
								<div class="col-md-6 mt-5 mb-4">
									<div class="image shadow">
										<img src="{{url(asset($del->image))}}" width="100%" height="225">
									</div>
									<div class="inner bg-white shadow">
										<div class="deal">
											{{-- <a href="#" class="share"><i class="fa fa-share-alt"></i></a> --}}
											<a href="{{ url('home/business?id') }}={{$del->deal_id}}" class="prnt" style="top:45%;">VIEW DEAL</a>
										</div>
										<div class="white-block-content">
											<ul class="list-unstyled list-inline top-meta">
												<li>
													<div class="item-ratings">
														@if(1 || 2 || 3 || 4 || 5 == $del->star)
														@for($i=1;$i<=$del->star;$i++)
														<i class="fa fa-star" backup-class="fa fa-star"></i>
														@endfor
														@for($i=$del->star;$i<5;$i++)
														<i class="fa fa-star-o" aria-hidden="true"></i>
														@endfor
														@endif
														<span class="rs"> {{$del->rating}} ratings</span>
														<div class="pull-right">
															<span class="rs">Expires in:</span>
															<span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
														</div>
													</div>
												</li>
											</ul>
											<h6><a href="javascript:void(0)" class="text-body">{{$del->name}}</a></h6>
											<ul class="list-unstyled list-inline bottom-meta">
												<li>
													<i class="fa fa-map-marker"></i><a href="javascript:void(0)" class="mr-3">{{$del->address}}</a>
												</li>
											</ul>
											<div class="white-block-footer border border-light">
												<div class="white-block-content px-0 py-0">
													<h2 class="price">₹{{$del->price-$del->price*$del->discount/100}} <span class="price-sale">₹{{$del->price}}</span></h2>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endif
								@else
								@if($del['coupon_id']!='')
								<div class="col-md-6 mt-5 mb-4">
									<img src="{{ url(asset($del->image)) }}" width="100%" height="225">
									<div class="inner bg-white shadow">
										<div class="log deal">
											<a href="javascript:void(0)" style="background-color: orangered;" @if(auth::guard('web')->check()) data-image="{{ url(asset($del->image)) }}" data-name="{{$del->name}}" data-code="{{$del->code}}" data-id='{{$del->coupon_id}}' data-desc="{{$del->description}}" data-discount="{{ $del->discount }}" data-ex="{{ $del->validity}}" class="prnt"  id="code" @else class="prnt"  id="codes" @endif  >VIEW CODE</a>
										</div>
										<div class="white-block-content">
											<ul class="list-unstyled list-inline top-meta">
												<li>
													<div class="item-ratings">
														<span class="rs">Discount:</span>
														<span class="red-meta">{{ $del->discount }} %</span>
														<div class="pull-right">
															<span class="rs">Expires in:</span>
															<span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
														</div>
													</div>
												</li>
											</ul>
											<h6><a href="javascript:void(0)" class="text-body"> {{$del->name}}</a></h6>
											<ul class="list-unstyled list-inline bottom-meta">
												<li>
													<i class="fa fa-map-marker icon-margin"></i>
													<a href="javascript:void(0)" class="mr-3">{{$del->address}}</a>
												</li>
												<li>
													<span class="rs" style="font-size: 14px;">{{$del->description}}</span>
												</li>
											</ul>
										</div>
									</div>
								</div>
								@else
								<div class="col-md-6 mt-5  mb-4">
									<div class="image shadow">
										<img src="{{url(asset($del->image))}}" width="100%" height="225">
									</div>
									<div class="inner bg-white shadow">
										<div class="deal">
											{{-- <a href="#" class="share"><i class="fa fa-share-alt"></i></a> --}}
											<a href="{{ url('home/business?id') }}={{$del->deal_id}}" class="prnt" style="top:45%;">VIEW DEAL</a>
										</div>
										<div class="white-block-content">
											<ul class="list-unstyled list-inline top-meta">
												<li>
													<div class="item-ratings">
														@if(1 || 2 || 3 || 4 || 5 == $del->star)
														@for($i=1;$i<=$del->star;$i++)
														<i class="fa fa-star" backup-class="fa fa-star"></i>
														@endfor
														@for($i=$del->star;$i<5;$i++)
														<i class="fa fa-star-o" aria-hidden="true"></i>
														@endfor
														@endif
														<span class="rs"> {{$del->rating}} ratings</span>
														<div class="pull-right">
															<span class="rs">Expires in:</span>
															<span class="red-meta">{{ now()->diffInDays($del->validity)}} days</span>
														</div>
													</div>
												</li>
											</ul>
											<h6><a href="javascript:void(0)" class="text-body">{{$del->name}}</a></h6>
											<ul class="list-unstyled list-inline bottom-meta">
												<li>
													<i class="fa fa-map-marker"></i><a href="javascript:void(0)" class="mr-3">{{$del->address}}</a>
												</li>
											</ul>
											<div class="white-block-footer border border-light">
												<div class="white-block-content px-0 py-0">
													<h2 class="price">₹{{$del->price-$del->price*$del->discount/100}} <span class="price-sale">₹{{$del->price}}</span></h2>
												</div>
											</div>
										</div>
									</div>
								</div>
								@endif
								@endif
								@endforeach()
							</div>
						</div>
						@else
						<div class="col-md-9 mt-3">
							<div class="inner shadow bg-white">
								<div class="white-block p-3">
									<div class="white-block-c">
										<p class="nothing-found">Currently there is no coupons and deals for this store.</p>
									</div>
								</div>
							</div>
						</div>
						@endif
						{{-- all deal section end --}}
					</div>
				</div>
			</div>
		</section>
	</div>
	@include('layouts.main.footer')
	@endsection