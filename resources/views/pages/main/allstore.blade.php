@extends('layouts.main.master')
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
  {{-- breadcrumb --}}
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>All Stores</h1>
          <p>Chilra All Stores View Page</p>
        </div>
      </div>
    </div>
  </section>
  {{-- featured deals section --}}
  <div class="container sectiontwo sectionfour sectionfifth">
    <div class="row">
      <div class="col-md-3">
        <div class="inner bg-white shadow">
          <ul class="list-unstyled">
            <li class="nav-item secli border border-light">
              <h4 class="looking">Categories</h4>
            </li>
            <li class="nav-item active secli border-0">
              <a href="{{ url('home/allstore') }}"><span style="position: unset;" class="catname">All Categories</span></a>
            </li>
            @foreach($cat as $ct)
            <li class="nav-item @if(request()->cat== $ct->id ) active @endif secli border-0">
              <a href="{{ url('home/allstore?cat=') }}{{$ct->id}}"><span class="catname text-capitalize">{{$ct->cat}}</span></a>    <span class="count">{{$ct->count}} </span>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        @if(!$store->isEmpty())
         <div class="viewmode bg-white shadow">
          <div class="white-block offer-filter ">
            <div class="white-block-title2 pt-3">
              <div class="row">
                <div class="col-sm-3">
                  <ul class="list-unstyled list-inline">
                    <li class="view">
                      <a href="javascript:void(0)" class="acti">Store</a>
                      <a href="javascript:void(0)" class="active2"><i class="fa fa-th-large"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
       <div class="row mt-5 mb-5">
      @foreach($store as $key=>$st)
      <div class="col-md-5">
        <div class="inner bg-white shadow">
          <div class="white-block deal">
            <div class="white-block-media">
              <img src="{{ url(asset($st->store))}}" width="100%" height="225">
            </div>
            <a href="{{url('home/store?id=')}}{{$st->merchant_id}}" class="prnt" style="top:192px;">View Store</a>
            <div class="white-block-content">
              <ul class="list-unstyled list-inline top-meta">
                <li>
                  <div class="item-ratings">
                    <span class="text-capitalize rs">{{$st->cat}}</span>
                    <div class="pull-right">
                      <span class="rs">Joined On:</span>
                      <span class="red-meta">{{$st->created_at->format('d-M-Y')}}</span>
                    </div>
                  </div>
                </li>
              </ul>
              <h6><a  class="text-body">{{$st->name}}</a></h6>
              <ul class="list-unstyled list-inline bottom-meta">
                <li>
                  <i class="fa fa-map-marker icon-margin"></i>
                  <a  class="mr-3">{{$st->address}}</a>
                </li>
              </ul>
             <ul class="list-unstyled list-inline bottom-meta">
                <li>
                  <span class="rs" style="font-size: 14px;">{{$st->descr}}</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
      </div>
      @else
      <div class="row">
        <div class="col-md-12 mb-5">
          <div class="inner shadow bg-white">
            <div class="white-block p-3">
              <div class="white-block-c">
                <p class="nothing-found">Currently there is no store or this category has no results.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      {{-- all deal section end --}}
    </div>
  </div>
</div>
</div>
</div>
@include('layouts.main.footer')
@endsection