@extends('layouts.main.master')<!-- main layout file -->
@section('content')
@include('layouts.main.nav')
<div class="home-area back">
	{{--  ============================================== section banners ================================================= --}}
	<div class="banner sectionone" style="background-image:url({{url(asset(''))}}/{{$site->banner}})">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-white text-center">{{$site->text}}</h1>
					<p class="text-white text-center">The most complete solution for deals & coupons online business.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					@if(Auth::guard('web')->check())
					<p class="text-white text-center">HI, {{Auth::guard('web')->user()->name}}</p>
					@else
					<div class="text-center">
						<a href="{{route('login')}}">
							<button type="button" class="btn btn-success text-capitalize mt-3 font-weight-bold px-3"><i class="fa fa-user pr-4" aria-hidden="true"></i>{{$site->btn_one}}</button>
						</a>
						<a href="{{route('merchant/login')}}">
							<button type="button" class="btn btn-success mt-3 text-capitalize font-weight-bold px-3"><i class="fa fa-user pr-4" aria-hidden="true"></i>{{$site->btn_two}}</button>
						</a>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	{{--  ============================================== section featured deals ================================================= --}}
	<div class="container sectiontwo">
		<div class="row mt-5">
			<div class="col-md-3 mb-5">
				<div class="inner bg-white shadow">
					<h4 class="looking">I'm looking for:</h4>
					<ul class="list-unstyled list-inline offer-type-filter">
						<li class="nav-item fistli">
							<a href="{{url('home/alldeals')}}" class="active">All</a>
							<a href="{{url('home/deals')}}" class="dd">Deals</a>
							<a href="{{url('home/coupons')}}" class="dd">Coupons</a>
						</li>
					</ul>
					<ul class="list-unstyled">
						@foreach($cat as $ct)
						<li class="nav-item secli">
							<a href="{{ url('home/alldeals?cat=') }}{{$ct->id}}"><i class="{{$ct->icon}}"></i><span class="catname text-capitalize">{{$ct->cat}}</span></a>
						</li>
						@endforeach
					</ul>
				</div>
			</div>
			<div class="col-md-9">
				@include('pages.main.deal_child')
			</div>
		</div>
	</div>
	{{--  ============================================== section deal hunters ================================================= --}}
	<div class="container">
		<div class="row mt-3">
			<div class="col-md-12 col-sm-12">
				<div class="section-divider bg-white shadow">
					<div class="white-block-title">
						<i class="fa fa-user" area-hidden="true"></i>
						<h2>Top Deal Hunters</h2>
						<a href="{{url('home/dealhunters')}}"><span class="text-body"> ALL DEAL HUNTERS</span>
						<i class="fa fa-arrow-circle-o-right text-center"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container sectionthree">
	<div class="row mt-5">
		@foreach($user as $key=> $us)
		<div class="col-md-4 mb-5">
			<div class="inner bg-white shadow">
				<img src="{{url(asset($us->image))}}" class="rounded" width="100%" height="242">
				<div class="white-block-content">
					<ul class="list-unstyled list-inline item-ratings">
						<li>
							<span class="orange"> Verified <i class="fa fa-check text-success"></i></span>
							<div class="pull-right"><span class="rs">Followers:</span> <span class="red-meta">@if($us->follower){{$us->follower}}@else 0 @endif</span></div>
						</li>
					</ul>
					<h6 class="text-body">{{$us->name}}</h6>
					<ul class="list-unstyled list-inline bottom-meta">
						<li>
							<i class="fa fa-map-marker"></i><span class="mr-3">{{$us->address}}</span>
						</li>
					</ul>
					<hr>
					<i class="fa fa-user rr" aria-hidden="true"></i><a href="{{url('userprofile?id=')}}{{$us->user_id}}" class="btn3">Visit Profile</a>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
{{-- =================================================section deals ================================================== --}}
<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="section-divider bg-white shadow">
				<div class="white-block-title">
					<i class="fa fa-star"></i><h2>Top Rated Deals</h2>
					<a href="{{url('/home/deals')}}"><span class="text-body"> ALL DEALS</span><i class="fa fa-arrow-circle-o-right text-center"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container sectionfour">
	<div class="row mt-5">
		@foreach($topthree as $key=>$del)
		<div class="col-md-4 mb-5">
			<div class="inner bg-white shadow">
				<img src="{{asset($del->image)}}" class="rounded" width="100%" height="242">
				<div class="deal">
					<a href="{{url('home/business?id=')}}{{$del->deal_id}}" class="prnt" style="top: 47%">View Deal</a>
				</div>
				<div class="white-block-content">
					<ul class="list-unstyled list-inline d-flex top-meta">
						<li>
							<div class="item-ratings">
								@if(1 || 2 || 3 || 4 || 5 == $del->star)
								@for($i=1;$i<=$del->star;$i++)
								<i class="fa fa-star" backup-class="fa fa-star"></i>
								@endfor
								@for($i=$del->star;$i<5;$i++)
								<i class="fa fa-star-o" aria-hidden="true"></i>
								@endfor
								@endif
								<span class="rs mr-4"> ({{$del->rating}} ratings)</span>
								
							</div>
						</li>
						<li>
							<div class="pull-right item-ratings">
								@if(now()->diffInDays($del->validity)==0)
									<span class="rs">Expires on:</span>
									<span class="red-meta"> Today </span>
								@else
									<span class="rs">Expires in:</span>
									<span class="red-meta">{{ now()->diffInDays($del->validity)}} days </span>
								@endif
							</div>
						</li>
					</ul>
					<h6><span class="text-body">{{$del->name}}</span></h6>
					<ul class="list-unstyled list-inline bottom-meta">
						<li>
							<i class="fa fa-map-marker icon-margin"></i>
							<span class="mr-3">{{$del->location}}</span>
						</li>
					</ul>
					<div class="white-block-footer" style="border-top: 1px solid #f4f4f4;">
						<div class="white-block-content mt-2 px-0 py-0">
							<h2 class="price">₹{{$del->price-$del->price*$del->discount/100}} <span class="price-sale">₹{{$del->price}}</span></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
{{-- ================================================= section Coupons ================================================== --}}
<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="section-divider bg-white shadow">
				<div class="white-block-title">
					<i class="fa fa-star text-center"></i><h2>Top Used Coupons</h2>
					<a href="{{url('/home/coupons')}}"><span class="text-body"> ALL COUPONS</span><i class="fa fa-arrow-circle-o-right text-center"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container sectionfifth sectionfour">
	<div class="row mt-5">
		@foreach($topcp as $key=>$cp)
		<div class="col-md-4 mb-5">
			<div class="inner bg-white shadow">
				<img src="{{ url(asset($cp->image)) }}" class="rounded" width="100%" height="242">
				<div class="deal coupon">
					<a href="{{url('home/coupons')}}" class="prnt" style="background-color: orangered;top: 49%;">View Coupon</a>
				</div>
				<div class="white-block-content">
					<ul class="list-unstyled list-inline top-meta">
						<li>
							<div class="item-ratings">
								<span class="rs">Discount:</span>
								<span class="red-meta">{{ $cp->discount }} %</span>
								<div class="pull-right">
									<span class="rs">Expires in:</span>
									<span class="red-meta">{{ now()->diffInDays($cp->validity)}} days</span>
								</div>
							</div>
						</li>
					</ul>
					<h6>
					<span class="text-body">{{$cp->name}}</span>
					</h6>
					<ul class="list-unstyled list-inline bottom-meta">
						<li>
							<i class="fa fa-map-marker"></i>
							<span class="mr-3">{{$cp->location}}</span>
						</li>
						<li>
							<p>{{$cp->description}}</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
		@endforeach()
	</div>
</div>
{{-- ================================================= section store ================================================== --}}
<div class="container">
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="section-divider bg-white shadow">
				<div class="white-block-title">
					<i class="fa fa-rss"></i><h2>Recent Blogs</h2>
					<a href="{{url('/home/blog')}}" ><span class="text-body"> ALL Blogs</span><i class="fa fa-arrow-circle-o-right text-center"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container blog  sectionfour">
	<div class="row mt-5">
		@foreach($blog as $key=>$data)
		<div class="col-md-4 mb-5">
			<div class="inner bg-white shadow">
				<div class="thumbnail">
					<img src="{{url(asset($data->image))}}" class="img-fluid rounded mb-4">
				</div>
				<div class="white-block-contents ml-3 mr-3">
					<span class="cats mb-3 {{$data->color}}">{{$data->category}}</span>
					<a href="{{url('home/blogpage?id=')}}{{$data->blog_id}}"><h2>{{$data->name}}</h2></a>
					<ul class="list-unstyled list-inline d-flex bottom-meta mb-0">
						<li>
							<img src="{{url(asset($data->author_image))}}" class="img-fluid rounded mb-4">
						</li>
						<li class="mt-1 text-dim ">
							<span class="by">By </span><span class="text-dark">{{$data->author_name}}</span><span class="date ml-2">  -  {{$data->created_at->diffForHumans()}}</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
{{-- ================================================= section CONTACT ================================================== --}}
<div class="container">
	<div class="row mb-3">
		<div class="col-md-12">
			<div class="section-divider bg-white mt-5 mb-3 shadow">
				<div class="white-block-title p-3 text-center">
					<h2>Join India's #1 online platform for local businesses </h2>
					<a style="float: none;" href="{{url('/contactus')}}">
						<div class="text-body" style="color: orangered !important;">
							<i class="fa fa-envelope p-0" style="font-size: 18px;"></i> CONTACT US
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@include('layouts.main.footer')
@endsection