<?php

namespace App\Http\Controllers;

use Str;
use Hash;
use View;
use Session;
use App\Otp;
use App\Plan;
use App\Deal;
use App\Promotion;
use App\Code;
use App\Coupon;
use Carbon\Carbon;
use App\Review;
use App\Order;
use App\Feature;
use App\Setting;
use App\Category;
use App\Merchant;
use App\Payment;
use App\Membership;
use App\Comment;
use App\Coupon_report;
use App\Deal_report;
use App\Mail\SendMailvendor;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator, Redirect, Response,File;

class MerchantController extends Controller
{
    /*public function __construct() {
        $site = Setting::first();
        View::share(['title' => $site->title, 'fab' => $site->fab, 'logo' => $site->logo]);
    }*/

    public function merchantregister() {
        $cat = category::All();
        return view('pages.merchant.wprofile')->with('cat',$cat);
    }

    public function merchant() {
        if(Auth::guard('vendor')->check()) {

           $order=order::leftjoin('deals', 'orders.deal_id', '=', 'deals.deal_id')->where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)->orderBy('orders.id','desc')->take(5)->get();

           $earning=order::where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)->where('inv_status',1)->sum('amount');

           $invoice=order::where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)->where('inv_status',0)->sum('amount');

           $membership=Membership::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->first();

           $feature= Membership::leftjoin('features', 'features.'.$membership->plan, '=', $membership->plan)->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->first();

           for($i=1;$i<=date('t');$i++) {
           	$user=Deal::leftjoin('orders', 'deals.deal_id', '=', 'orders.deal_id')->where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)->whereDay('orders.created_at','=', $i)->whereMonth('orders.created_at','=', now()->month)->sum('amount');

           	if(!$user) {
           		$pay[]=array($i,0);
           	} else {
           		$pay[]=array($i,$user);
           	}}
           	$view=deal::select(DB::raw('count(promotion.deal_id) As view'))
                        ->where('deals.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                        ->leftjoin('promotion','deals.deal_id','=','promotion.deal_id')
                        ->whereMonth('promotion.created_at',now()->month)
                        ->groupBy('deals.id')
                        ->get();
                  /*  echo "<pre>";print_r($view);exit;*/
            $sum = 0;
            foreach($view as $fg => $df) 
            {
                $sum+=$df->view;
            }

            $comments=comment::select('deals.name','comments.comment','comments.created_at','users.image')
                            ->leftjoin('deals', 'comments.deal_id', '=', 'deals.deal_id')
                            ->leftjoin('users', 'comments.user_id', '=', 'users.user_id')
                            ->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)
                            ->orderBy('comments.id','desc')
                            ->take(5)
                            ->get();

            $rate=review::select('deals.name','reviews.review','reviews.created_at','users.image')
                            ->leftjoin('deals', 'reviews.deal_id', '=', 'deals.deal_id')
                            ->leftjoin('users', 'reviews.user_id', '=', 'users.user_id')
                            ->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)
                            ->orderBy('reviews.id','desc')
                            ->take(5)
                            ->get();
         
           $deal=Deal::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->whereMonth('created_at',now()->month)->count();if(!$deal){$deal=0;}
           $coupon=Coupon::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->count();if(!$coupon){$coupon=0;}

           return view('merchant')->with(['plan'=>$membership->plan,'deal'=>$deal,'feature'=>$feature,'coupon'=>$coupon,'order'=>$order,'er'=>$earning,'py'=>$pay,'sum'=>$sum,'comm'=>$comments,'rate'=>$rate,'invoice'=>$invoice]);
        } else {
            return Redirect('merchant/login');
        }
    }

    public function forget() {
        return view('pages.merchant.forget');
    }

    public function invoice() {
    	if(Auth::guard('vendor')->check())
        {
        	if(request()->id)
        	{
               order::where('merchant_id',request()->id)->where('inv_status',1)->delete();
               return back()->with('success','Data Deleted Successfully');
        	}
    	     $invoice = order::select('orders.merchant_id',DB::raw('sum(orders.amount) As invoice'),'orders.inv_status','orders.updated_at')
    	               ->distinct('orders.merchant_id')
    	               ->where('orders.merchant_id','=',auth::guard('vendor')->user()->merchant_id)
    	               ->where('orders.inv_status',1)
    	               ->groupBy('orders.merchant_id','orders.inv_status','orders.updated_at')
    	               ->paginate(10);
            return view('pages.merchant.invoice')->with(['invoice'=>$invoice]);
        }
        else
        {
            return Redirect('merchant/login');
        }
        
    }

    public function deals() {
        if(Auth::guard('vendor')->check())
        {
         $ship=Membership::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->value('plan'); //plan get
         $featured=feature::where('name','featured deals')->value($ship); 
         if(is_numeric($featured))
         {
          $feature=$featured;
         } 
         else  
         {
           $feature=0;
         } //featured deals 

         $limit=feature::first()->value($ship);  //deals limit get per month
         $cat=category::get(); //category get

         $loc=merchant::select('address','category')->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->first(); //location get 

         $deals= deal::select('deals.*',DB::raw('round(avg(reviews.review)) As star'))
                        ->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')
                        ->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)
                        ->groupBy('deals.id')
                        ->orderBy('deals.id','desc')
                        ->paginate(10); //deals data fetch

         $dc=Deal::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->count(); //deals counts all
         $fc=Deal::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->where('featured',1)->count(); //featured count

         /*echo "<pre>";print_r($featured);exit;*/

         return view('pages.merchant.deal')->with(['deals'=>$deals,'count'=>$dc,'limit'=>$limit,'cat'=>$cat,'loc'=>$loc,'feature'=>$feature,'fc'=>$fc]);
        }
        else
        {
            return Redirect('merchant/login');
        }
    }
    public function merchantlogin() {
        return view('pages.merchant.mrlogin');
    }

    public function merchantcoupon() {
        if(Auth::guard('vendor')->check())
        {
           $cat=category::get();
           $loc=merchant::select('address','category')->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->first();
           $coupon=coupon::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->orderBy('id','desc')->paginate(10);
           return view('pages.merchant.coupon')->with(['coup'=>$coupon,'cat'=>$cat,'loc'=>$loc]);
        }
        else
        {
            return Redirect('merchant/login');
        }
    }
    public function merchantpayment() {
            $order=order::select('orders.*','deals.merchant_id','deals.name')
                            ->leftjoin('deals', 'orders.deal_id', '=', 'deals.deal_id')
                            ->where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                            ->orderBy('orders.id','desc')
                            ->paginate(5);
    
           return view('pages.merchant.payment')->with('order',$order);
    }
    public function merchantreport() {
        if(Auth::guard('vendor')->check())
        {
            $st = deal::leftjoin('orders', 'deals.deal_id', '=', 'orders.deal_id')
                            ->where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                            ->whereDate('orders.created_at', carbon::today())
                            ->count(); 
                            //count today purchased deals

            $ts = coupon::leftjoin('coupon_reports', 'coupons.coupon_id', '=', 'coupon_reports.coupon_id')
                            ->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)
                            ->whereDate('coupon_reports.created_at', carbon::today())
                            ->count(); 

        $topfived=deal::select('deals.name',DB::raw('count(orders.id) As five'))
                        ->where('deals.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                        ->whereMonth('orders.created_at', '=', now()->month)
                        ->leftjoin('orders', 'deals.deal_id', '=', 'orders.deal_id')
                        ->groupBy('deals.id')
                        ->orderBy('five','desc')
                        ->limit(5)
                        ->get();

        $topfivec=coupon::select('coupons.name',DB::raw('count(coupon_reports.used) As count'))
                        ->where('coupons.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                        ->whereMonth('coupon_reports.created_at', '=', now()->month)
                        ->leftjoin('coupon_reports', 'coupons.coupon_id', '=', 'coupon_reports.coupon_id')
                        ->groupBy('coupons.id')
                        ->orderBy('count','desc')
                        ->limit(5)
                        ->get();

            /*echo "<pre>";print_r($topfivec);exit;*/


            $ivy=deal::leftjoin('orders', 'deals.deal_id', '=', 'orders.deal_id')
                              ->where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                              ->whereYear('orders.created_at', '=', now()->year)
                              ->sum('amount');

            if($ivy==0){ $values=0;}
            else{
                for ($i=1;$i<=12;$i++) 
                {
                  $values[]=deal::leftjoin('orders', 'deals.deal_id', '=', 'orders.deal_id')
                                  ->where('orders.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                                  ->whereYear('orders.created_at', '=', now()->year)
                                  ->whereMonth('orders.created_at', '=', $i)
                                  ->sum('amount'); 
                                  // All year payment 
                }
            }

            $view=deal::select(DB::raw('count(promotion.deal_id) As view'))
                        ->where('deals.merchant_id',Auth::guard('vendor')->user()->merchant_id)
                        ->leftjoin('promotion','deals.deal_id','=','promotion.deal_id')
                        ->whereDate('promotion.created_at',carbon::today())
                        ->groupBy('deals.id')
                        ->get();
                  /*  echo "<pre>";print_r($view);exit;*/
            $sum = 0;
            foreach($view as $fg => $df) 
            {
                $sum+=$df->view;
            }
            $promotion=promotion::select('platform',DB::raw('count(platform) As count'))
                            ->distinct('platform')
                            ->groupBy('platform')
                            ->get();
                            
          return view('pages.merchant.report')->with(['val'=>$values,'st'=>$st,'stl'=>$topfived,'lts'=>$topfivec,'view'=>$sum,'ts'=>$ts,'share'=>$promotion]);
        }
        else
        {
            return Redirect('merchant/login');
        }
    }
    public function merchantbusiness() {
    	if(Auth::guard('vendor')->check())
        {
		     $cat = category::get();
		     $us=Merchant::leftjoin('categories', 'merchant.category', '=', 'categories.id')
                           ->where('merchant_id',Auth::guard('vendor')
                            ->user()->merchant_id)
                           ->first();

		     return view('pages.merchant.business')->with(['mr'=>$us,'cat'=>$cat]);
        }
        else
        {
            return Redirect('merchant/login');
        }
    }
    
    public function merchantsupport() {
        return view('pages.merchant.support');
    }
    public function merchantmembership() {
    	if(Auth::guard('vendor')->check())
        {
         $ship = Membership::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->first();
         $data = feature::orderBy('id', 'ASC')->get();
         $price = feature::where('name','price')->first();
         return view('pages.merchant.membership')->with(['data'=>$data,'ship'=>$ship,'price'=>$price]);
        }
        else
        {
            return Redirect('merchant/login');
        }
    }

    public function paysuccess(Request $request){
        $data = [
                 'payment_id'     =>  $request->payment_id,
                 'merchant_id'    =>  $request->product_id,
                 'amount'         =>  $request->amount,
                 'email'          =>  $request->email,
                 'phone'          =>  $request->phone,
                 'plan'           =>  $request->plan,
                 'remember_token' =>  $request->token,
                 'status'         =>  'Success',
                ];
        $getId = Payment::insertGetId($data);  
        $datas = Membership::where('merchant_id',$request->product_id)->update(['plan'=> $request->plan]); 
        $arr = array('msg' => 'Payment successful.', 'status' => true);
        return Response()->json($arr); 
    }

     public function merchantsuccess(Request $request){
       return Redirect('merchant/membership')->with('success','Payment Done Successfully');
     }

    public function merchantprofile() {
      $us=Merchant::select('tw','fb','gp')->where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->first();
      return view('pages.merchant.profile')->with('social',$us);
    }

    public function vendorlog(Request $request) {
        $rules = array('email' => 'required', 'password' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) 
        {
            return Redirect::back()->withErrors($validator, 'danger')->withInput();
        } 
        else 
        {
            if (Auth::guard('vendor')->attempt(array('email' => $request->get('email'), 'password' => $request->get('password')))) 
            {
                session(['merchant_id' => Auth::guard('vendor')->user()->merchant_id,'email' => Auth::guard('vendor')->user()->email,'image' => Auth::guard('vendor')->user()->image, 'role' => 'vendor']);
                return Redirect('/merchant');
            } 
            else 
            {
                return Redirect::back()->withErrors('Wrong Credentials', 'danger')->withInput();
            }
        }
    }

    public function send_otp(request $request) {
        $sid     = env('TWILIO_SID');
        $token   = env('TWILIO_TOKEN');
        $client  = new Client($sid, $token);
        $number  = '+91' . $request->get('nm');
        $remember_token=$request->get('token');
        $sendotp = mt_rand(100000, 999999);
        $request['user_id'] = mt_rand(1000000, 9999999);
        $message = $client->messages->create($number, (['body' => $sendotp, 'from' => env('TWILIO_FROM')]));
        otp::Create([
                        'user_id' => $request['user_id'], 'otp' => $sendotp, 'phone' => $number, 'remember_token' => $remember_token,'expire'=>'no'
                    ]);
    }

    public function merchant_reg(request $input) {

        $sid    = env('TWILIO_SID');
        $token  = env('TWILIO_TOKEN');
        $client = new Client($sid, $token);

        $otp = $input->get('otp');
        $phone = '+91' . $input->get('phone');

        if ($otp) 
        {
            $userotp = otp::where(['phone' => $phone ,'expire'=>'no'])->orderBy('id', 'desc')->get()->first();
            if ($otp == $userotp->otp) 
            {
                $image = $input->file('image');
                $name = 'images/merchant/merchant' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/merchant/');
                $image->move($destinationPath, $name);
                $request = $input->except(['image']);
                $request['image'] = $name;
                $request['merchant_id'] = mt_rand(1000000, 9999999);
                $request['password'] = Hash::make($input->get('password'));
                $request['remember_token'] = $input->get('_token');
                merchant::Create($request);
                otp::where('phone',$phone)->update(['expire'=>'yes']);
                membership::Create(['merchant_id' => $request['merchant_id'], 'plan' => 'starter', 'remember_token' => $input->get('_token')]);
                return Redirect('merchant/login')->with('success', 'Account Created Successfully');
            } 
            else 
            {
                return Redirect::back()->withErrors('Otp Incorrect', 'danger')->withInput();
            }
        }
    }

    public function dealsave(request $request) {
        $rules = array('deal' => 'required', 'price' => 'required','discount' => 'required',
            'validity' => 'required','description' => 'required' ,'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'deal')->withInput();
        } 
        else 
        {
            $user = new deal;
            $user->merchant_id=$request->merchant_id;
            $user->deal_id=mt_rand(100000, 999999);
            $user->name=$request->deal;
            $user->price=$request->price;
            $user->discount=$request->discount;
           /* $user->quantity=$request->quantity;*/
            $user->validity=now()->addDays($request->validity);
            $user->description=$request->description;
            $user->location=$request->location;
            $user->cat=$request->category;
            if($request->featured){$user->featured=$request->featured;}
            else{$user->featured=0;}
            $image = $request->file('image');
                $name = 'images/merchant/deal' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/merchant');
                $image->move($destinationPath, $name);
                $user->image = $name;
            $user->remember_token=$request->_token;
           /* $user->bought=0;*/
            $user->save();
            return back()->with('success', 'Deal Data Save Successfully');
       }
    }
    public function deletedeal($id) {
        $stud=deal::where('id',$id);
        $stud->delete();
        return Redirect('/merchant/deals')->with('success', 'Deal Deleted Successfully');
    }
    public function deletecoupon($id) {
        $stud=coupon::where('id',$id);
        $stud->delete();
        return Redirect('/merchant/coupons')->with('success', 'Coupon Deleted Successfully');
    }
    public function couponsave(request $request) {
        $rules = array('coupon' => 'required','code' => 'required', 'discount' => 'required',
            'validity' => 'required','description' => 'required' ,'image' => 'required|mimes:jpeg,jpg,png,gif|max:10000');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'coupon')->withInput();
        } 
        else 
        {
            $user = new coupon;
            $user->coupon_id=mt_rand(100000, 999999);
            $user->merchant_id=$request->merchant_id;
            $user->name=$request->coupon;
            $user->code=$request->code;
            $user->discount=$request->discount;
            $user->validity=now()->addDays($request->validity);
            $user->description=$request->description;
            $user->remember_token=$request->_token;
            $user->location=$request->location;
            $user->cat=$request->category;
            $image = $request->file('image');
                $name = 'images/merchant/coupon' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/merchant/');
                $image->move($destinationPath, $name);
                $user->image = $name;
            $user->save();
            return back()->with('success', 'Coupon Data Save Successfully');
       }
    }
    public function send(Request $request) {
        $email = $request->email;
        if ($email == '') {
            return back()->with('danger', 'Please Enter Email');
        } else {
            $checkemail = merchant::where(['email' => $email])->first();
            if (!$checkemail) {
                return back()->with('danger', 'Email Not Found');
            } else {
                $code = Str::random(40);
                code::create(['email' => $email, 'code' => $code, 'expire' => '0']);
                $data = array('email' => $email, 'code' => $code);
                Mail::to($email)->send(new SendMailvendor($data));
                return back()->with('success', 'Security Link Sent Successfully');
            }
        }
    }
    public function newpassword($code) {
        $check = code::where(['code' => $code])->orderBy('id','desc')->first();
        if (now() < $check->created_at->addDays(1)) {
           if ($check->expire == 1) {
                return Redirect('merchant/login')->with('danger', 'Authentication Code Expired');
            } else {
                return view('pages.merchant.newpassword')->with(['code' => $code]);
            }
        } else {
            code::where('code',$code)->update(['expire' => 1]);
            return Redirect('merchant/login')->with('danger', 'Authentication Code Expired');  
        }
    }
    public function setpassword(Request $request) {
        $code = $request->code;
        $rules = array('password' => 'required|min:6|confirmed');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'danger')->withInput();
        } else {
            $password = Hash::make($request->get('password'));
            $check = code::where(['code' => $code])->first();
            merchant::where('email', $check->email)->update(['password' => $password]);
            code::where('code', $check->code)->update(['expire' => 1]);
            return Redirect('/merchant/login')->with('success', 'Password Changed Successfully');
        }
    }  

    public function mlogout(Request $request) {
        $request->session()->flush();
        $request->session()->regenerate();
        Auth::guard('vendor')->logout();
        return Redirect('/merchant/login');
    }
    public function resave(Request $request) {

        $rules = array('name' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) 
        {
            return Redirect::back()->withErrors($validator, 'res')->withInput();
        } 
        else
        {
            $image = $request->file('store');
            if($image)
            {
              /*$destinationPath = public_path('/images/merchant/');
              foreach ($image as $img) 
              {
                $name[] = 'images/merchant/store' . time() . '.' . $img->getClientOriginalExtension();
                $img->move($destinationPath, $name);

              }
             echo '<pre>'; print_r($name);
             exit;*/

            
              $name = 'images/merchant/store' . time() . '.' . $image->getClientOriginalExtension();
               $destinationPath = public_path('/images/merchant/');
               $image->move($destinationPath, $name);
               $input=$request->except(['_token']);
              
            }
            else
            {
               $input=$request->except(['_token','image']);
            }
            $request['remember_token']=$request->_token;
            merchant::where('merchant_id',$request->merchant_id)->update($input);
            return Redirect::back()->with('success', 'Data Updated Successfully');
        }

    }

     public function order_status(Request $request) {

      $arr =order::where('payment_id',$request->id)->update(['order_status'=>1]);
      return $arr;
     }

     public function dealupdate(Request $request) {

        if($request->featured==''){$request['featured']=0;}
        $image = $request->file('image');
        if($image)
        {
            $name = 'images/merchant/deal' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/merchant/');
            $image->move($destinationPath, $name);
            $input=$request->except(['_token']);
            $input['image']=$name;
        }
        else
        {
           $input=$request->except(['_token']);
        }
        $input['validity']=now()->addDays($request->validity);
        $input['remember_token']=$request->_token;
        deal::where('deal_id',$input['deal_id'])->update($input);
        return Redirect::back()->with('success', 'Data Updated Successfully');
     }
     public function couponupdate(Request $request) {

        $image = $request->file('image');
        if($image)
        {
            $name = 'images/merchant/coupon' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/merchant/');
            $image->move($destinationPath, $name);
            $input=$request->except(['_token']);
            $input['image']=$name;
        }
        else
        {
           $input=$request->except(['_token']);
        }
       
        $input['validity']=now()->addDays($request->validity);
        $input['remember_token']=$request->_token;
        coupon::where('coupon_id',$input['coupon_id'])->update($input);
        return Redirect::back()->with('success', 'Data Updated Successfully');
     }
     public function merchant_profile(Request $request) {
          $image = $request->file('image');
          if($image)
          {
            $name = 'images/merchant/profile' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/merchant/');
            $image->move($destinationPath, $name);
            $input=$request->except(['_token']);
            $input['image']=$name;
          }
          else
          {
            $input=$request->except(['_token','image']);
          }
          $input['remember_token']=$request->_token;
          merchant::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->update($input);
          return Redirect::back()->with('success', 'Data Updated Successfully');
       }

       public function merchant_password(Request $request) {
          $rules = array('password' => 'required|min:6|confirmed');
          $validator = Validator::make(Input::all(), $rules);
          if ($validator->fails()) {
              return Redirect::back()->withErrors($validator, 'pswd')->withInput();
          } else {
            $password = Hash::make($request->get('password'));
            merchant::where('merchant_id',Auth::guard('vendor')->user()->merchant_id)->update(['password'=>$password]);
            return Redirect::back()->with('successs', 'Data Updated Successfully');
          }
        }
}
