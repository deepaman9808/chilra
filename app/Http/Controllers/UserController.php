<?php
namespace App\Http\Controllers;
use Str;
use Hash;
use View;
use Cookie;
use Session;
use App\Code;
use App\Blog;
use App\Otp;
use App\Promotion;
use App\User;
use App\Deal;
use App\Local;
use App\Order;
use Socialite;
use App\Coupon;
use App\Review;
use App\Setting;
Use App\Traffic;
use App\Student;
use App\Payment;
use App\Comment;
use App\Facebook;
use App\Category;
use App\Merchant;
use Carbon\Carbon;
use App\Follower;
use App\Contactus;
use App\Localbuy;
use App\Coupon_report;
use Twilio\Rest\Client;
use App\Mail\SendMailuser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator, Redirect, Response, File;
use App\Helpers\UserSystemInfoHelper;
class UserController extends Controller {
    public function __construct() {
        $site = Setting::first();
        $cat = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(deals.cat) As count'))->leftjoin('deals', 'categories.id', '=', 'deals.cat')->groupBy('categories.id')->get();
        $count = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(coupons.cat) As cpo'))->leftjoin('coupons', 'categories.id', '=', 'coupons.cat')->groupBy('categories.id')->get();
        View::share(['fb'=> $site->fb,'tw'=>$site->tw,'gp'=>$site->gp,'cats'=>$cat,'catscp'=>$count]);
    }
    /* login page */
    public function login() {
        return view('pages.main.login');
    }
    /* register page */
    public function register() {
        return view('pages.main.register');
    }
    //user registration
    public function registered(Request $request) {
        $rules = array('name' => 'required|min:4', 'email' => 'required|unique:users|email', 'image' => 'required|mimes:jpeg,jpg,png,gif|max:5000', 'password' => 'required|min:6|confirmed', 'policy' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'register')->withInput();
        } else {
            $user = new User;
            $user->user_id = mt_rand(10000000, 99999999);
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->provider = 'gmail';
            $user->profile = '20';
            $user->status = '0';
            $user->password = Hash::make($request->get('password'));
            $user->remember_token = $request->get('_token');
            $image = $request->file('image');
            $name = 'images/home/user' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/home/');
            $image->move($destinationPath, $name);
            $user->image = $name;
            $user->save();
            if (Auth::attempt(array('email' => $request->email, 'password' => $request->get('password')))) {
                return Redirect('/home');
            } else {
                return Redirect::back()->withErrors('Something Went Wrong', 'register')->withInput();
            }
        }
    }
    //user login
    public function log(Request $request) {
        $rules = array('email' => 'required', 'password' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'danger')->withInput();
        } else {
            $credentials = array('email' => $request->email, 'password' => $request->password);
            if ($request->remember) {
                if (Auth::guard('web')->attempt($credentials, true)) {
                    return Redirect('/home');
                } else {
                    return Redirect::back()->withErrors('Wrong Credentials', 'danger')->withInput();
                }
            } else {
                if (Auth::guard('web')->attempt($credentials)) {
                    return Redirect('/home');
                } else {
                    return Redirect::back()->withErrors('Wrong Credentials', 'danger')->withInput();
                }
            }
        }
    }
    //logout
    public function logout(Request $request) {
        $request->session()->flush();
        Auth::guard('web')->logout();
        return Redirect('/login');
    }
    //logout
    public function profile() {
        if (Auth::guard('web')->check()) {
            if (request()->deleteid) {
                local::where('deal_id', request()->deleteid)->delete();
                return redirect('profile');
            }
            $f = follower::where('user_id', auth::user()->user_id)->count();
            $c = local::where('user_id', auth::user()->user_id)->count();
            $d = localbuy::where('user_id', auth::user()->user_id)->count();

            $cat = category::all();
            $data = order::select('orders.*', 'deals.name')
                        ->leftjoin('deals', 'orders.deal_id', '=', 'deals.deal_id')
                        ->where('email', auth::user()->email)
                        ->groupBy('orders.id', 'deals.name')
                        ->orderBy('orders.id', 'desc')
                        ->get();

            $saving = order::where('email', auth::user()->email)
                          ->orderBy('orders.id', 'desc')
                          ->sum('saving');

            $totalam = order::where('email', auth::user()->email)
                        ->orderBy('orders.id', 'desc')
                        ->sum('amount');

            $deal = local::select('locals.*', DB::raw('round(avg(reviews.review)) As star'), 'categories.cat as cats', DB::raw('count(promotion.deal_id) As view'))
                       ->leftjoin('categories', 'locals.cat', '=', 'categories.id')
                       ->leftjoin('promotion', 'locals.deal_id', '=', 'promotion.deal_id')
                       ->leftjoin('reviews', 'locals.deal_id', '=', 'reviews.deal_id')
                       ->where('locals.user_id', auth::user()->user_id)
                       ->groupBy('locals.id')
                       ->get();
                $sum = 0;
                foreach ($deal as $fg => $df) {
                    $sum+= $df->star;
                }
                if($sum!=0) {$avg=$sum / count($deal);} else {$avg=0;}

            return view('/pages.main.profile')->with(['data' => $data,'saving'=>$saving, 'sum' => $totalam, 'rate' => $sum, 'cat' => $cat, 'deal' => $deal, 'follower' => $f, 'mydeals' => $c, 'sales' => $d]);
        } else {
            return redirect('/login');
        }
    }
    public function dealupdate(request $request) {
        if (Auth::guard('web')->check()) {
            $image = $request->file('image');
            if ($image) {
                $name = 'images/home/deal' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/home/');
                $image->move($destinationPath, $name);
                $input = $request->except(['_token']);
                $input['image'] = $name;
            } else {
                $input = $request->except(['_token', 'image']);
            }
            $request['remember_token'] = $request->_token;
            $input['validity'] = now()->addDays($request->validity);
            local::where('user_id', $request->user_id)->update($input);
            return back()->with('success', 'Local deal updated successfully');
        } else {
            return back()->withErrors('Something went wrong', 'login')->withInput();
        }
    }
    //update profile
    public function update(request $request) {
        $validator = Validator::make($request->all(), ['gender' => 'required', 'dob' => 'required|mimes:jpeg,jpg,png,gif|max:5000', 'mobile' => 'required|numeric', 'college' => 'required', 'course' => 'required', 'address' => 'required', 'interest' => 'required', 'urself' => 'required', 'image' => 'mimes:jpeg,jpg,png,gif|max:5000']);
        if ($validator->fails()) {
            return redirect('/profile')->withErrors($validator, 'login')->withInput();
        } else {
            $image = $request->file('dob');
            $name = 'images/home/student' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/home/');
            $image->move($destinationPath, $name);
            $input = $request->except(['_token', 'name', 'email']);
            $input['remember_token'] = $request['_token'];
            $input['dob'] = $name;
            Student::insert($input);
            User::where('user_id', auth()->user()->user_id)->update(['profile' => 100]);
            return Redirect('/profile');
        }
    }
    public function local(request $request) {
        if (request()->cat) {
            $local = local::select('locals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'locals.deal_id', '=', 'reviews.deal_id')->groupBy('locals.id')->where('locals.validity', '>', now())->where('locals.cat', '=', request()->cat)->get();
        } else {
            $local = local::select('locals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'locals.deal_id', '=', 'reviews.deal_id')->groupBy('locals.id')->where('locals.validity', '>', now())->get();
        }
        $cat = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(locals.cat) As count'))->leftjoin('locals', 'categories.id', '=', 'locals.cat')->groupBy('categories.id')->get();
        $loc = local::select('loc')->distinct()->get();
        return view('pages.main.local')->with(['cat' => $cat, 'loc' => $loc, 'local' => $local]);
    }
    //homepage
    public function home(request $request) {
        $getip = UserSystemInfoHelper::get_ip();
        $getbrowser = UserSystemInfoHelper::get_browsers();
        $getdevice = UserSystemInfoHelper::get_device();
        $getos = UserSystemInfoHelper::get_os();
        $user = new traffic();
        $user->ip = $getip;
        $user->browser = $getbrowser;
        $user->device = $getdevice;
        $user->os = $getos;
        $user->save();
        $cat = category::all();
        $featured = deal::select('deals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->groupBy('deals.id')->where('deals.validity', '>', now())->where('featured', 1)->orderBy('id', 'desc')->get();
       $topdeal = deal::select('deals.*',DB::raw('count(reviews.review) As rating'),DB::raw('round(avg(reviews.review)) As star'))
                        ->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')
                        ->where('deals.validity','>',now())
                        ->groupBy('deals.deal_id')
                        ->orderBy('rating','desc')
				        ->limit(3)
				        ->get();

		/*echo "<pre>";print_r($topdeal);exit;*/

       /* $topdeal = deal::select('deals.name','deals.deal_id','deals.name','deals.price','deals.discount','deals.validity','deals.image','deals.location','deals.created_at',DB::raw('round(avg(reviews.review)) As star'), DB::raw('sum(reviews.review) As rating'))
                         ->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')
                         ->where('deals.created_at','<',now())
                         ->groupBy('deals.name','deals.name','deals.price','deals.discount','deals.validity','deals.image','deals.location','deals.deal_id','reviews.review')
                         /*->orderBy('reviews.review','desc')
                         ->limit(3)
                         ->get();*/
       /* echo "<pre>";print_r($topdeal);exit;*/

        $topcp = coupon::select('coupons.*', DB::raw('count(coupon_reports.used) As used'))->leftjoin('coupon_reports', 'coupons.coupon_id', '=', 'coupon_reports.coupon_id')->groupBy('coupons.id', 'coupon_reports.coupon_id')->orderBy('id', 'desc')->limit(3)->get();
        $site = Setting::first();
        $user = user::select('users.user_id', 'users.name', 'users.email', 'users.image', DB::raw('sum(followers.total) As follower'), 'students.address')->leftjoin('followers', 'users.user_id', '=', 'followers.user_id')->leftjoin('students', 'users.user_id', '=', 'students.user_id')->groupBy('users.user_id', 'users.name', 'users.email', 'users.image', 'students.address', 'followers.total')->where('users.status', '=', 1)->orderBy('followers.total', 'desc')->get()->take(3);
        /*$store = merchant::select('merchant.descr','merchant.merchant_id', 'merchant.name', 'merchant.id', 'merchant.category', 'merchant.store', 'merchant.address', 'categories.cat', 'categories.icon', 'merchant.created_at', 'merchant.phone')->leftjoin('categories', 'merchant.category', '=', 'categories.id')->orderBy('merchant.id')->get()->take(3);*/
        $blog=blog::orderBy('id','desc')->get()->take(3);
        return view('home')->with(['cat' => $cat, 'featured' => $featured, 'topthree' => $topdeal, 'topcp' => $topcp, 'site' => $site, 'user' => $user, 'blog' => $blog]);
    }
    public function allstore() {
        if (request()->cat) {
            $store = merchant::select('merchant.merchant_id', 'merchant.name', 'merchant.id', 'merchant.category', 'merchant.descr', 'merchant.store', 'merchant.address', 'categories.cat', 'categories.icon', 'merchant.created_at', 'merchant.phone')->leftjoin('categories', 'merchant.category', '=', 'categories.id')->where('store', '!=', '')->where('merchant.category', '=', request()->cat)->get();
        } else {
            $store = merchant::select('merchant.merchant_id', 'merchant.name', 'merchant.id', 'merchant.descr', 'merchant.category', 'merchant.store', 'merchant.address', 'categories.cat', 'categories.icon', 'merchant.created_at', 'merchant.phone')->leftjoin('categories', 'merchant.category', '=', 'categories.id')->where('store', '!=', '')->get();
        }
        $cat = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(merchant.category) As count'))->leftjoin('merchant', 'categories.id', '=', 'merchant.category')->groupBy('categories.id')->get();
        return view('pages.main.allstore')->with(['cat' => $cat, 'store' => $store]);
    }
    public function dealhunters() {
        $hunters = user::select('users.user_id', 'users.name', 'users.email', 'users.image', DB::raw('sum(followers.total) As follower'), 'students.address')->leftjoin('followers', 'users.user_id', '=', 'followers.user_id')->leftjoin('students', 'users.user_id', '=', 'students.user_id')->groupBy('users.user_id', 'users.name', 'users.email', 'users.image', 'students.address', 'followers.total')->where('users.status', '=', 1)->orderBy('followers.total', 'desc')->get();
        $cat = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(merchant.category) As count'))->leftjoin('merchant', 'categories.id', '=', 'merchant.category')->groupBy('categories.id')->get();
        return view('pages.main.dealhunters')->with(['cat' => $cat, 'hunters' => $hunters]);
    }
    public function localdeals(Request $request) {
        $validator = Validator::make($request->all(), ['name' => 'required', 'image' => 'required|mimes:jpeg,jpg,png,gif|max:5000', 'discount' => 'required|numeric', 'cat' => 'required', 'loc' => 'required', 'validity' => 'required', 'description' => 'required', 'price' => 'required' ]);
        if ($validator->fails()) {
            return redirect('/profile')->withErrors($validator, 'deal')->withInput();
        } else {
            $image = $request->file('image');
            if ($image) {
                $name = 'images/home/deals' . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/home/');
                $image->move($destinationPath, $name);
            }
            $request['remember_token'] = $request->_token;
            $input = $request->except(['_token', 'image']);
            $input['deal_id'] = mt_rand(1000000, 9999999);
            $input['image'] = $name;
            $input['validity'] = now()->addDays($request->validity);
            local::insert($input);
            return back()->with('success', 'Local deal created successfully');
        }
    }
    public function redirect($provider) {
        return Socialite::driver($provider)->redirect();
    }
    public function callback($provider) {
        $getInfo = Socialite::driver($provider)->user();
        $facebook = User::where('user_id', $getInfo->id)->first();
        if (!$facebook) {
            $facebook = User::create(['user_id' => $getInfo->id, 'name' => $getInfo->name, 'email' => $getInfo->email, 'provider' => $provider, 'image' => $getInfo->avatar_original, 'profile' => '20']);
        }
        Auth::login($facebook);
        $user = Auth::user();
        session(['name' => $user->name, 'user_id' => $user->user_id, 'image' => $user->image, 'provider' => $user->provider]);
        return redirect()->to('/home');
    }
    public function forgotuser() {
        return view('pages.main.forgotuser');
    }
    public function send(Request $request) {
        $email = $request->email;
        if ($email == '') {
            return back()->with('danger', 'Please Enter Email');
        } else {
            $checkemail = user::where(['email' => $email])->first();
            if (!$checkemail) {
                return back()->with('danger', 'Email Not Found');
            } else {
                $code = Str::random(40);
                code::create(['email' => $email, 'code' => $code, 'expire' => '0']);
                $data = array('email' => $email, 'code' => $code);
                Mail::to($email)->send(new SendMailuser($data));
                return back()->with('success', 'Security Link Sent Successfully');
            }
        }
    }
    public function newpassword($code) {
        $check = code::where('code',$code)->orderBy('id','desc')->first();
        if (now() < $check->created_at->addDays(1)) {
            if ($check->expire == 1) {
                return Redirect('/forgotuser')->with('danger', 'Authentication Code Expired');
            } else {
                return view('pages.main.newpassword')->with(['code' => $code]);
            }
        } else {
            code::where('code',$code)->update(['expire' => 1]);
            return Redirect('/forgotuser')->with('danger', 'Authentication Code Expired');  
        }
    }
    public function setpassword(Request $request) {
        $rules = array('password' => 'required|min:6|confirmed');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'danger')->withInput();
        } else {
            $password = Hash::make($request->get('password'));
            $checkemail = code::where('code',$request->code)->orderBy('id','desc')->first();
            user::where('email', $checkemail->email)->update(['password' => $password]);
            code::where('code', $checkemail->code)->update(['expire' => '1']);
            return Redirect('/login')->with('success', 'Password Changed Successfully');
        }
    }
    public function alldeals(Request $request) {
        if (request()->cat) {
            $a = deal::select('deals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->groupBy('deals.id')->where('deals.validity', '>', now())->where('deals.cat', '=', request()->cat)->get();
            $b = coupon::select('coupons.*')->where('coupons.cat', '=', request()->cat)->get();
        } else {
            $a = deal::select('deals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->groupBy('deals.id')->where('deals.validity', '>', now())->get();
            $b = coupon::select('coupons.*')->get();
        }
        $c = collect($a)->merge($b)->sortbydesc('id');
        $cat = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(deals.cat) As count'))->leftjoin('deals', 'categories.id', '=', 'deals.cat')->groupBy('categories.id')->get();
        $location = deal::select(DB::raw('distinct(deals.location)'), DB::raw('count(deals.cat) As cdn'))->groupBy('deals.location')->get();
        $featured = deal::select('deals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->groupBy('deals.id')->where('deals.validity', '>', now())->where('featured', 1)->orderBy('id', 'desc')->get();
        return view('pages.main.alldeals')->with(['merge' => $c, 'cats' => $cat, 'location' => $location, 'featured' => $featured]);
    }
    public function deals(Request $request) {
        if (request()->cat) {
            $a = deal::select('deals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->groupBy('deals.id')->where('deals.validity', '>', now())->where('deals.cat', '=', request()->cat)->orderBy('deals.id', 'desc')->get();
        } else {
            $a = deal::select('deals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->groupBy('deals.id')->where('deals.validity', '>', now())->orderBy('deals.id', 'desc')->get();
        }
        $cat = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(deals.cat) As count'))->leftjoin('deals', 'categories.id', '=', 'deals.cat')->groupBy('categories.id')->get();
        $location = deal::select(DB::raw('distinct(deals.location)'), DB::raw('count(deals.cat) As cdn'))->groupBy('deals.location')->get();
        $featured = deal::select('deals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->groupBy('deals.id')->where('deals.validity', '>', now())->where('featured', 1)->orderBy('id', 'desc')->get();
        return view('pages.main.deals')->with(['merge' => $a, 'cats' => $cat, 'location' => $location, 'featured' => $featured]);
    }
    public function coupons(Request $request) {
      /*  if(auth::guard('web')->check())
        {*/
            if (request()->cat) {
                $b = coupon::where('cat', request()->cat)->orderBy('id', 'desc')->get();
            } else {
                $b = coupon::orderBy('id', 'desc')->get();
            }
            $cat = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(deals.cat) As count'))->leftjoin('deals', 'categories.id', '=', 'deals.cat')->groupBy('categories.id')->get();
            $count = category::select(DB::raw('distinct(categories.cat)'), 'categories.id', DB::raw('count(coupons.cat) As cpo'))->leftjoin('coupons', 'categories.id', '=', 'coupons.cat')->groupBy('categories.id')->get();
            $location = coupon::select(DB::raw('distinct(coupons.location)'), DB::raw('count(coupons.cat) As cdn'))->groupBy('coupons.location')->get();
            return view('pages.main.coupons')->with(['merge' => $b, 'count' => $count, 'location' => $location]);
        /*}
        else
        {
            return Redirect('login');
        }*/
    }
    public function dealpay(Request $request) {
        $data = [
                    'payment_id' => $request->payment_id, 
                    'deal_id' => $request->product_id,  
                    'merchant_id' => $request->merchant, 
                    'amount' => $request->amount, 
                    'saving' => $request->saving, 
                    'email' => $request->email, 
                    'phone' => $request->phone, 
                    'remember_token' => $request->token, 
                    'status' => 'success', 
                    'order_status' => 0,
                    'inv_status' => 0
                ];
        $arr = Order::insertGetId($data);
        return Response()->json($arr);
    }
    public function dealsuccess(Request $request) {
        return Redirect('home/deals')->with('success', 'Deal Purchased Successfully');
    }
    public function business(Request $request) {
        if (auth::guard('web')->check()) {

        	if(auth::user()->status==1)
    		{
               if (request()->id) 
               {
                    if(request()->enc)
                    {
                        if(request()->enc && request()->user_id==auth::guard('web')->user()->user_id)
                        { 
                          comment::where('id',request()->enc)->delete();
                          return Redirect()->back()->with('success','comment deleted successfully');
                        }
                        else
                        {
                          return back()->with('danger','Something went wrong');
                        }
                    }
                    if(request()->whatsapp!='')
                    {
                       $mr= deal::select('merchant_id')->where('deal_id',request()->id)->value('merchant_id');
                        promotion::updateOrCreate(['user_id'=>auth::guard()->user()->user_id,'deal_id'=>request()->id,'provider_id'=>$mr,'platform'=>'whatsapp']);
                    }
                    elseif(request()->fbclid!='')
                    {
                        $mr= deal::select('merchant_id')->where('deal_id',request()->id)->value('merchant_id');
                        promotion::updateOrCreate(['user_id'=>auth::guard()->user()->user_id,'deal_id'=>request()->id,'provider_id'=>$mr,'platform'=>'facebook']);
                    }
                    elseif(request()->link!='')
                    {
                        $mr= deal::select('merchant_id')->where('deal_id',request()->id)->value('merchant_id');
                        promotion::updateOrCreate(['user_id'=>auth::guard()->user()->user_id,'deal_id'=>request()->id,'provider_id'=>$mr,'platform'=>'link']);
                    }
                    else
                    {
                        $mr= deal::select('merchant_id')->where('deal_id',request()->id)->value('merchant_id');
                        promotion::updateOrCreate(['user_id'=>auth::guard()->user()->user_id,'deal_id'=>request()->id,'provider_id'=>$mr,'platform'=>'explore']);
                    }

                    $deal = deal::select('deals.*', 'merchant.address','merchant.merchant_id', DB::raw('count(orders.deal_id) As bought'))->leftjoin('merchant', 'deals.merchant_id', '=', 'merchant.merchant_id')->leftjoin('orders', 'deals.deal_id', '=', 'orders.deal_id')->where('deals.deal_id', $request->id)->groupBy('deals.id', 'orders.email', 'merchant.address')->first();
                    $user = student::select('mobile')->where('user_id', auth::user()->user_id)->first();
                    $comment = comment::select('comments.*', 'users.image', 'users.name')->leftjoin('users', 'comments.user_id', '=', 'users.user_id')->where('deal_id', $request->id)->orderBy('comments.id', "DESC")->get();
                    $review = review::where('deal_id', $request->id)->count();
                    $dreview = review::where('user_id',auth::guard('web')->user()->user_id)->where('deal_id', $request->id)->count();
                    $star = review::where('deal_id', $request->id)->avg('review');
                    /*$rep = deal_report::updateOrCreate(['deal_id' => $request->id, 'user_id' => auth::user()->user_id]);*/
                    return view('pages.main.business')->with(['deal' => $deal, 'comment' => $comment, 'review' => $review, 'star' => round($star, 1), 'user' => $user,'dreview'=>$dreview]);
                } 
                else 
                {
                   return Redirect('/home');
                }
            }
                else
    		{
    			return redirect('/profile');
    		}
        } 
        else 
        {
            return Redirect('/login');
        }
    }
    public function localbuy(Request $request) {
        if (auth::guard('web')->check()) {
            if (request()->id) 
            {
            	if(request()->enc)
                    {
                        if(request()->enc && request()->user_id==auth::guard('web')->user()->user_id)
                        { 
                          comment::where('id',request()->enc)->delete();
                          return Redirect()->back()->with('success','comment deleted successfully');
                        }
                        else
                        {
                          return back()->with('danger','Something went wrong');
                        }
                    }
                $deal = local::select('locals.*', DB::raw('count(promotion.deal_id) As view'))->leftjoin('promotion', 'locals.deal_id', '=', 'promotion.deal_id')->where('locals.deal_id', $request->id)->groupBy('locals.id')->first();
                $user = student::select('mobile')->where('user_id', auth::user()->user_id)->first();
                $comment = comment::select('comments.*', 'users.image', 'users.name')->leftjoin('users', 'comments.user_id', '=', 'users.user_id')->where('deal_id', $request->id)->orderBy('comments.id', "DESC")->get();
                $review = review::where('deal_id', $request->id)->count();
                $dreview = review::where('user_id',auth::guard('web')->user()->user_id)->where('deal_id', $request->id)->count();
                $star = review::where('deal_id', $request->id)->avg('review');
                $mr= local::where('deal_id',request()->id)->value('user_id');
                
                $rep = promotion::updateOrCreate(['deal_id' => request()->id,'provider_id'=>$mr, 'user_id' => auth::user()->user_id,'platform'=>'explore']);
                return view('pages.main.localbuy')->with(['deal' => $deal, 'comment' => $comment, 'review' => $review, 'star' => round($star, 1), 'user' => $user,'dreview'=>$dreview]);
            } else {
                return Redirect('/home');
            }
        } else {
            return Redirect('/home');
        }
    }
    public function store(Request $request) {
        $store = merchant::where('merchant_id', $request->id)->first();
        $a = deal::select('deals.*', 'merchant.address', DB::raw('round(avg(reviews.review)) As star'),DB::raw('count(reviews.review) As rating'))->leftjoin('merchant', 'deals.merchant_id', '=', 'merchant.merchant_id')->leftjoin('reviews', 'deals.deal_id', '=', 'reviews.deal_id')->where('merchant.merchant_id', $request->id)->groupBy('deals.id', 'merchant.address')->get();
        $e = deal::where('merchant_id', $request->id)->count();
        $b = coupon::select('coupons.*', 'merchant.address')->leftjoin('merchant', 'coupons.merchant_id', '=', 'merchant.merchant_id')->where('merchant.merchant_id', $request->id)->get();

        $f = coupon::where('merchant_id', $request->id)->count();
        $c = collect($a)->merge($b)->sortbydesc('id');
        $cat = category::where('id', $store->category)->value('cat');
        return view('pages.main.store')->with(['store' => $store, 'cat' => $cat, 'merge' => $c, 'dl' => $e, 'cp' => $f]);
    }
    public function userprofile(Request $request) {
    	if (auth::guard('web')->check()) 
    	{
    		if(auth::user()->status==1)
    		{
    			$profile = user::select('users.user_id', 'users.email', 'users.image', 'users.name', 'students.gender','students.interest','students.urself','students.created_at')->leftjoin('students', 'users.user_id', '=', 'students.user_id')->where('users.user_id','=',request()->id)->groupBy('users.id', 'students.id')->first();

                

		        $deals = local::select('locals.*', DB::raw('round(avg(reviews.review)) As star'), DB::raw('count(reviews.review) As rating'))->leftjoin('reviews', 'locals.deal_id', '=', 'reviews.deal_id')->where('locals.user_id', $profile->user_id)->groupBy('locals.id')->get();
		        $sum = 0;
		        foreach ($deals as $fg => $df) {
		            $sum+= $df->star;
		        }
		        if($sum!=0) {$avg=$sum / count($deals);} else {$avg=0;}
		        $c = local::where('user_id', $profile->user_id)->count('name');
		        $f = follower::where('user_id', $profile->user_id)->count('user_id');
		        return view('pages.main.userprofile')->with(['profile' => $profile, 'local' => $deals, 'count' => $c, 'follower' => $f, 'avg' => $avg]);
    		}
    		else
    		{
    			return redirect('/profile');
    		}
        } 
        else 
        {
            return Redirect('/login');
        }
    }
    public function review(Request $request) {
        $data = ['user_id' => $request->user_id, 'deal_id' => $request->deal_id, 'review' => $request->review, 'remember_token' => $request->token];
        review::updateOrCreate(['user_id' => $request->user_id, 'deal_id' => $request->deal_id], $data);
        $getid = review::where('deal_id', $request->deal_id)->count();
        return Response()->json($getid);
    }
    public function comment(Request $request) {
        $data = ['user_id' => $request->user_id, 'deal_id' => $request->deal_id, 'comment' => $request->comment, 'page' => $request->page , 'remember_token' => $request->token];
        comment::insert($data);
        $arr = array('comment' => $request->comment);
        return Response()->json($arr);
    }
    public function couponreport(Request $request) {
        coupon_report::updateOrCreate(['coupon_id' => $request->id, 'user_id' => $request->user_id, 'used' => 1]);
    }
    public function follower(Request $request) {
        follower::updateOrCreate(['user_id' => $request->user_id, 'login_id' => $request->login_id, 'total' => 1, 'remember_token' => $request->token]);
        return 1;
    }
    public function localcheck(Request $request) {
        localbuy::updateOrCreate(['user_id' => $request->user_id, 'deal_id' => $request->deal_id, 'loc' => $request->loc, 'remember_token' => $request->token]);
        return $request->loc;
    }
    public function contact(Request $request) {
        $con=setting::first();
        return view('pages.main.contactus')->with('con',$con);
    }
    public function privacypolicy(Request $request) {
        return view('pages.main.privacypolicy');
    }
    public function contactdb(Request $request) {
        $rules = array('name' => 'required|min:4', 'email' => 'required|email', 'message' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'contact')->withInput();
        } else {
            $input = $request->except(['_token']);
            $input['remember_token'] = $request->_token;
            contactus::insert($input);
            return back()->with('success', 'Thanks for contacting us, our team will get in touch with you soon.');
        }
        return view('pages.main.contactus');
    }
    public function settingup(Request $request) {
        $rules = array('name' => 'required|min:4', 'email' => 'required|email', 'password' => 'required|min:6|confirmed');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'prof')->withInput();
        } else {
            $input = $request->except(['_token', 'password_confirmation']);
            $input['remember_token'] = $request->_token;
            $input['password'] = Hash::make($request->password);
            user::where('user_id', auth::user()->user_id)->update($input);
            return back()->with('suckcess', 'Profile Updated Successfully');
        }
    }
    public function send_otp(request $request) {
        $sid = env('TWILIO_SID');
        $token = env('TWILIO_TOKEN');
        $client = new Client($sid, $token);
        $number = '+91' . $request->get('nm');
        $remember_token = $request->get('token');
        $sendotp = mt_rand(100000, 999999);
        $request['user_id'] = Auth::user()->user_id;
        $message = $client->messages->create($number, (['body' => $sendotp, 'from' => env('TWILIO_FROM') ]));
        otp::Create(['user_id' => $request['user_id'], 'otp' => $sendotp, 'phone' => $number, 'remember_token' => $remember_token, 'expire' => 'no']);
    }
    public function check_otp(request $request) {
        $otp = otp::where('user_id', Auth::user()->user_id)->orderBy('id', 'desc')->first();
        if ($otp->expire == 'no') {
            if ($otp->otp == $request->otp) {
                otp::where('otp', $otp->otp)->update(['expire' => 'yes']);
                return '1'; //otp matched
                
            } else {
                return '0'; //otp incorrect
                
            }
        } else {
            return '2'; //otp expire
            
        }
    }
    public function check_pid(request $request) {
        $deal = deal::where('deal_id', $request->pid)->first();
        return $deal;
    }
    public function blog(request $request) {
        $blog=blog::orderBy('id','desc')->paginate(9);
        return view('pages.main.blog')->with('blog',$blog);
    }
    public function blogpage(request $request) {
        $recent=blog::orderBy('id','desc')->get()->take(3);
        $blog=blog::where('blog_id',request()->id)->first();
        return view('pages.main.blogpage')->with(['data'=>$blog,'rec'=>$recent]);
    }
}
