<?php
namespace App\Http\Controllers;
use Str;
use Hash;
use View;
use Session;
use App\Code;
use App\Blog;
use App\User;
use App\Deal;
use App\Admin;
use App\Traffic;
use App\Setting;
use App\Merchant;
use App\Feature;
use App\Coupon;
use App\Student;
use App\Order;
use App\Payment;
use App\Contactus;
use App\Local;
use App\Media;
use App\Follower;
use App\Category;
use App\Comment;
use App\Membership;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator, Redirect, Response, File;
class AdminController extends Controller {
    public function forgotadmin() {
        return view('pages.dashboard.forgotadmin');
    }
    public function membership() {
        if (Auth::guard('admin')->check()) {
            $data = payment::select('merchant.vendor_name','payments.*')->leftjoin('merchant','payments.merchant_id','=','merchant.merchant_id')->orderBy('id', 'desc')->paginate(10);
            return view('pages.dashboard.membership')->with('meme', $data);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function orders() {
        if (Auth::guard('admin')->check()) {
            if (request()->deleteid) {
                order::where('deal_id', request()->deleteid)->delete();
            }
            $data = order::select('orders.*','deals.name')->leftjoin('deals','orders.deal_id','=','deals.deal_id')->orderBy('id', 'desc')->paginate(10);
            return view('pages.dashboard.order')->with('order', $data);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function coupons() {
        if (Auth::guard('admin')->check()) {
            $coupon = coupon::orderBy('id', 'desc')->paginate(10);
            return view('pages.dashboard.coupons')->with('coupon', $coupon);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function deals() {
        if (Auth::guard('admin')->check()) {
            $deal = Deal::orderBy('id', 'desc')->paginate(10);
            return view('pages.dashboard.deals')->with('deal', $deal);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function payments() {
        return view('pages.dashboard.payments');
    }
    public function featuresedit(Request $request) {
        if ($request->ajax()) {
            feature::where('id', $request->id)->update([$request->field => $request->value]);
        }
    }
    public function featureadd(Request $request) {
        feature::create(['name' => $request->name, 'starter' => $request->starter, 'competitor' => $request->competitor, 'champion' => $request->champion, 'remember_token' => $request->_token]);
        return Redirect('plans');
    }
    public function plans() {
        if (Auth::guard('admin')->check()) {
            $data = feature::orderBy('id', 'ASC')->get();
            return view('pages.dashboard.plans')->with('data', $data);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function contact() {
        if (Auth::guard('admin')->check()) {
            if(request()->id) {
                contactus::where('id',request()->id)->delete();
                return back()->with('success','Message Deleted Successfully');
            }
            $data = contactus::orderBy('id', 'desc')->paginate(10);
            return view('pages.dashboard.contact')->with('contact', $data);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function settings() {
        if (Auth::guard('admin')->check()) {
            $site = Setting::first();
            $cat = category::get();
            return view('pages.dashboard.settings')->with(['site' => $site, 'cat' => $cat]);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function invoices() {
        if (Auth::guard('admin')->check()) {
            if (request()->id) {
                order::where('merchant_id', request()->id)->delete();
                return back()->with('success', 'Data Deleted Successfully');
            }
            $invoice = order::select('merchant.vendor_name', 'orders.merchant_id', DB::raw('sum(orders.amount) As invoice'), 'merchant.email', 'merchant.phone', 'orders.inv_status')->leftjoin('merchant', 'orders.merchant_id', '=', 'merchant.merchant_id')->distinct('orders.merchant_id')->groupBy('orders.merchant_id', 'merchant.vendor_name', 'merchant.email', 'merchant.phone', 'orders.inv_status')->paginate(10);
            return view('pages.dashboard.invoices')->with(['invoice' => $invoice]);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function catedit(request $request) {
        if (Auth::guard('admin')->check()) {
            if (request()->deleteid) {
                category::where('id', request()->deleteid)->delete();
                return back()->with('deleted', 'Data Deleted Successfully');
            }
            category::where('id', $request->cat_id)->update(['cat' => $request->cat, 'icon' => $request->icon]);
            return back()->with('edited', 'Data Updated Successfully');
        } else {
            return Redirect('adminlogin');
        }
    }
    public function catadd(request $request) {
        if (Auth::guard('admin')->check()) {
            category::insert(['cat' => $request->cat, 'icon' => $request->icon]);
            return back()->with('added', 'Data Added Successfully');
        } else {
            return Redirect('adminlogin');
        }
    }
    public function login() {
        return view('pages.dashboard.login');
    }
    public function adminlog(Request $request) {
        $rules = array('email' => 'required|email', 'password' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'login')->withInput();
        } else {
            $credentials = array('email' => strtolower($request->email), 'password' => $request->password);
            if ($request->remember) {
                if (Auth::guard('admin')->attempt($credentials, true)) {
                    return Redirect('/dashboard');
                } else {
                    return Redirect::back()->withErrors('Wrong Credentials', 'login')->withInput();
                }
            } else {
                if (Auth::guard('admin')->attempt($credentials)) {
                    return Redirect('/dashboard');
                } else {
                    return Redirect::back()->withErrors('Wrong Credentials', 'login')->withInput();
                }
            }
        }
    }
    public function locals() {
        if (Auth::guard('admin')->check()) {
            if (request()->id) {
                local::where('deal_id', request()->id)->delete();
            }
            $local = local::select('locals.*','categories.cat')
                            ->leftjoin('categories','locals.cat','=','categories.id')
                            ->orderBy('locals.id', 'desc')
                            ->paginate(10);
            return view('pages.dashboard.local')->with(['local' => $local]);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function dealhunters() {
        if (Auth::guard('admin')->check()) {
            if (request()->deleteid) {
                user::where('user_id', request()->deleteid)->delete();
            }
            $hunter = user::select('users.user_id','users.name','users.image', 'users.email', DB::raw('sum(followers.total) As follower'),'students.mobile','students.address')
                          ->leftjoin('followers', 'users.user_id', '=', 'followers.user_id')
                          ->leftjoin('students', 'users.user_id', '=', 'students.user_id')
                          ->groupBy('users.user_id','users.name', 'users.email', 'followers.total','students.mobile','students.address','users.image')
                          ->where('users.status', '=', 1)
                          ->orderBy('followers.total', 'desc')
                          ->get()
                          ->take(10);
            return view('pages.dashboard.dealhunters')->with(['hunter' => $hunter]);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function dashboard() {
        if (Auth::guard('admin')->check()) {
            $traffic = traffic::distinct()->count('id');
            $users = user::distinct()->count('user_id');
            $merchant = merchant::distinct()->count('id');
            $userdata = user::orderBy('id', 'DESC')->get()->take(5);
            $revenue = payment::orderBy('id', 'DESC')->sum('amount');
            $device = traffic::select('device', DB::raw('count(device) As count'))->distinct('device')->groupBy('device')->get();
            for ($i = 0;$i <= 12;$i++) {
                $values[] = payment::whereYear('created_at', '=', now()->year)->whereMonth('created_at', '=', $i)->sum('amount'); // All year payment 
            }
            return view('dashboard')->with(['userdata' => $userdata, 'traffic' => $traffic, 'users' => $users, 'merchant' => $merchant, 'revenue' => $revenue, 'val' => $values, 'device' => $device]);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function users() {
        if (Auth::guard('admin')->check()) {
            if (request()->deleteid) {
                user::where('user_id', request()->deleteid)->delete();
            }
            $userdata = user::where('profile', '=', 20)->where('status', 0)->orderBy('id', 'DESC')->paginate(10);
            return view('pages.dashboard.users')->with('userdata', $userdata);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function student() {
        if (Auth::guard('admin')->check()) {
            if (request()->action && request()->user_id) {
                user::where('user_id', request()->user_id)->update(['status' => request()->action]);
                return Redirect('student');
            }
            if (request()->deleteid) {
                student::where('user_id', request()->deleteid)->delete();
            }
            $userdata = student::select('students.*', 'users.email', 'users.name', 'users.image', 'users.status')->leftjoin('users', 'users.user_id', '=', 'students.user_id')->where('users.profile', '=', 100)->orderBy('students.id', 'desc')->paginate(10);
            return view('pages.dashboard.student')->with('userdata', $userdata);
        } else {
            return Redirect('adminlogin');
        }
    }
    public function vendors() {
        if (Auth::guard('admin')->check()) {
            if (request()->deleteid) {
                merchant::where('merchant_id', request()->deleteid)->delete();
            }
            $vendordata = Merchant::leftjoin('membership', 'merchant.merchant_id', '=', 'membership.merchant_id')->orderBy('merchant.id', 'desc')->paginate(10);
            return view('pages.dashboard.vendor')->with('vendordata', $vendordata);
        } else {
            return Redirect('adminlogin');
        }
    }
    function removedata(Request $request) {
        $studemt = user::where('user_id', $request->user_id)->first();
        $studemt->delete();
    }
    function removevendor(Request $request) {
        $studemt = merchant::where('user_id', $request->user_id)->first();
        $studemt->delete();
    }
    public function fetch_data(Request $request) {
        if ($request->ajax()) {
            $userdata = user::orderBy('id', 'DESC')->paginate(10);
            return view('pages.dashboard.user_child', compact('userdata'))->render();
        }
    }
    public function fetch_data_vendor(Request $request) {
        if ($request->ajax()) {
            $vendordata = merchant::orderBy('id', 'DESC')->paginate(10);
            return view('pages.dashboard.vendor_child', compact('vendordata'))->render();
        }
    }
    public function dprofile(request $request) {
        if (Auth::guard('admin')->check()) {
            return view('pages.dashboard.profile');
        } else {
            return Redirect('/adminlogin');
        }
    }
    public function dlogout(Request $request) {
        $request->session()->flush();
        $request->session()->regenerate();
        Auth::guard('admin')->logout();
        return Redirect('/adminlogin');
    }
    public function send(Request $request) {
        $email = $request->email;
        if ($email == '') {
            return back()->with('danger', 'Please Enter Email');
        } else {
            $checkemail = user::where(['email' => $email])->first();
            if (!$checkemail) {
                return back()->with('danger', 'Email Not Found');
            } else {
                $code = Str::random(40);
                code::create(['email' => $email, 'code' => $code, 'expire' => '0']);
                $data = array('email' => $email, 'code' => $code);
                Mail::to($email)->send(new SendMailuser($data));
                return back()->with('success', 'Security Link Sent Successfully');
            }
        }
    }
    public function newpassword($code) {
        $checkcode = code::where(['code' => $code])->orderBy('id','desc')->first();
        if (now() < $check->created_at->addDays(1)) {
           if ($check->expire == 1) {
                return Redirect('/forgotadmin')->with('danger', 'Authentication Code Expired');
            } else {
                return view('pages.dashboard.newpassword')->with(['code' => $code]);
            }
        } else {
            code::where('code',$code)->update(['expire' => 1]);
            return Redirect('/forgotadmin')->with('danger', 'Authentication Code Expired');  
        }
    }
    public function setpassword(Request $request) {
        $rules = array('password' => 'required|min:6|confirmed');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'danger')->withInput();
        } else {
            $password = Hash::make($request->get('password'));
            Admin::where('id', $request->id)->update(['password' => $password]);
            $checkemail = code::where(['code' => $request->code])->first();
            code::where('code', $checkemail->code)->update(['expire' => '1']);
            return Redirect('/adminlogin')->with('success', 'Password Changed Successfully');
        }
    }
    public function adminpro(Request $request) {
        $input = $request->except(['password']);
        $input['remember_token'] = $request->get('_token');
        Admin::updateOrCreate(['user_id' => $request->user_id], $input);
        return Redirect('/adminprofile');
    }
    public function adminpassword(Request $request) {
        $rules = array('password' => 'required|min:6|confirmed', 'currentpassword' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'danger')->withInput();
        } else {
            $user = admin::where('user_id', $request->user_id)->first();
            if (Hash::check($request->currentpassword, $user->password)) {
                $user = admin::where('user_id', $request->user_id)->update(['password' => hash::make($request->password) ]);
                return Redirect::back()->with('success', 'Password changed successfully')->withInput();
            } else {
                return Redirect::back()->withErrors('Incorrect Current Password', 'danger')->withInput();
            }
        }
    }
    public function adminproimage(Request $request) {
        $rules = array('profile-img' => 'mimes:jpeg,jpg,png,gif|max:10000');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'pro')->withInput();
        } else {
            $image = $request->file('profile-img');
            $name = 'images/dashboard/' . 'admin' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images/dashboard/');
            $image->move($destinationPath, $name);
            $user = admin::where('user_id', $request->user_id)->update(['image' => $name]);
            return Redirect::back();
        }
    }
    public function icons(Request $request) {
        $rules = array('text' => 'required', 'banner' => 'mimes:jpeg,jpg,png,gif|max:5000');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'danger')->withInput();
        } else {
            $banner = $request->file('banner');
            if ($banner) {
                $name = 'images/brand/' . 'banner' . time() . '.' . $banner->getClientOriginalExtension();
                $destinationPath = public_path('/images/brand/');
                $banner->move($destinationPath, $name);
                $input = $request->except(['_token']);
                $input['banner'] = $name;
            } else {
                $input = $request->except(['_token', 'banner']);
            }
            $input['remember_token'] = $request->_token;
            setting::where('id', 1)->update($input);
            return Redirect::back()->with('success', 'Data Updated Successfully');
        }
    }
    public function cont(Request $request) {
        $rules = array('address' => 'required', 'email' => 'required', 'phone' => 'required', 'descr' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'dangers')->withInput();
        } else {
            $input=$request->except(['_token']);
            $input['remember_token'] = $request->_token;
            setting::where('id', 1)->update($input);
            return Redirect::back()->with('successs', 'Data Updated Successfully');
        }
    }
    public function vendordealedit(Request $request) {
        $rules = array('name' => 'required', 'price' => 'required', 'discount' => 'required', 'validity' => 'required', 'description' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'pro')->withInput();
        } else {
            $input = $request->except(['_token']);
            $input['remember_token'] = $request->_token;
            $input['validity'] = now()->addDays($request->validity);
            deal::where('deal_id', $request->deal_id)->update($input);
            return Redirect('deals')->with('success', 'Data Updated Successfully');
        }
    }
    public function vendorcouedit(Request $request) {
        $rules = array('name' => 'required', 'code' => 'required', 'discount' => 'required', 'validity' => 'required', 'description' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'pro')->withInput();
        } else {
            $input = $request->except(['_token']);
            $input['remember_token'] = $request->_token;
            $input['validity'] = now()->addDays($request->validity);
            coupon::where('coupon_id', $request->coupon_id)->update($input);
            return Redirect('coupons')->with('success', 'Data Updated Successfully');
        }
    }
    public function deletedeal($id) {
        $stud = deal::where('deal_id', $id);
        $stud->delete();
        return Redirect::back()->with('success', 'Deal Deleted Successfully');
    }
    public function deletecoupon($id) {
        $stud = coupon::where('coupon_id', $id);
        $stud->delete();
        return Redirect::back()->with('success', 'Coupon Deleted Successfully');
    }
    public function deletepayment($id) {
        $stud = payment::where('payment_id', $id);
        $stud->delete();
        return Redirect::back()->with('success', 'Data Deleted Successfully');
    }
    public function inv_status(request $request) {
        $inv = order::where('merchant_id', $request->mer)->whereMonth('created_at', now()->month)->update(['inv_status' => $request->id]);
        return $inv;
    }
    public function social(request $request) {
           $input=$request->except(['_token']);
           $input['remember_token']=$request->_token;
           $inv = setting::where('user_id', 'XB6ORQztnUK3T89P')->update($input);
           return back()->with('succ','Data Updated Successfully');
        }
    public function localcomment(request $request) {
        if(auth::guard('admin')->check()) {
            if(request()->id) {
                comment::where('id',request()->id)->delete();
                return back()->with('success','Data Deleted Successfully');
            }
           $comment=comment::select('comments.id','users.name As username','locals.name As dealname','comments.comment','comments.created_at')
                            ->leftjoin('locals','comments.deal_id','=','locals.deal_id')
                            ->leftjoin('users','comments.user_id','=','users.user_id')
                            ->where('comments.page','local')
                            ->orderBy('comments.id','desc')
                            ->paginate(10);
           return view('pages.dashboard.localcomment')->with(['comment'=>$comment]); 
       } else {
            return Redirect('home');
        }
    }   
    public function comment(request $request) {
        if(auth::guard('admin')->check()) {
            if(request()->id) {
                comment::where('id',request()->id)->delete();
                return back()->with('success','Data Deleted Successfully');
            }
           $comment=comment::select('comments.id','users.name As username','deals.name As dealname','comments.comment','comments.created_at')
                            ->leftjoin('deals','comments.deal_id','=','deals.deal_id')
                            ->leftjoin('users','comments.user_id','=','users.user_id')
                            ->where('comments.page','merchant')
                            ->orderBy('comments.id','desc')
                            ->paginate(10);
           return view('pages.dashboard.comment')->with(['comment'=>$comment]); 
       } else {
            return Redirect('home');
        }
    }
    public function blog(request $request) {
        if(auth::guard('admin')->check()) {
            if(request()->deleteid)
            {
                blog::where('blog_id',request()->deleteid)->delete();
                return back()->with('success',"Blog Data Deleted Successfully");
            }
           $blog= blog::orderBy('id','desc')->paginate(10);
           return view('pages.dashboard.blog')->with('blog',$blog); 
       } else {
            return Redirect('home');
        }
    }
    public function addblog(request $request) {
        if(auth::guard('admin')->check()) {
            $cats= category::get();
           return view('pages.dashboard.addblog')->with('cats',$cats); 
       } else {
            return Redirect('home');
        }
    }
    public function saveblog(request $request) {
        if(auth::guard('admin')->check()) {
            $rules = array('name' => 'required','author_name' => 'required','category'=>'required','image'=>'required','content' => 'required');
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator, 'pro')->withInput();
            } else {
                $image = $request->file('image');
                if ($image) {
                    $name = 'images/main/' . 'blog' . time() . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/images/main/');
                    $image->move($destinationPath, $name);
                    $input = $request->except(['_token']);
                    $input['image'] = $name;
                } else {
                    $input = $request->except(['_token', 'image']);
                }
                $arr=array('bg-primary','bg-success','bg-info','bg-warning','bg-secondary','bg-purple');
                $input['color']=$arr[array_rand($arr)];
                $input['remember_token']=$request->_token;
                $input['blog_id'] = Str::random(25);
                $blog = blog::updateOrCreate($input);
                return Redirect('showblog')->with('success','Blog Added Successfully'); 
           } 
        } else {
            return Redirect('home');
        }
    }
    public function editblog(request $request) {
        if(auth::guard('admin')->check()) {
            $cats= category::get();
            $blog=blog::where('blog_id',request()->id)->first();
            return view('pages.dashboard.editblog')->with(['data'=>$blog,'cats'=>$cats]); 
           } 
        else {
            return Redirect('home');
        }
    }
    public function upblog(request $request) {
        if(auth::guard('admin')->check()) {
            $rules = array('name' => 'required','author_name' => 'required','category'=>'required','content' => 'required');
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator, 'pro')->withInput();
            } else {
                $image = $request->file('image');
                if ($image) {
                    $name = 'images/main/' . 'blog' . time() . '.' . $image->getClientOriginalExtension();
                    $destinationPath = public_path('/images/main/');
                    $image->move($destinationPath, $name);
                    $input = $request->except(['_token']);
                    $input['image'] = $name;
                } else {
                    $input = $request->except(['_token', 'image']);
                }
                $arr=array('bg-primary','bg-success','bg-info','bg-warning','bg-secondary','bg-purple');
                $input['color']=$arr[array_rand($arr)];
                $input['remember_token']=$request->_token;
                $blog = blog::where('blog_id',$request->blog_id)->update($input);
                return back()->with('success','Blog Updated Successfully'); 
           } 
        } else {
            return Redirect('home');
        }
    }
    public function media(request $request) {
        if(auth::guard('admin')->check()) {
        	if(request()->id)
        	{
        		media::where('id',request()->id)->delete();
        	}
            $image=media::orderBy('id','desc')->get();
            return view('pages.dashboard.media')->with('image',$image); 
           } 
        else {
            return Redirect('home');
        }
    }
    public function addmedia(request $request) {
        if(auth::guard('admin')->check()) {
        	$rules = array('image'=>'mimes:jpeg,jpg,png|max:5000');
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator, 'pro')->withInput();
            } else {
	        	$image = $request->file('image');
	            $name = 'images/main/' . 'media' . time() . '.' . $image->getClientOriginalExtension();
	            $destinationPath = public_path('/images/main/');
	            $image->move($destinationPath, $name);
	            $input = $request->except(['_token']);
	            $input['image'] = $name;
	            $input['remember_token']=$request->_token;
	            media::insert($input);
	            return back()->with('success','Image Added Successfully');
           } 
       }
        else {
            return Redirect('home');
        }
    }
    public function calltoaction(Request $request) {
        $rules = array('btn_one' => 'required', 'btn_two' => 'required');
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator, 'call')->withInput();
        } else {
            $input=$request->except(['_token']);
            $input['remember_token'] = $request->_token;
            setting::where('id', 1)->update($input);
            return Redirect::back()->with('callset', 'Data Updated Successfully');
        }
    }

}
