<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    protected $table = 'payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id','payment_id','amount','email','phone','plan','status','remember_token'
    ];


     protected $hidden = [
         'remember_token',
    ];

}
