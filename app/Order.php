<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deal_id','payment_id','merchant_id','amount','saving','email','phone','status','order_status','remember_token'
    ];


     protected $hidden = [
         'remember_token',
    ];
}
